//
//  PopKornBrain.h
//  popKorn
//
//  Created by Àlex Vergara on 23/08/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UCInternetUtils.h"
#import "FeaturedList.h"
#import "TVshow.h"
#import "PKSettings.h"
#import "UIKache.h"
#import "TxDownloadShowList.h"
#import "TxDownloadShowInfo.h"
#import "TxDownloadFeatured.h"

//#warning TRY_TO_REMOVE
@class UCSharer, Episode, UIKache;

#pragma Mark - PopKornBrain interface
@interface PopKornBrain : NSObject {
@private
    NSManagedObjectContext *context;
    UCSharer *sharer;
    UIKache *uiKache;
}

#pragma Mark - Properties
@property (strong) NSManagedObjectContext *context;
@property (strong) UCSharer *sharer;
@property (strong) UIKache *uiKache;

#pragma Mark - TVshow methods
- (void) setShow:(TVshow *)show asFavorite:(BOOL)isFavorite;

#pragma Mark - Episode methods
- (BOOL) areTherePastUnseenEpisodes:(Episode *)episode;
- (void) setPastEpisodesAsSeen:(Episode *)episode;

@end
