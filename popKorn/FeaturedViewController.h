//
//  FeaturedViewControllerViewController.h
//  popKorn
//
//  Created by Àlex Vergara Nebot on 29/02/12.
//  Copyright (c) 2012 Undead Coders. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopKornBrain.h"
#import "ShowDetailView.h"

@interface FeaturedViewController : UIViewController <UITableViewDelegate, UITableViewDelegate, UIKacheTable> {
@private
    PopKornBrain *pkb;
    UISegmentedControl *sc;
}

@property (retain) PopKornBrain *pkb;
@property (strong, nonatomic) IBOutlet UITableView *table;

-(id) initWithPopKornBrain:(PopKornBrain *)brain;

@end
