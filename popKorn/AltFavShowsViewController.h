//
//  AltFavShowsViewController.h
//  popKorn
//
//  Created by Àlex Vergara Nebot on 09/02/12.
//  Copyright (c) 2012 Undead Coders. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopKornBrain.h"
#import "ShowDetailView.h"

@interface AltFavShowsViewController : UIViewController <UITableViewDelegate, UITableViewDelegate, UIKacheTable> {
    PopKornBrain *pkb;
    UISegmentedControl *sc;
}

@property (retain) PopKornBrain *pkb;
@property (strong, nonatomic) IBOutlet UITableView *table;

-(id) initWithPopKornBrain:(PopKornBrain *)brain;

@end
