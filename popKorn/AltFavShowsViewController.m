//
//  AltFavShowsViewController.m
//  popKorn
//
//  Created by Àlex Vergara Nebot on 09/02/12.
//  Copyright (c) 2012 Undead Coders. All rights reserved.
//

#import "AltFavShowsViewController.h"

@implementation AltFavShowsViewController

@synthesize pkb;
@synthesize table;

-(id) initWithPopKornBrain:(PopKornBrain *)brain
{
    self = [super init];
    if (self) {
        CustomTabBarItem *tabItem = [[CustomTabBarItem alloc] initWithTitle:NSLocalizedString(@"favorites", nil) image:[UIImage imageNamed:@"favSeries"] tag:0];
        tabItem.customHighlightedImage=[UIImage imageNamed:@"orangeFavSeries"];
        self.tabBarItem = tabItem;
        //self.title = NSLocalizedString(@"favorites", nil);
        
        sc = [[UISegmentedControl alloc] initWithItems:
                                  [NSArray arrayWithObjects:
                                   NSLocalizedString(@"allFavorites", nil),
                                   NSLocalizedString(@"pendingFavorites", nil),
                                   nil]];
        [sc         addTarget:self
                       action:@selector(segmentAction)
             forControlEvents:UIControlEventValueChanged];
        sc.selectedSegmentIndex = 0;
        sc.frame = CGRectMake(0,0,310,30);
        sc.segmentedControlStyle = UISegmentedControlStyleBar;
        UIBarButtonItem *sbi = [[UIBarButtonItem alloc] initWithCustomView:sc];
        self.navigationItem.rightBarButtonItem = sbi;
        
        self.pkb = brain;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Table ops

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (sc.selectedSegmentIndex == 0)
        return [self.pkb.uiKache.favShows count];
    else
        return [self.pkb.uiKache.unseenFavShows count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"FavShowCell";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
	}
    
    KachedFavorite *show;
    if (sc.selectedSegmentIndex == 0)
        show = [self.pkb.uiKache.favShows objectAtIndex:indexPath.row];
    else
        show = [self.pkb.uiKache.unseenFavShows objectAtIndex:indexPath.row];

    cell.textLabel.text = show.name;
    cell.detailTextLabel.text = show.nextEp;
    cell.imageView.image = show.thumbnail;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    //per canviar el color de la cell quan es selecciona
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIColor orangeColor]];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *name;
    if (sc.selectedSegmentIndex == 0)
        name = ((KachedFavorite *) [self.pkb.uiKache.favShows objectAtIndex:indexPath.row]).name;
    else
        name = ((KachedFavorite *) [self.pkb.uiKache.unseenFavShows objectAtIndex:indexPath.row]).name;
    TVshow *show = [TVshow tvShowWithName:name inContext:self.pkb.context];
    ShowDetailView *dv = [[ShowDetailView alloc] init];
    dv.show = show;
    dv.pkb = self.pkb;
    [self.navigationController pushViewController:dv animated:YES];
}

#pragma mark - Auxiliary ops

- (void) segmentAction {
    [self.table reloadData];
}

- (void) reloadTable {
    [self.table reloadData];
}

#pragma mark - View lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [self.table reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [self setTable:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
