//
//  ShowInfoViewController.m
//  popKorn
//
//  Created by Àlex Vergara on 10/09/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import "ShowInfoViewController.h"

@implementation ShowInfoViewController

@synthesize labelGenre;
@synthesize labelClassification;
@synthesize labelStatus;
@synthesize labelTitle;
@synthesize shareTwitter;
@synthesize shareFacebook;
@synthesize shareMail;
@synthesize labelYear;
@synthesize showImage;
@synthesize showPlot;
@synthesize showUrl;
@synthesize pkb;

- (void) checkAuth
{
    if ([pkb.sharer facebookIsAuthorized]) [self.shareFacebook setImage:[UIImage imageNamed:@"face"] forState:UIControlStateNormal];
    else [self.shareFacebook setImage:[UIImage imageNamed:@"faceDisabled"] forState:UIControlStateNormal];
    
    if ([pkb.sharer twitterIsAuthorized]) [self.shareTwitter setImage:[UIImage imageNamed:@"twitter"] forState:UIControlStateNormal];
    else [self.shareTwitter setImage:[UIImage imageNamed:@"twitterDisabled"] forState:UIControlStateNormal];
    
    [self.shareMail setImage:[UIImage imageNamed:@"mail"] forState:UIControlStateNormal];
}

- (void) setYear:(NSString *)year
{
    self.labelYear.text = year;
}

- (void) setClassif:(NSString *)classif
{
    self.labelClassification.text = classif;
}

- (void) setGenre:(NSString *)genre
{
    self.labelGenre.text = genre;
}

- (void) setStatus:(NSString *)status
{
    self.labelStatus.text = status;
}

- (void) setTitle:(NSString *)title
{
    self.labelTitle.text = title;
}

-(void)imageRequestSuccesful:(UIImage *)img {
    //NSLog(@"Rebo al delegat la imatge correctament!");
    self.showImage.image = img;
    [UIView animateWithDuration:0.6
                     animations:^{ 
                         showImage.alpha = 1;
                     }];
}

-(void)imageRequestUnsuccesful:(NSError *)error {
    //NSLog(@"NO Rebo al delegat la imatge!");
}

- (void) setImage:(NSURL *)imageUrl
{

    imageURL = imageUrl.absoluteString;
    UIImage *img = [imgCnt requestImage:imageUrl];
    if (img) {
        //NSLog(@"Image already in cache: origin 3");
        self.showImage.image = img;
        self.showImage.alpha = 1.0;
    }
}

- (void) sendAuthNotification
{
    [self checkAuth];
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        pkb.sharer.delegate = self;
        imgCnt = [[UCImageCache alloc] init];
        imgCnt.delegate = self;
        imageURL = @"http://undeadcoders.com/img/logopopKorn.png";
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void) initSharer:(id <UCSharerDelegate>)givenDelegate
{
    pkb.sharer.delegate = givenDelegate;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    [self checkAuth];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (IBAction)shareTwitterPressed:(id)sender {
    if ([pkb.sharer twitterIsAuthorized])
        [pkb.sharer postTwitter:[NSString stringWithFormat:NSLocalizedString(@"I am following %@! #popKorn",nil),self.labelTitle.text]];
    else [pkb.sharer authorizeTwitter];
}

- (IBAction)shareFacebookPressed:(id)sender {
    if ([pkb.sharer facebookIsAuthorized]) {
        NSLog(@"[ShowInfo] publico al face amb titol: %@ desc: %@ i imatge: %@", [NSString stringWithFormat:NSLocalizedString(@"I am following %@!",nil),self.labelTitle.text],  NSLocalizedString(@"descFace", nil)
              , imageURL);
        NSString *showName = [self.labelTitle.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *link = [NSString stringWithFormat:@"http://www.undeadcoders.com/popkorn/show.php?showName=%@&showUrl=../pkdb/showDbV1_0%@",showName,self.showUrl];
        [pkb.sharer postFacebook:[NSString stringWithFormat:NSLocalizedString(@"I am following %@!",nil),self.labelTitle.text] 
                  andDescription:showPlot
                        andImage:imageURL
                         andLink:link];
    }
    else [pkb.sharer authorizeFacebook];
}

- (IBAction)shareMailPressed:(id)sender {
    [pkb.sharer postMail:[NSString stringWithFormat:NSLocalizedString(@"Hey check out %@!",nil),self.labelTitle.text]  withSubject:@"popKorn"];
}
@end
