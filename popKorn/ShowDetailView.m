//
//  DetailView.m
//  popKorn
//
//  Created by Àlex Vergara on 26/08/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import "ShowDetailView.h"

@implementation ShowDetailView

@synthesize butPlot;
@synthesize butSeasons;
@synthesize show;
@synthesize pkb;
@synthesize showInfo;
@synthesize showPlot;
@synthesize showSeasons;
@synthesize tabImage;
@synthesize plotLoading;
@synthesize plotStatus;
@synthesize seasonsLoading;

@synthesize sivc;
@synthesize sv;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (CGRect) moveFrame:(CGRect)frame x:(int)x
{
    frame.origin.x += x;
    return frame;
}

- (void)loadShowInfoViewController
{
    [self.sivc setTitle:show.name];
    [self.butPlot setTitle:NSLocalizedString(@"plot",nil) forState:UIControlStateNormal];
    [self.butSeasons setTitle:NSLocalizedString(@"seasons",nil) forState:UIControlStateNormal];
    
    NSMutableString *genreTag = [NSLocalizedString(@"genre", nil) mutableCopy];
    BOOL first = YES;
    for (Genre *g in show.genres) {
        if (!first) [genreTag appendString:@", "];
        first = NO;
        [genreTag appendString:NSLocalizedString(g.name, nil)];
    }
    [self.sivc setGenre:genreTag];
    NSString *plot = show.plot;
    sivc.showPlot = plot ? plot : show.name;
    NSString *url = show.url;
    sivc.showUrl = url ? url : @"";

    if (show.lastInfoUpdate) {
        //if (![NSURL URLWithString:show.photoURL]) NSLog(@"Error ve de loadShowInfoViewController");
        [self.sivc setImage:[NSURL URLWithString:show.photoURL]];
        
        NSString *classifTag = NSLocalizedString(@"classification",nil);
        [self.sivc setClassif:[classifTag stringByAppendingString:NSLocalizedString(show.classification,nil)]];
                
        NSString *statusTag = NSLocalizedString(@"status",nil);
        [self.sivc setStatus:[statusTag stringByAppendingString:NSLocalizedString(show.status,nil)]];
    } else {
        NSString *classifTag = NSLocalizedString(@"classification",nil);
        [self.sivc setClassif:[classifTag stringByAppendingString:NSLocalizedString(@"loading",nil)]];
        
        NSString *statusTag = NSLocalizedString(@"status",nil);
        [self.sivc setStatus:[statusTag stringByAppendingString:NSLocalizedString(@"loading",nil)]];
    }
    
    if (show.lastSeasonsUpdate) {
        NSString *yearTag = NSLocalizedString(@"year",nil);
        int year = show.year;

        if (year == 0)
            [self.sivc setYear:[yearTag stringByAppendingString:@"-"]];
        else
            [self.sivc setYear:[yearTag stringByAppendingString:[NSString stringWithFormat:@"%d",show.year]]];
    } else {
        NSString *yearTag = NSLocalizedString(@"year",nil);
        [self.sivc setYear:[yearTag stringByAppendingString:NSLocalizedString(@"loading",nil)]];
    }
    
    [TxDownloadShowInfo downloadShowInfoInBackground:self.show.name withDelegate:self withBrain:self.pkb];
}

- (void)placePlot:(NSString *)plot {
    self.sivc.showPlot = plot;
    self.showPlot.text = plot;
    self.showPlot.contentOffset = CGPointMake(0.0, 1.0);
    self.showPlot.contentOffset = CGPointMake(0.0, 0.0);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = self.show.name;

    
    //boto favorit
    UIImage *favImage = [self.show.isFavorite boolValue] ? [UIImage imageNamed:@"favBarItem"] : [UIImage imageNamed:@"notFavBarItem"];
    UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithImage:favImage
                                                             style:UIBarButtonItemStylePlain
                                                            target:self
                                                            action:@selector(favoriteTapped)];
    self.navigationItem.rightBarButtonItem = item;

    //tab bar
    ShowInfoViewController *sivcb = [[ShowInfoViewController alloc] init];
    sivcb.pkb = self.pkb;
    self.sivc = sivcb;
    [self.sivc initSharer:self];
    [self.showInfo addSubview:self.sivc.view];
    [self loadShowInfoViewController];
    
    //seasons
    self.sv = [[SeasonsView alloc] initWithShow:self.show brain:self.pkb];
    self.sv.navCon = self.navigationController;
    self.showSeasons.dataSource = self.sv;
    self.showSeasons.delegate = self.sv;
    self.showSeasons.frame = [self moveFrame:self.showSeasons.frame x:320];
    self.showSeasons.hidden = NO;
    self.seasonsLoading.frame = [self moveFrame:self.seasonsLoading.frame x:320];
    if (self.show.seasons.count > 0) self.seasonsLoading.alpha = 0.0;
    else                             self.showSeasons.alpha = 0.0;

    //plot
    if (self.show.plot) {
        if ([self.show.plot length] == 0) {
            [self placePlot:NSLocalizedString(@"noPlot", nil)];
            self.plotLoading.alpha = 0.0;
            self.plotStatus.alpha = 0.0;
        }
        else if ([PKSettings translate]) {
            NSString * locale = [[NSLocale preferredLanguages] objectAtIndex:0];
            if ([self.show.plotLocale hasPrefix:locale]) {
                [self placePlot:self.show.plotLocalized];
                self.plotLoading.alpha = 0.0;
                self.plotStatus.alpha = 0.0;
            }
            else {
                UCTranslate *tr = [[UCTranslate alloc] init];
                tr.delegate = self;
                self.plotStatus.text = NSLocalizedString(@"translating",nil);
                [tr bingTranslate:self.show.plot toLanguage:locale];
            }
        }
        else {
            [self placePlot:self.show.plot];
            self.plotLoading.alpha = 0.0;
            self.plotStatus.alpha = 0.0;
        }
    }
    else {
        self.plotStatus.text = NSLocalizedString(@"loading info",nil);
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.showSeasons reloadData];
}

- (void)favoriteTapped
{
    [self.pkb.context refreshObject:self.show mergeChanges:NO];

    BOOL isFavorite = [self.show.isFavorite boolValue];

    [self.pkb setShow:self.show asFavorite:!isFavorite];
    
    self.navigationItem.rightBarButtonItem.image = isFavorite ? [UIImage imageNamed:@"notFavBarItem"] : [UIImage imageNamed:@"favBarItem"];
    
    if(isFavorite)
        [[TKAlertCenter defaultCenter]postAlertWithMessage:NSLocalizedString(@"noFavoriteAlert", nil)
                                                     image:[UIImage imageNamed:@"notFavBarItem"]];
    else
        [[TKAlertCenter defaultCenter]postAlertWithMessage:NSLocalizedString(@"favoriteAlert", nil)
                                                     image:[UIImage imageNamed:@"favBarItem"]];

    //PopKornNotificator *pkn = [[PopKornNotificator alloc]init];
    //pkn.brain = self.pkb;
    //[pkn scheduleNotifications];
    
}

- (void)viewDidUnload
{
    [self setPlotStatus:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (CGRect) moveFrameToPosition:(CGRect)frame x:(int)x
{
    frame.origin.x = x;
    return frame;
}

- (IBAction)buttonPlot:(id)sender {
    self.tabImage.image = [UIImage imageNamed:@"tabsPlot"];

    //necessari o de vegades no es mostra el plot
    self.showPlot.contentOffset = CGPointMake(0.0, 1.0);
    self.showPlot.contentOffset = CGPointMake(0.0, 0.0);

    [UIView animateWithDuration:0.4
                     animations:^{
                         self.showPlot.frame = [self moveFrameToPosition:self.showPlot.frame x:0];
                         self.plotStatus.frame = [self moveFrameToPosition:self.plotStatus.frame x:0];
                         self.plotLoading.frame = [self moveFrameToPosition:self.plotLoading.frame x:150];
                         self.showSeasons.frame = [self moveFrameToPosition:self.showSeasons.frame x:320];
                         self.seasonsLoading.frame = [self moveFrameToPosition:self.seasonsLoading.frame x:470];
                     }
     ];
}

- (IBAction)buttonSeasons:(id)sender {
    self.tabImage.image = [UIImage imageNamed:@"tabsSeasons"];
    
    //necessari o de vegades no es mostra el plot
    self.showPlot.contentOffset = CGPointMake(0.0, 1.0);
    self.showPlot.contentOffset = CGPointMake(0.0, 0.0);

    [UIView animateWithDuration:0.4
                     animations:^{
                         self.showPlot.frame = [self moveFrameToPosition:self.showPlot.frame x:-320];
                         self.plotStatus.frame = [self moveFrameToPosition:self.plotStatus.frame x:-320];
                         self.plotLoading.frame = [self moveFrameToPosition:self.plotLoading.frame x:-170];
                         self.showSeasons.frame = [self moveFrameToPosition:self.showSeasons.frame x:0];
                         self.seasonsLoading.frame = [self moveFrameToPosition:self.seasonsLoading.frame x:150];
                     }
     ];
}


- (void) episodeListRequestSuccessful
{
    [self.pkb.context refreshObject:self.show mergeChanges:NO];
    [self.sv loadSeasons];
    [self.showSeasons reloadData];
    
    [UIView animateWithDuration:0.4
                     animations:^{
                         self.seasonsLoading.alpha = 0.0;
                         self.showSeasons.alpha = 1.0;
                     }
     ];
}

- (void) setYear:(NSString *)year
{
    [self.sivc setYear:year];
}

- (void) setClassif:(NSString *)classif
{
    [self.sivc setClassif:classif];
}

- (void) setGenre:(NSString *)genre
{
    [self.sivc setGenre:genre];
}

- (void) setStatus:(NSString *)status
{
    [self.sivc setStatus:status];
}

- (void) setTitle:(NSString *)title
{
    [self.sivc setTitle:title];
}

- (void) setImage:(NSURL *)imageUrl
{
    //if (!imageUrl) NSLog(@"Error ve de [showDetailView setImage]");
    //else
        [self.sivc setImage:imageUrl];
}

- (void) setPlot:(NSString *)plot
{
    //NSLog(@"%@",plot);
    self.showPlot.alpha = 0.0;
    
    if ([PKSettings translate] && [plot length]) {
        NSString * locale = [[NSLocale preferredLanguages] objectAtIndex:0];
        UCTranslate *tr = [[UCTranslate alloc] init];
        tr.delegate = self;
        self.plotStatus.text = NSLocalizedString(@"translating",nil);
        [tr bingTranslate:plot toLanguage:locale];
        return;
    }

    if ([plot length] == 0) [self placePlot:NSLocalizedString(@"noPlot",nil)];
    else [self placePlot:plot];
    
    [UIView animateWithDuration:0.4
                     animations:^{
                         self.showPlot.alpha = 1.0;
                         self.plotLoading.alpha = 0.0;
                         self.plotStatus.alpha = 0.0;
                     }
     ];
}

-(void)translationOperationDidFailWithError:(NSError *)error {
    NSLog(@"Error al traduir %@", self.show.plot);
}

-(void)translationOperationDidFinishTranslatingText:(NSString *)result {
    [self.pkb.context refreshObject:self.show mergeChanges:NO];
    self.show.plotLocalized = result;
    self.show.plotLocale = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    NSError* mocError = nil;
    [self.pkb.context save:&mocError];
    if (mocError) {
        NSLog(@"ERROR al guardar context en -(void)translationOperationDidFinishTranslatingText:(NSString *)result, codi: %@",[mocError localizedDescription]);
    }
    
    [self placePlot:result];
    
    [UIView animateWithDuration:0.4
                     animations:^{
                         self.showPlot.alpha = 1.0;
                         self.plotLoading.alpha = 0.0;
                         self.plotStatus.alpha = 0.0;
                     }
     ];
}

- (void) requestUnsuccessful:(NSError *)error
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error",nil) message:NSLocalizedString(@"error504",nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    NSLog(@"%@",NSLocalizedString(@"error504", nil));
}

- (void) episodeListRequestUnsuccessful
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error",nil) message:NSLocalizedString(@"error503",nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    NSLog(@"%@",NSLocalizedString(@"error503", nil));
}

- (void) sendAuthNotification
{
    [self.sivc sendAuthNotification];
}

@end
