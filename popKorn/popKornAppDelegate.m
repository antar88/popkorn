//
//  popKornAppDelegate.m
//  popKorn
//
//  Created by Àlex Vergara on 04/09/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import "popKornAppDelegate.h"

@implementation popKornAppDelegate

@synthesize window = _window;
@synthesize managedObjectContext = __managedObjectContext;
@synthesize managedObjectModel = __managedObjectModel;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;

@synthesize facebook;

@synthesize brain;

static NSString* kAppId = @"179010468868244";

- (void)fbDidLogin {
    NSLog(@"Loguejat correctament!");
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[facebook accessToken] forKey:@"FBAccessTokenKey"];
    [defaults setObject:[facebook expirationDate] forKey:@"FBExpirationDateKey"];
    [defaults synchronize];
    [self.brain.sharer sendAuthNotification];
}

- (BOOL)handlePopKornUrl:(NSString *)url {
    NSLog(@"PopKorn URL!!");
    brain = [[PopKornBrain alloc] init];
    brain.context = self.managedObjectContext;
    brain.uiKache = [[UIKache alloc] initInManagedObjectContext:brain.context];    PopKornTabBarController *pktbc = [[PopKornTabBarController alloc] initWithPopKornBrain:brain];
    pktbc.selectedIndex = 1;
    
    
    TVshow *show = [TVshow tvShowWithName:url inContext:brain.context];
    ShowDetailView *dv = [[ShowDetailView alloc] init];
    dv.show = show;
    dv.pkb = brain;
    
    [((UINavigationController*) pktbc.selectedViewController) pushViewController:dv animated:NO];

    self.window.rootViewController = pktbc;
    
    //[pktbc.selectedViewController.navigationController pushViewController:dv animated:YES];
    
    return YES;
}

// Pre iOS 4.2 support
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    NSLog(@"Application openURL pre 4.2 ... %@", url);
    if ([[[url absoluteString] substringToIndex:7] compare:@"popkorn"] == NSOrderedSame) {
        return [self handlePopKornUrl:[[url absoluteString] substringFromIndex:10]];
    }
    return [facebook handleOpenURL:url]; 
}

// For iOS 4.2+ support
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    NSLog(@"Application openURL ... %@", url);
    if ([[[url absoluteString] substringToIndex:7] compare:@"popkorn"] == NSOrderedSame) {
        return [self handlePopKornUrl:[[url absoluteString] substringFromIndex:10]];
    }
    return [facebook handleOpenURL:url]; 
}


-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    
    if([viewController.tabBarItem.title compare: NSLocalizedString(@"calendar", nil)] == NSOrderedSame) cal.badgeValue = nil;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //[self showDbToPlistCompact];
    
    // Override point for customization after application launch.
    NSLog(@"Obrim la app...");
    brain = [[PopKornBrain alloc] init];
    brain.context = self.managedObjectContext;
    brain.uiKache = [[UIKache alloc] initInManagedObjectContext:brain.context];
    //[TxDownloadFeatured downloadFeaturedInContext:self.brain.context];
    
    
    //FACEBOOK 
    facebook = [[Facebook alloc] initWithAppId:kAppId andDelegate:self];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"FBAccessTokenKey"] 
        && [defaults objectForKey:@"FBExpirationDateKey"]) {
        NSLog(@"Facebook te preferencies ja guardades!");
        facebook.accessToken = [defaults objectForKey:@"FBAccessTokenKey"];
        facebook.expirationDate = [defaults objectForKey:@"FBExpirationDateKey"];
    }
    else NSLog(@"Facebook NO té preferencies ja guardades!");
    
    NSDate *lastUpdate = [PKSettings lastUpdate];
    if (!lastUpdate) {
        welcomeScreenViewController *flvc = [[welcomeScreenViewController alloc] init];
        [TxDownloadShowList downloadShowListInContext:self.brain.context withDelegate:flvc];
        brain.sharer.delegate = flvc;
        flvc.brain = self.brain;
        self.window.rootViewController = flvc;
        [UCImageCache deleteCache];
    }
    else {
        //[self initialLog];
        
        PopKornTabBarController *pktbc = [[PopKornTabBarController alloc] initWithPopKornBrain:brain];
        cal = [UITabBarItem alloc];
        cal = [pktbc.tabBar.items objectAtIndex:3];
        pktbc.delegate = self;
        //[[pktbc.tabBar.items objectAtIndex:2] setBadgeValue:@"4"];
        
        self.window.rootViewController = pktbc;

        NSTimeInterval interval = [lastUpdate timeIntervalSinceNow] / (60*60*24);
        if (interval < -1) {
            NSLog(@"[popKornAppDelegate] More than 24h since last update, updating...");
            [TxDownloadShowList downloadShowListInContext:self.brain.context withDelegate:nil];
            [TxDownloadFeatured downloadFeaturedInContext:self.brain.context];
            
            //actualitzant la informació dels favorits
            NSManagedObjectContext *moc = [[NSManagedObjectContext alloc] init];
            [moc setPersistentStoreCoordinator:self.persistentStoreCoordinator];
            NSFetchRequest *request = [[NSFetchRequest alloc] init];
            request.entity = [NSEntityDescription entityForName:@"TVshow" inManagedObjectContext:moc];
            request.predicate = [NSPredicate predicateWithFormat:@"isFavorite = %@",[NSNumber numberWithBool:YES]];
            NSLog(@"[popKornAppDelegate] Fetching fav shows in context %@....",moc);
            NSArray *favShows = [moc executeFetchRequest:request error:nil];
            NSLog(@"[popKornAppDelegate] Done");
            for (TVshow *show in favShows) {
                [TxDownloadShowInfo downloadShowInfoInBackground:show.name withDelegate:nil withBrain:self.brain];
            }
            NSLog(@"[popKornAppDelegate] Update complete");
        }
        NSLog(@"[popKorn Delegate] surto de DidFinishLaunching");
    }
    
    
    Class cls = NSClassFromString(@"UILocalNotification");
	if (cls) {
		UILocalNotification *notification = [launchOptions objectForKey:
                                             UIApplicationLaunchOptionsLocalNotificationKey];
		
		if (notification) {
			NSString *reminderText = [notification.userInfo objectForKey:@"kRemindMeNotificationDataKey"];
			[(PopKornTabBarController *)self.window.rootViewController showReminder:reminderText];
		}
	}
	
    if(application.applicationIconBadgeNumber !=0) cal.badgeValue = [NSString stringWithFormat:@"%d",application.applicationIconBadgeNumber];
    else cal.badgeValue = nil;
	application.applicationIconBadgeNumber = 0;
    
    PopKornNotificator *popNoti = [[PopKornNotificator alloc] init];
    popNoti.brain = self.brain;
    [popNoti scheduleNotifications];
    
    
    [self.window makeKeyAndVisible];
    //[TestFlight takeOff:@"c509a4fac925e34c0688d44aee3c05e9_Mjk4MDYyMDExLTA5LTE4IDEyOjA3OjU5LjI3MTgxNw"];
    /*
     UILocalNotification *localNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey]; //notificacions
     
     if (localNotification) [[MKLocalNotificationsScheduler sharedInstance] handleReceivedNotification:localNotification]; //notificacions
     
     [[MKLocalNotificationsScheduler sharedInstance] clearBadgeCount]; //notificacions
     */
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
    if(application.applicationIconBadgeNumber !=0) cal.badgeValue = [NSString stringWithFormat:@"%d",application.applicationIconBadgeNumber];
    else cal.badgeValue = nil;
    application.applicationIconBadgeNumber = 0;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}



- (void)awakeFromNib
{
    /*
     Typically you should set up the Core Data stack here, usually by passing the managed object context to the first view controller.
     self.<#View controller#>.managedObjectContext = self.managedObjectContext;
     */
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil)
    {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error])
        {
            /*
             Replace this implementation with code to handle the error appropriately.
             
             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
             */
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - Core Data stack

/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *)managedObjectContext
{
    if (__managedObjectContext != nil)
    {
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil)
    {
        __managedObjectContext = [[NSManagedObjectContext alloc] init];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return __managedObjectContext;
}

/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created from the application's model.
 */
- (NSManagedObjectModel *)managedObjectModel
{
    if (__managedObjectModel != nil)
    {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"popKorn" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];    
    return __managedObjectModel;
}

/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (__persistentStoreCoordinator != nil)
    {
        return __persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"popKorn.sqlite"];
    
    NSError *error = nil;
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error])
    {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter: 
         [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        //[TestFlight passCheckpoint:@"Error carregant la base de dades"];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error",nil) message:NSLocalizedString(@"error400",nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        NSLog(@"%@",NSLocalizedString(@"error400", nil));
        //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        //abort();
    }    
    
    //[TestFlight passCheckpoint:@"Base de dades carregada amb èxit"];
    return __persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

/**
 Returns the URL to the application's Documents directory.
 */
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
	
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive) {
        // Application was in the foreground when notification was delivered.
        if([notification.fireDate compare:[[NSDate alloc]init]] == NSOrderedDescending || [notification.fireDate compare:[[NSDate alloc]init]] == NSOrderedSame) {
            NSString *reminderText = [notification.userInfo objectForKey:@"kRemindMeNotificationDataKey"];
            [(PopKornTabBarController *)self.window.rootViewController showReminder:reminderText];
        }
        else [[UIApplication sharedApplication]cancelLocalNotification:notification];
        application.applicationIconBadgeNumber = 0;
    }
    
}
/*
 - (void)application:(UIApplication *)app didReceiveLocalNotification:(UILocalNotification *)localNotification { //notificacions
 
 if (app.isFirstResponder ) [[MKLocalNotificationsScheduler sharedInstance] handleReceivedNotification:localNotification]; //notificacions
 
 
 } */


/**
 * Called when the user dismissed the dialog without logging in.
 */
- (void)fbDidNotLogin:(BOOL)cancelled {
    NSLog(@"Sessio Not Login Cancelled: %d!", (int) cancelled);
}

/**
 * Called after the access token was extended. If your application has any
 * references to the previous access token (for example, if your application
 * stores the previous access token in persistent storage), your application
 * should overwrite the old access token with the new one in this method.
 * See extendAccessToken for more details.
 */
- (void)fbDidExtendToken:(NSString*)accessToken
               expiresAt:(NSDate*)expiresAt {
    NSLog(@"Extensio AccessToken!");
}

/**
 * Called when the user logged out.
 */
- (void)fbDidLogout {
    NSLog(@"Logout!");
}

/**
 * Called when the current session has expired. This might happen when:
 *  - the access token expired
 *  - the app has been disabled
 *  - the user revoked the app's permissions
 *  - the user changed his or her password
 */
- (void)fbSessionInvalidated {
    NSLog(@"Sessio invalidada!");
}

- (void)request:(FBRequest *)request didFailWithError:(NSError *)error {
    NSLog(@"%@", [error localizedDescription]);
    NSLog(@"Err details: %@", [error description]);
};



@end
