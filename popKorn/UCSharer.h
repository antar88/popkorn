//
//  UCSharer.h
//  shareTest
//
//  Created by Àlex Vergara on 01/09/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SHKTwitter.h"
#import "SHKMail.h"
#import "FBConnect.h"
#import "CustomTabBarItem.h"



@protocol UCSharerDelegate <NSObject>
@optional
- (void) sendAuthNotification;
- (void) presentModalViewController:(UIViewController *)modalViewController animated:(BOOL)animated;
- (void) dismissModalViewControllerAnimated:(BOOL)animated;
@end

@interface UCSharer : NSObject <MFMailComposeViewControllerDelegate, FBDialogDelegate> {
    SHKTwitter *twitter;
   // Facebook *facebook;
    BOOL ios5;
    id<UCSharerDelegate> delegate;
    BOOL fbaut;
}

@property (strong) id<UCSharerDelegate> delegate;
@property BOOL ios5;
//@property (nonatomic, retain) Facebook *facebook;

- (void) sendAuthNotification;

- (BOOL) twitterIsAuthorized;
- (void) authorizeTwitter;
- (void) postTwitter:(NSString *)status;

- (BOOL) facebookIsAuthorized;
- (void) authorizeFacebook;
- (void) postFacebook:(NSString *)status andDescription:(NSString *)desc andImage:(NSString *)imageURL;
- (void) postFacebook:(NSString *)status andDescription:(NSString *)desc andImage:(NSString *)imageURL andLink:(NSString *)url;

- (BOOL) mailIsAuthorized;
- (void) authorizeMail;
- (void) postMail:(NSString *)status withSubject:(NSString *)subject;
- (void) postMail:(NSString *)status withSubject:(NSString *)subject toMail:(NSString *)mail;

@end
