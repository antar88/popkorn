//
//  help01ViewController.m
//  popKorn
//
//  Created by Àlex Vergara Nebot on 27/10/11.
//  Copyright (c) 2011 Apple. All rights reserved.
//

#import "Welcome07ViewController.h"

@implementation Welcome07ViewController

@synthesize facebookButton;
@synthesize twitterButton;
@synthesize label;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Sharing

- (void)checkAuth
{
    UIImage *twitterImage = brain.sharer.twitterIsAuthorized ? [UIImage imageNamed:@"twitterChecked"] : [UIImage imageNamed:@"twitterUnchecked"];
    [self.twitterButton setImage:twitterImage forState:UIControlStateNormal];
    UIImage *facebookImage = brain.sharer.facebookIsAuthorized ? [UIImage imageNamed:@"facebookChecked"] : [UIImage imageNamed:@"facebookUnchecked"];
    [self.facebookButton setImage:facebookImage forState:UIControlStateNormal];
}

- (IBAction)facebookPressed:(id)sender {
    if (!brain.sharer.facebookIsAuthorized)
        [brain.sharer authorizeFacebook];
    else [facebookButton setImage:[UIImage imageNamed:@"facebookChecked"] forState:UIControlStateNormal];
}

- (IBAction)twitterPressed:(id)sender {
    if (!brain.sharer.twitterIsAuthorized)
        [brain.sharer authorizeTwitter];
    else [twitterButton setImage:[UIImage imageNamed:@"twitterChecked"] forState:UIControlStateNormal];
}

#pragma mark - Sharing delegate

- (void)sendAuthNotification
{
    [self checkAuth];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    brain.sharer.delegate = self;
    self.label.text = NSLocalizedString(@"welcomeSocial", nil);
    [self checkAuth];
}

- (void)viewDidUnload
{
    [self setLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
