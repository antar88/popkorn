//
//  TxDownloadShowInfo.h
//  popKorn
//
//  Created by Àlex Vergara Nebot on 27/02/12.
//  Copyright (c) 2012 Undead Coders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PopKornBrain.h"

@class PopKornBrain;

@interface TxDownloadShowInfo : NSObject

+ (void) downloadShowInfoInBackground:(NSString *)showName withDelegate:(id<tvShowDownloadDelegate>)givenDelegate withBrain:(PopKornBrain *)brain;

@end
