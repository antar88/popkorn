//
//  FeaturedShow.h
//  popKorn
//
//  Created by Àlex Vergara Nebot on 15/02/12.
//  Copyright (c) 2012 Undead Coders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "TVshow.h"

@class TVshow;

@interface FeaturedShow : NSManagedObject

@property (nonatomic, retain) NSNumber * position;
@property (nonatomic, retain) TVshow *show;

+ (FeaturedShow *) featuredShowInManagedObjectContext:(NSManagedObjectContext *)context withName:(NSString *)name andPosition:(NSNumber *)position;

@end
