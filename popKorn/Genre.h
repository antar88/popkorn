//
//  Category.h
//  popKorn
//
//  Created by Àlex Vergara on 28/09/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TVshow;

#pragma mark - Interface
@interface Genre : NSManagedObject {

}

#pragma mark - Properties
@property (nonatomic, strong) NSString * localizedName;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSSet *shows;

#pragma mark - Initializers
+ (Genre *) genreWithName:(NSString *)givenName inContext:(NSManagedObjectContext *)context;

@end


#pragma mark - Core Data Generated Accessors
@interface Genre (CoreDataGeneratedAccessors)

- (void)addShowsObject:(TVshow *)value;
- (void)removeShowsObject:(TVshow *)value;
- (void)addShows:(NSSet *)values;
- (void)removeShows:(NSSet *)values;

@end
