//
//  CatShowsViewController.m
//  popKorn
//
//  Created by Àlex Vergara on 23/08/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import "CategoriesViewController.h"

@implementation CategoriesViewController

@synthesize pkb;
@synthesize table;

- initWithPopKornBrain:(PopKornBrain *)brain
{
    self = [super init];
    if (self) {
        CustomTabBarItem *tabItem = [[CustomTabBarItem alloc] initWithTitle:NSLocalizedString(@"genres", nil) image:[UIImage imageNamed:@"genres"] tag:0];
        tabItem.customHighlightedImage=[UIImage imageNamed:@"orangeGenres"];
        self.tabBarItem = tabItem;
        self.title = NSLocalizedString(@"genres", nil);

        self.pkb = brain;
    }
    return self;
}

#pragma mark - Table ops

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.pkb.uiKache.genres count] +1; 
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"GenreCell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
	}
    
    if (indexPath.row == 0) cell.textLabel.text = NSLocalizedString(@"allTvShows", nil);
    else cell.textLabel.text = [self.pkb.uiKache.genres objectAtIndex:indexPath.row-1];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    //per canviar el color de la cell quan es selecciona
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIColor orangeColor]];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Genre *genre = nil;
    if (indexPath.row) {
        NSString *name = [self.pkb.uiKache.genres objectAtIndex:indexPath.row-1];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        request.entity = [NSEntityDescription entityForName:@"Genre" inManagedObjectContext:self.pkb.context];
        request.predicate = [NSPredicate predicateWithFormat:@"localizedName = %@",name];
        NSError *error = nil;
        NSLog(@"[CategoriesViewController] Fetching genre in context %@....",self.pkb.context);
        genre = (Genre *) [[self.pkb.context executeFetchRequest:request error:&error] lastObject];
        NSLog(@"[CategoriesViewController] Done");
    }
    CatShowsViewController *csvc = [[CatShowsViewController alloc] init];
    csvc.genre = genre;
    csvc.pkb = self.pkb;
    csvc = [csvc initInManagedObjectContext:self.pkb.context];
    [self.navigationController pushViewController:csvc animated:YES];
}

#pragma mark - Auxiliary ops

- (void) loadGenres {
    [self.pkb.uiKache loadGenresFromManagedObjectContext:self.pkb.context];
}

#pragma mark - View lifecycle

- (void)viewWillAppear:(BOOL)animated {
    //[self loadGenres];
    [self.table reloadData];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)viewDidUnload {
    [self setTable:nil];
    [super viewDidUnload];
}
@end
