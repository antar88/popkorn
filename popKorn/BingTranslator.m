//
//  BingTranslator.m
//  popKorn
//
//  Created by Antar Moratona Franco on 28/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "BingTranslator.h"

@implementation BingTranslator
#define URL_STRING @"http://api.microsofttranslator.com/v2/Http.svc/Translate?appId=E6F3EB7CF7C29D8E3BF8516889272B52063F1084&text="
#define client @"c2a0d06f-e43f-473b-bf81-5198940a3cd4"
#define clientSecret @"IoSYX1udpuHipj2/BqVqfXly/wGE98mAiiztcImAzYM="

@synthesize delegate;

-(id)initWithText:(NSString *) textATraduir {
    self = [self init];
    text = textATraduir;
    return self;
}

-(void) setText:(NSString *) t {
    text = t;
}

-(NSURL *) getUrlto:(NSString *) toL {
    NSString* u = [URL_STRING stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]; 
    
    NSString* stringURL = [NSString stringWithFormat:@"%@%@&from=en&to=%@",u, text, toL];
    
    NSString* webStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    return [NSURL URLWithString:webStringURL];
}

- (NSString *) translateSync:(NSString *) to {
    
    NSURL* urlString = [self getUrlto:to];
    //NSLog(@"URL per a la traduccio: %@", urlString);
    NSString *textTraduit;
    
    NSURLRequest* request = [NSURLRequest requestWithURL: urlString cachePolicy: NSURLRequestReloadIgnoringCacheData timeoutInterval: 60.0];
    
    NSURLResponse* response; NSError* error;
    
    NSData* data = [NSURLConnection sendSynchronousRequest: request returningResponse: &response error: &error];
    if (data == nil) {
        NSLog(@"Could not connect to the translator server: !%@! %@", urlString, [error description]);
    } else {
        NSString* contents = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSRange match;
        match = [contents rangeOfString: @">"];
        contents = [contents substringFromIndex: match.location+1];
        
        match = [contents rangeOfString: @"<"];
        textTraduit = [contents substringWithRange: NSMakeRange (0, match.location)];
        return textTraduit;
    }
    return nil;
}

-(void) translate:(NSString *) to {
    NSURL* urlString = [self getUrlto:to];
    
    //NSLog(@"BING TRANSLATE WEB = %@", urlString);
    
    NSURLRequest *request = [NSURLRequest requestWithURL: urlString cachePolicy: NSURLRequestReloadIgnoringCacheData timeoutInterval: 60.0];

    NSURLConnection *conn = [NSURLConnection alloc];
    
    [conn initWithRequest:request delegate:self startImmediately:YES];

}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [delegate bingTranslationdidFailWithError:error];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    NSString* contents = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
    NSRange match;
    match = [contents rangeOfString: @">"];
    contents = [contents substringFromIndex: match.location+1];
    
    match = [contents rangeOfString: @"<"];
    NSString *textTraduit = [contents substringWithRange: NSMakeRange (0, match.location)];

    [delegate bingTranslationSuccess:textTraduit];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
}





@end
