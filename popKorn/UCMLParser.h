//
//  UCMLParser.h
//  UCInternetUtils
//
//  Created by Àlex Vergara on 11/08/11.
//  Copyright 2011 Àlex Vergara. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma UCMLParser Interface
@interface UCMLParser : NSObject {
@private
    NSString *data;
    int index;
}

#pragma Mark - Initializers
- (id) initWithString:(NSString *)dataToParse;
- (id) initWithURL:(NSString *)url;

#pragma Mark - Consultors
/* returns true if index has reached end of data */
- (BOOL) eof;

#pragma Mark - Methods
/* places index at the end of the latest occurrence of subString */
- (void) goToNext:(NSString *)subString;

/* places index at the end of the latest occurrence of subString */
- (void) goToLast:(NSString *)subString;

/* places index at next '>' character */
- (void) skipTag;

#pragma Mark - Functions
/* returns substring from index to the next '>' character */
- (NSString *) getTag;

/* returns substring from index to the next '<' character */
- (NSString *) getNonTag;

/* returns substring from index to the next occurrence of subString */
- (NSString *) readUntil:(NSString *)subString;

/* returns substring from index to the next occurrence of subString,
    but does not move index to eof if subString is not found */
- (NSString *) readUntilButDontMoveIndexIfNotFound:(NSString *)subString;

/* returns substring from next occurrence of prefix to next occurrence of suffix */
- (NSString *) readBetween:(NSString *)prefix and:(NSString *)suffix;

/* returns substring from index to the end of data */
- (NSString *) readRest;

/* returns data with no -ML tags, endlines, or multiple contiguous space characters */
- (NSString *) plainText;

#pragma Mark - Other Parsing Utils
/* returns YES when subString is a substring of string, returns NO otherwise */
+ (BOOL) string:(NSString *)string contains:(NSString *)subString;

/* initializes and returns an NSDate instance, given a date in the following formats:
 25/12/2011
 25/Dec/2011
 WARNING: may and will crash if string is not properly formatted */
+ (NSDate *) dateFromString:(NSString *)string;

/* prints out the given date in the format specified in the locale preferences */
+ (NSString *) stringFromDate:(NSDate *) date;

/* replaces all occurrences of toChange for substring toAdd in given string */
+ (void) donadaMutableString:(NSMutableString *)string canvia:(NSString *)toChange per:(NSString *)toAdd;

@end
