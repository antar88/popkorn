//
//  Season.h
//  popKorn
//
//  Created by Àlex Vergara on 23/08/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Episode.h"

#define seasonSeen 2
#define seasonHalfSeen 1
#define seasonUnseen 0

@class Episode, TVshow;

#pragma Mark - Season Interface
@interface Season : NSManagedObject

#pragma Mark - Properties
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * showName;
@property (nonatomic, strong) TVshow *show;
@property (nonatomic, strong) NSSet *episodes;
@property (readonly) int seen;
@property (readonly) int seenAndEmitted;
@property (readonly) int unseenEpisodes;
@property (readonly) int year;
@end

#pragma Mark - CoreData Generated Accessors
@interface Season (CoreDataGeneratedAccessors)

- (void)addEpisodesObject:(Episode *)value;
- (void)removeEpisodesObject:(Episode *)value;
- (void)addEpisodes:(NSSet *)values;
- (void)removeEpisodes:(NSSet *)values;

#pragma Mark - Initializers
+ (Season *) seasonWithName:(NSString *)givenName show:(NSString *)givenShow inContext:(NSManagedObjectContext *)context;
+ (BOOL) seasonExists:(NSString *)givenName show:(NSString *)givenShow inContext:(NSManagedObjectContext *)context;
+ (Season *) createSeasonWithName:(NSString *)givenName withShow:(NSString*)showName inContext:(NSManagedObjectContext *)context;

#pragma Mark - Consultors
- (int) seen;
- (int) seenAndEmitted;
- (int) unseenEpisodes;

@end
