//
//  WelcomeViewController.h
//  popKorn
//
//  Created by Àlex Vergara Nebot on 27/10/11.
//  Copyright (c) 2011 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopKornBrain.h"

@interface WelcomeViewController : UIViewController {
    PopKornBrain *brain;
    UIViewController *superview;
}

@property (nonatomic, strong) PopKornBrain *brain;
@property (nonatomic, strong) UIViewController *superview;

- (id)initWithBrain:(PopKornBrain *)givenBrain superview:(UIViewController *)controller;

@end
