//
//  UCMLParser.h
//  UCInternetUtils
//
//  Created by Àlex Vergara on 11/08/11.
//  Copyright 2011 Àlex Vergara. All rights reserved.
//

#import "UCMLParser.h"

@implementation UCMLParser

#pragma Mark - Initializers
- (id)init
{
    self = [super init];
    if (self) {
        data = [[NSString alloc] init];
        index = 0;
    }
    
    return self;
}

- (id) initWithString:(NSString *)dataToParse
{
    self = [self init];
    data = dataToParse;
    return self;
}

- (id) initWithURL:(NSString *)url
{
    NSError *downloadError = nil;
    NSString *downloadData = [NSString stringWithContentsOfURL:[NSURL URLWithString:url]
                                                      encoding:NSUTF8StringEncoding
                                                         error:&downloadError];
    if (downloadError) {
        downloadError = nil;
        downloadData = [NSString stringWithContentsOfURL:[NSURL URLWithString:url]
                                                encoding:NSASCIIStringEncoding
                                                   error:&downloadError];
        if (downloadError) return nil;
    }
    return [self initWithString:downloadData];
}

#pragma Mark - Auxiliary private methods
- (void) discardEndLines
{
    for (int j = 0; j + index < data.length; ++j) {
        if ([data compare:@"\n"
                  options:NSLiteralSearch
                    range:NSMakeRange(index+j, 1)] == NSOrderedSame ||
            [data compare:@"\r"
                  options:NSLiteralSearch
                    range:NSMakeRange(index+j, 1)] == NSOrderedSame ||
            [data compare:@"\t"
                  options:NSLiteralSearch
                    range:NSMakeRange(index+j, 1)] == NSOrderedSame) {
                index+= 1;
            }
        else return;
    }
}

+ (NSString *) discardEndLines:(NSString *) string
{
    while ([string hasPrefix:@"\n"] || [string hasPrefix:@"\r"] || [string hasPrefix:@"\t"]) {
        string = [string substringWithRange:NSMakeRange(1,string.length-1)];
    }
    return string;
}

+ (int) month:(NSString *)string
{
    if ([string hasPrefix:@"Jan"]) return 1;
    if ([string hasPrefix:@"Feb"]) return 2;
    if ([string hasPrefix:@"Mar"]) return 3;
    if ([string hasPrefix:@"Apr"]) return 4;
    if ([string hasPrefix:@"May"]) return 5;
    if ([string hasPrefix:@"Jun"]) return 6;
    if ([string hasPrefix:@"Jul"]) return 7;
    if ([string hasPrefix:@"Aug"]) return 8;
    if ([string hasPrefix:@"Sep"]) return 9;
    if ([string hasPrefix:@"Oct"]) return 10;
    if ([string hasPrefix:@"Nov"]) return 11;
    if ([string hasPrefix:@"Dec"]) return 12;
    return [string intValue];
}

#pragma Mark - Consultors
- (BOOL) eof
{
    return index > data.length;
}

#pragma Mark - Methods
- (void) goToNext:(NSString *)subString
{
    for (; index + subString.length < data.length; ++index) {
        if ([data compare:subString
                  options:NSLiteralSearch
                    range:NSMakeRange(index, subString.length)] == NSOrderedSame) {
            index+=subString.length;
            return;
        }
    }
    index = data.length +1; //move index to EOF
}

- (void) goToLast:(NSString *)subString
{
    for (int j = 0; index-j >= 0; ++j) {
        if ([data compare:subString
                  options:NSLiteralSearch
                    range:NSMakeRange(index-j, subString.length)] == NSOrderedSame) {
            index = index - j + subString.length;
            return;
        }
    }
    index = data.length +1; //move index to EOF
}

- (void) skipTag
{
    [self getTag];
}

#pragma Mark - Functions
- (NSString *) getTag
{
    //[self discardEndLines];
    for (int j = 0; j + index < data.length; ++j) {
        if ([data compare:@">"
                  options:NSLiteralSearch
                    range:NSMakeRange(index+j, 1)] == NSOrderedSame) {
            NSString *returnData = [data substringWithRange:NSMakeRange(index, j+1)];
            index+= j+1;
            return [UCMLParser discardEndLines:returnData];
        }
    }
    index = data.length +1; //move index to EOF
    return nil;
}

- (NSString *) getNonTag
{
    //[self discardEndLines];
    for (int j = 0; j + index < data.length; ++j) {
        if ([data compare:@"<"
                  options:NSLiteralSearch
                    range:NSMakeRange(index+j, 1)] == NSOrderedSame) {
            NSString *returnData = [data substringWithRange:NSMakeRange(index, j)];
            index+= j;
            return returnData;
        }
    }
    index = data.length +1; //move index to EOF
    return nil;
}

- (NSString *) readUntil:(NSString *)subString
{
    //[self discardEndLines];
    for (int j = 0; j + index + subString.length < data.length; ++j) {
        if ([data compare:subString
                  options:NSLiteralSearch
                    range:NSMakeRange(index+j, subString.length)] == NSOrderedSame) {
            NSString *returnData = [data substringWithRange:NSMakeRange(index, j)];
            index+= j+subString.length;
            return returnData;
        }
    }
    index = data.length +1; //move index to EOF
    return nil;
}

- (NSString *) readUntilButDontMoveIndexIfNotFound:(NSString *)subString
{
    int dupIndex = index;
    NSString *result = [self readUntil:subString];
    if (!result) index = dupIndex;  //move index from EOF to original position
    return result;
}

- (NSString *) readBetween:(NSString *)prefix and:(NSString *)suffix
{
    [self goToNext:prefix];
    return [self readUntil:suffix];
}

- (NSString *) readRest
{
    return [data substringWithRange:NSMakeRange(index,data.length-index)];
}

- (NSString *) plainText
{
    NSMutableString *textPla = [[NSMutableString alloc]init];
    for(index = 0; index < data.length; ++index) {
        NSString *caracter = [data substringWithRange:NSMakeRange(index, 1)];
        if([caracter compare:@"<"] == NSOrderedSame) {
            [self readUntil:@">"];
            --index;
        }
        else if (![caracter hasPrefix:@"\t"] &&  ![caracter hasPrefix:@"\n"] && ![caracter hasPrefix:@"\r"]) {
            [textPla appendString:caracter];
        }
    }
    return textPla;
}

#pragma Mark - Other Parsing Utils
+ (BOOL) string:(NSString *)string contains:(NSString *)subString
{
    for (int i = 0; i + subString.length < string.length; ++i) {
        if ([string compare:subString
                    options:NSLiteralSearch
                      range:NSMakeRange(i, subString.length)] == NSOrderedSame) {
            return YES;
        }
    }
    return NO;
}

+ (NSDate *) dateFromString:(NSString *)string
{
    if ([string hasPrefix:@"(null)"]) return nil;
    UCMLParser *dateParser = [[UCMLParser alloc] initWithString:string];
    NSString *day = [dateParser readUntil:@"/"];
    NSString *month = [dateParser readUntil:@"/"];
    NSString *year = [dateParser readRest];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:[day intValue]];
    [comps setMonth:[UCMLParser month:month]];
    [comps setYear:[year intValue]];
    [comps setTimeZone: [NSTimeZone timeZoneWithName:@"GMT"]];
    NSCalendar *gregorian =[[NSCalendar alloc]
                            initWithCalendarIdentifier:NSGregorianCalendar];
    return [gregorian dateFromComponents:comps];
}

+ (NSString *) stringFromDate:(NSDate *) date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    return [dateFormatter stringFromDate:date].description;
}

+(void) donadaMutableString:(NSMutableString *)string canvia:(NSString *)toChange per:(NSString *)toAdd {
    NSRange range = [string rangeOfString:toChange options:NSLiteralSearch];
    while (range.length != 0) {
        [string replaceCharactersInRange:range withString:toAdd];
        range = [string rangeOfString:toChange options:NSLiteralSearch];
    }
}

@end