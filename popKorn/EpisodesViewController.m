//
//  EpisodesViewController.m
//  popKorn
//
//  Created by Àlex Vergara on 01/09/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import "EpisodesViewController.h"

@implementation EpisodesViewController

@synthesize pkb;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- initInManagedObjectContext:(NSManagedObjectContext *)context withSeason:(Season *)season
{
    if (self = [super initWithStyle:UITableViewStylePlain]) {
        self.title = NSLocalizedString(@"episodes",nil);
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        request.entity = [NSEntityDescription entityForName:@"Episode" inManagedObjectContext:context];
        request.predicate = [NSPredicate predicateWithFormat:@"season = %@",season];
        request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"code" ascending:YES]];
        
        NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                              managedObjectContext:context
                                                                                sectionNameKeyPath:nil
                                                                                         cacheName:nil];        
        self.fetchedResultsController = frc;
        
        self.titleKey = @"displayName";
        self.subtitleKey = @"localizedDate";
    }
    return self;
}

- (UIImage *)thumbnailImageForManagedObject:(NSManagedObject *)managedObject
{
    Episode *episode = (Episode *) managedObject;
    if ([episode.seen boolValue]) {
        return [UIImage imageNamed:@"seen"];
    }
    return [UIImage imageNamed:@"unSeen"];
}

- (UITableViewCellAccessoryType)accessoryTypeForManagedObject:(NSManagedObject *)managedObject
{
	return UITableViewCellAccessoryDisclosureIndicator;
}

- (void)managedObjectSelected:(NSManagedObject *)managedObject
{
    Episode *episode = (Episode *) managedObject;
    EpisodeDetailView *edv = [[EpisodeDetailView alloc] init];
    edv.episode = episode;
    edv.pkb = self.pkb;
    [self.navigationController pushViewController:edv animated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

@end