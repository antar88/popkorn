//
//  PopKornTabBarController.h
//  popKorn
//
//  Created by Àlex Vergara on 06/09/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PopKornBrain.h"

#import "CategoriesViewController.h"
#import "FeaturedViewController.h"
//#import "AllShowsViewController.h"
#import "FavShowsViewController.h"
#import "AltFavShowsViewController.h"
#import "CalendarViewController.h"
#import "PreferencesViewController.h"

@interface PopKornTabBarController : UITabBarController {
    PopKornBrain *brain;
}

@property (strong) PopKornBrain *brain;

- (id)initWithPopKornBrain:(PopKornBrain *)givenBrain;

- (void)showReminder:(NSString *)text;
- (void)changeBadge:(NSInteger)badgeNum;

@end
