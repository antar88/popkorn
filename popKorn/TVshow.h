//
//  TVshow.h
//  popKorn
//
//  Created by Àlex Vergara on 23/08/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#pragma Mark - Imports
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Season.h"
#import "Genre.h"
#import "UCInternetUtils.h"

#pragma Mark - tvShowDownloadDelegate protocol
@protocol tvShowDownloadDelegate <NSObject>
@required
- (void) setYear:(NSString *)year;
- (void) setClassif:(NSString *)classif;
- (void) setStatus:(NSString *)status;
- (void) setTitle:(NSString *)title;
- (void) setImage:(NSURL *)imageUrl;
- (void) setPlot:(NSString *)plot;
- (void) episodeListRequestSuccessful;
- (void) episodeListRequestUnsuccessful;
- (void) requestUnsuccessful:(NSError *)error;
@end

@interface TVshow : NSManagedObject
#pragma Mark - Protocol
@property (nonatomic, strong) NSSet *    genres;
@property (nonatomic, strong) NSString * classification;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * photoURL;
@property (nonatomic, strong) NSNumber * isFavorite;
@property (nonatomic, strong) NSDate *   lastInfoUpdate;
@property (nonatomic, strong) NSDate *   lastSeasonsUpdate;
@property (nonatomic, strong) NSString * plot;
@property (nonatomic, strong) NSString * plotLocale;
@property (nonatomic, strong) NSString * plotLocalized;
@property (nonatomic, strong) NSString * url;
@property (nonatomic, strong) NSSet *    seasons;
@property (nonatomic, strong) NSString * sectionName;
@property (nonatomic, strong) NSString * status;
@property (readonly) int                 year;
@property (readonly) NSString *          nextEpisode;

#pragma Mark - Initializers
+ (TVshow *) tvShowWithName:(NSString *)givenName inContext:(NSManagedObjectContext *)context;
+ (TVshow *) tvShowWithName:(NSString *)givenName url:(NSString *)givenURL inContext:(NSManagedObjectContext *)context;
+ (TVshow *) createTvShowWithName:(NSString *)givenName url:(NSString *)givenURL inContext:(NSManagedObjectContext *)givenContext;

#pragma Mark - Consultors
- (int) year;
- (NSString *) firstLetterOfName;
- (int) seen;
- (int) unseenEpisodes;
- (NSString *) nextEpisode;

@end

#pragma Mark - CoreData Generated Accessors
@interface TVshow (CoreDataGeneratedAccessors)

- (void) addGenresObject:(NSManagedObject *)value;
- (void) removeGenresObject:(NSManagedObject *)value;
- (void) addGenres:(NSSet *)values;
- (void) removeGenres:(NSSet *)values;

- (void) addSeasonsObject:(NSManagedObject *)value;
- (void) removeSeasonsObject:(NSManagedObject *)value;
- (void) addSeasons:(NSSet *)values;
- (void) removeSeasons:(NSSet *)values;

@end
