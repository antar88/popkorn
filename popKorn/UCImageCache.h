//
//  ImagesController.h
//  popKorn
//
//  Created by Antar Moratona Franco on 21/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UCHTTPRequest.h"
#import "UCMLParser.h"

@protocol imagesControllerDelegate <NSObject>
@required
- (void) imageRequestSuccesful:(UIImage *)img;
- (void) imageRequestUnsuccesful:(NSError *)error;
@end

@interface UCImageCache : NSObject <httpRequestDelegate>

@property (strong) id<imagesControllerDelegate> delegate;
@property (strong) NSMutableDictionary *cache;


- (UIImage *) requestImage:(NSURL *) photoUrl;

+(NSString *) pathCache;

+(NSString *) filePathCache;

+(void) deleteCache;

@end