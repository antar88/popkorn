//
//  PKSettings.h
//  popKorn
//
//  Created by Antar Moratona on 06/09/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PKSettings : NSObject

+(NSDate *) lastUpdate;
+(void) setLastUpdate:(NSDate *)date;
//Configuració
+(void) setDaysBefore:(NSString *) daysBefore;
+(NSString *) daysBefore;
+(void) setDaysAfter:(NSString *) daysAfter;
+(NSString *) daysAfter;
+(void) setTranslate:(BOOL) translate;
+(BOOL) translate;
//notificacions
+(void) setNotis:(BOOL)notis;
+(BOOL) notis;
+(void) setNotisDay:(NSString *) day;
+(NSString *) notisDay;
+(void) setNotisHour:(NSString *) hour;
+(NSString *) notisHour;
+(void) setNotisMinutes:(NSString *) minutes;
+(NSString *) notisMinutes;
+(NSDateComponents *) notisDate;
+(NSString *) notisHourMinutes;


@end
