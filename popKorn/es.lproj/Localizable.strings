/* 
 Localizable.strings
 popKorn
 
 Created by Antar Moratona on 07/09/11.
 Copyright 2011 Undead Coders. All rights reserved.
 */

"welcomeText" = "¡Hola! ¡Parece que esta es la primera vez que utilizas PopKorn! Desliza el dedo hacia la izquierda para ver el tutorial.";
"tabBarHelp" = "Hay 5 pestañas en la app:\nGéneros: Busca series por género.\nTodas: Busca entre todas las series.\nFavoritas: Ver las series favoritas.\nCalendario: Ver los próximos capítulos de tus series favoritas.\nPreferencias: Cambiar las preferencias.";
"welcomeTranslation" = "La mayoría de series y capítulos tienen sinopsis, pero como son sacadas de TVRage están en inglés. Las podemos traducir, pero usando Bing Translate y por lo tanto no serán 100% fiables.";
"welcomeCalendar" = "Hay dos formas de ver los próximos capítulos. Puedes cambiar de la una a la otra con el botón de la izquierda.";
"welcomeNotis" = "Marcar series como favoritas es útil porque así aparecen en el calendario, y recibes notificaciones cuando salen capítulos nuevos, si así lo quieres.";
"showSeen01" = "PopKorn hace más fácil saber qué capítulos has visto y cúales no. Para marcar un capítulo como visto, pulsa este botón en la vista de capítulo.";
"showSeen02" = "Para marcar muchos a la vez, simplemente marca como visto el último capítulo que hayas visto. Entonces se marcarán como vistos todos los anteriores.";
"welcomeSocial" = "Con PopKorn también es fácil compartir qué series te gustan y qué capítulos estás viendo ahora mismo. Inicia sesión tocando los iconos inferiores.";
"welcomeWait" = "Por favor espera mientras la base de datos de series se termina de descargar...";
"welcomeLetsGo" = "¡La base de datos se ha descargado con éxito! ¡Toca el icono de abajo cuando estés listo!";
"letsGo" = "¡Vamos!";

"plot" = "Sinopsis";
"seasons" = "Temporadas";

"year" = "Año: ";
"genre" = "Género: ";
"classification" = "Clasificación: ";
"status" = "Estado: ";
"loading" = "Cargando...";

"genres" = "Géneros";
"allTvShows" = "Todas las series";
"favorites" = "Favoritas";
"episodes" = "Capítulos";
"calendar" = "Calendario";
"allFavorites" = "Todas las favoritas";
"pendingFavorites" = "Pendientes";

"Scripted" = "Actores reales";
"Animation" = "Animación";

"pastEpisodes" = "Sugerencia";
"pastEpisodesDescription" = "¿Quieres marcar también como vistos todos los capítulos anteriores a este?";
"no" = "No";
"yes" = "Sí";

"Canceled/Ended" = "Cancelada/Acabada";
"Final Season" = "Temporada Final";
"In Development" = "Activa";
"Never Aired" = "Nunca Emitida";
"New Series" = "Nueva Serie";
"On Hiatus" = "Pausada";
"Pilot Ordered" = "Piloto Encargado";
"Pilot Rejected" = "Piloto Rechazado";
"Returning Series" = "Activa";
"TBD/On The Bubble" = "Pendiente de Anuncio";

"Soaps" = "Culebrón";
"Family" = "Familia";
"Fantasy" = "Fantasía";
"Thriller" = "Thriller";
"Religion" = "Religión";
"Educational" = "Educacional";
"Medical" = "Médica";
"Crime" = "Crimen";
"Comedy" = "Comedia";
"Discovery/Science" = "Ciencia";
"Pets/Animals" = "Animales";
"Animation General" = "Animación";
"Romance/Dating" = "Romance";
"Talent" = "Talento";
"Children Cartoons" = "Animación Infantil";
"Tech/Gaming" = "Tecnología";
"Horror/Supernatural" = "Terror";
"Anthology" = "Antología";
"Children" = "Infantil";
"Music" = "Música";
"Adventure" = "Aventuras";
"Anime" = "Anime";
"Stop-Motion" = "Stop-Motion";
"Sketch/Improv" = "Gags/Improvisación";
"Literature" = "Literatura";
"Interview" = "Entrevista";
"Sports" = "Deportes";
"Celebrities" = "Celebridades";
"Mystery" = "Misterio";
"Action" = "Acción";
"History" = "Historia";
"How To/Do It Yourself" = "Manuales/DIY";
"Cinema/Theatre" = "Cine/Teatro";
"Puppets" = "Marionetas";
"Drama" = "Drama";
"Automobiles" = "Automóviles";
"Sci-Fi" = "Ciencia-Ficción";
"Teens" = "Adolescentes";
"Adult Cartoons" = "Animación Adulta";
"Super Heroes" = "Súper-Héroes";
"Adult/Porn" = "Adulta/Pornografía";
"Cooking/Food" = "Cocina";
"Current Events" = "Actualidad";
"Dance" = "Baile";
"Financial/Business" = "Negocios";
"Garden/Landscape" = "Jardinería";
"Housing/Building" = "Construcción";
"Lifestyle" = "Estilo de vida";
"Military/War" = "Guerra";
"Politics" = "Política";
"Fashion/Make-up" = "Moda";
"Fitness" = "Fitness";
"Travel" = "Viaje";
"Western" = "Oeste";
"Design/Decorating" = "Diseño";
"Arts & Crafts" = "Arte y Manualidades";

"nextEpisode" = "Próximo capítulo: ";
" (today)" = " (hoy)";
" (yesterday)" = " (ayer)";
" (tomorrow)" = " (mañana)";
" (%d days ago)" = " (hace %d días)";
" (in %d days)" = " (en %d días)";
"in %d days" = "En %d días";
"today" = "Hoy";
"tomorrow" = "Mañana";

"%d episodes" = "%d capítulos";
"%d episodes pending" = "%d capítulos pendientes";

"I am following %@! #popKorn" = "Estoy siguiendo %@! #popKornApp";
"I am following %@!" = "Estoy siguiendo %@!";
"Hey check out %@!" = "Échale un vistazo a %@!";
"I am watching %@ - %@! #popKornApp" = "¡Estoy viendo %@ - %@! #popKornApp";
"I am watching %@ - %@!" = "¡Estoy viendo %@ - %@!";

"pref" = "Preferencias";
"calendar" = "Calendario";
"info" = "Información";
"numDaysAfter" = "Días posteriores a hoy:";
"numDaysBefore" = "Días anteriores a hoy:";
"selectDays" = "Selecciona el numero de días";
"About" = "Sobre PopKorn";

"featured" = "Destacadas";
"top10" = "Top 10";
"ourFavorites" = "Nuestras favoritas";

"language" = "es";
"translate" = "Traducir sinopsis";
"translator" = "Traducir";
"gTranslate" = "(con Google Translate)";
"bTranslate" = "(con Bing Translate)";
"delCache" = "Borrar caché de imágenes";
"delCacheConfirm" = "¿Está seguro que desea borrar la caché de imágenes?"; 

"feedback" = "Haznos saber que le falta o que cambiarías de PopKorn a continuación:";

"Season" = "Temporada";
"Series" = "Temporada";
"Special" = "Especial";
"Movie" = "Película";
"Unnumbered Episode" = "Sin Número";

"listView" = "Lista";
"list" = "Pròximos episodios";
"mon" = "lun";
"tue" = "mar";
"wed" = "mié";
"thu" = "jue";
"fri" = "vie";
"sat" = "sáb";
"sun" = "dom";
"September" = "septiembre";
"October" = "octubre";
"November" = "noviembre";
"December" = "diciembre";
"January" = "enero";
"February" = "febrero";
"March" = "marzo";
"April" = "abril";
"May" = "mayo";
"June" = "junio";
"July" = "julio";
"August" = "agosto";
"de setembre" = "septiembre";
"d’octubre" = "octubre";
"de novembre" = "noviembre";
"de desembre" = "diciembre";
"de gener" = "enero";
"de febrer" = "febrero";
"de març" = "marzo";
"d’abril" = "abril";
"de maig" = "mayo";
"de juny" = "junio";
"de juliol" = "julio";
"d’agost" = "agosto";
"monthView" = "Calendario";

"noti" = "Notificaciones";
"activate" = "Activar?";
"when" = "Cuando?";
"sameDay" = "Mismo día";
"dayAfter" = "Día posterior";
"dayBefore" = "Día anterior";
"showsToday %@: %@" = "¡Hoy emiten %@: %@!";
"showsYesterday %@: %@" = "¡Mañana emiten %@: %@!";
"showsTomorrow %@: %@" = "¡Ayer emitieron %@: %@!";
"notisShowMe" = "Ver";
"ats" = "a las";
"at" = "a la";

"favoriteAlert" = "Favorito añadido!";
"noFavoriteAlert" = "Ya no es favorito";
"seen" = "Lo he visto!";
"notSeen" = "No lo he visto!";

"noPlot" = "Sinopsis no disponible";
"translating" = "Traduciendo...";
"loading info" = "Descargando información...";

"done" = "Hecho";

//CREDITS-LEGAL
"author" = "Código y gráficos por:";
"copyrightTitle" = "Nota de copyright";
"copyright" = "La información en formato texto está sacada de TVRage.com (a no ser que se especifique lo contrario) y está libre de copyright. Las imágenes pertenecen a sus respectivos propietarios. Si encuentras algún material que te pertenece, contáctanos en info@undeadcoders.com";

//ERRORES

"twitterAuth" = "Tienes que iniciar sesión en Twitter desde la aplicación Preferencias";

"error400" = "Error 400: Error cargando la base de datos. Debes borrar y reinstalar la app =(. Por favor notifícanos en info@undeadcoders.com";

"error500" = "Error 500: No hay conexión a Internet o TVRage está caída";
"error503" = "Error 503: No hay conexión a Internet o TVRage está caída";
"error504" = "Error 504: No hay conexión a Internet o TVRage está caída";

//Facebook 
"FaceLogout" = "Desautoriza Facebook";
"shareWithPopKorn" = "¡Comparte tus series favoritas y los capítulos que estás viendo con popKorn!";

// UCInternetUtils Strings:

"Email" = "Email";
"Username" = "Usuario";
"Password" = "Contraseña";
"Follow %@" = "Seguir %@";
"Continue" = "Continuar";
"Share" = "Compartir";
"More..." = "Más...";
"Cancel" = "Cancelar";
"Slug" = "Slug";
"Private" = "Privado";
"Public" = "Público";
"Caption" = "Súbtitulo";
"Title" = "Título";
"Tags" = "Etiquetas";
"Close" = "Cerrar";
"Notes" = "Notas";
"Note" = "Nota";
"Share" = "Compartir";
"Shared" = "Compartido";
"Edit" = "Editar";
"Actions" = "Acciones";
"Services" = "Servicios";
"Send to %@" = "Enviar a %@";
"Copy" = "Copiar";
"Copied!" = "¡Copiado!";
"Saving to %@" = "Compartiendo en %@";
"Saved!" = "Compartido!";
"Offline" = "Sin Conexión";
"Error" = "Error";
"Login" = "Iniciar Sesión";
"Logging In..." = "Iniciando Sesión...";
"Login Error" = "Error en Inicio de Sesión";
"Connecting..." = "Conectando...";

"Shortening URL..." = "Acortando URL...";
"Shorten URL Error" = "Error Acortando URL";
"We could not shorten the URL." = "No pudimos acortar la URL.";

"Create a free account at %@" = "Create a free account at %@";
"Create an account at %@" = "Create an account at %@";

"Send to Twitter" = "Compartir en Twitter";

"Message is too long" = "El mensaje es demasiado largo";
"Twitter posts can only be 140 characters in length." = "Los post de Twitter deben tener un máximo de 140 carácteres de longitud.";
"Message is empty" = "El mensaje está vacío";
"You must enter a message in order to post." = "Debes escribir un mensaje a compartir.";

"Enter your message:" = "Escribir mensaje:";

"Invalid email or password." = "Invalid email or password.";
"The service encountered an error. Please try again later." = "The service encountered an error. Please try again later.";
"There was a sending your post to Tumblr." = "There was a sending your post to Tumblr.";

"There was an error saving to Pinboard" = "There was an error saving to Pinboard";

"Sorry, Instapaper did not accept your credentials. Please try again." = "Sorry, Instapaper did not accept your credentials. Please try again.";
"Sorry, Instapaper encountered an error. Please try again." = "Sorry, Instapaper encountered an error. Please try again.";
"There was a problem saving to Instapaper." = "There was a problem saving to Instapaper.";

"Incorrect username and password" = "Incorrect username and password";
"There was an error logging into Google Reader" = "There was an error logging into Google Reader";
"There was a problem authenticating your account." = "There was a problem authenticating your account.";
"There was a problem saving your note." = "There was a problem saving your note.";

"There was a problem saving to Delicious." = "There was a problem saving to Delicious.";

"Open in Safari" = "Open in Safari";
"Attached: %@" = "Attached: %@";

"You must be online to login to %@" = "You must be online to login to %@";

"Auto Share" = "Auto Share";
"Enable auto share to skip this step in the future." = "Enable auto share to skip this step in the future.";

"You must be online in order to share with %@" = "You must be online in order to share with %@";
"There was an error while sharing" = "There was an error while sharing";

"Could not authenticate you. Please relogin." = "Could not authenticate you. Please relogin.";
"There was a problem requesting authorization from %@" = "There was a problem requesting authorization from %@";
"Request Error" = "Request Error";
"There was an error while sharing" = "There was an error while sharing";

"Authorize Error" = "Authorize Error";
"There was an error while authorizing" = "There was an error while authorizing";

"Authenticating..." = "Authenticating...";
"There was a problem requesting access from %@" = "There was a problem requesting access from %@";

"Access Error" = "Access Error";
"There was an error while sharing" = "There was an error while sharing";