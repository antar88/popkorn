//
//  firstLaunchViewController.h
//  popKorn
//
//  Created by Àlex Vergara on 05/09/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopKornBrain.h"
#import "WelcomeScreenPageController.h"
#import "PopKornTabBarController.h"

@interface welcomeScreenViewController : UIViewController <UCSharerDelegate, popKornBrainDownloadDelegate> {
    
    PopKornBrain *brain;
    WelcomeScreenPageController *contentController;
    int numberOfShows;
    int showsDownloaded;
    BOOL showListFinished;
    BOOL userIsWaiting;
}

@property (strong)      PopKornBrain *brain;
@property (strong)      IBOutlet WelcomeScreenPageController *contentController;
@property               int numberOfShows;
@property (nonatomic)   int showsDownloaded;
@property               BOOL showListFinished;
@property               BOOL userIsWaiting;
@property (strong)      NSMutableArray *viewControllers;

//Outlets
@property (strong, nonatomic) IBOutlet UIImageView *splashImg;
@property (strong, nonatomic) IBOutlet UIImageView *splashBox;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

- (void)sendAuthNotification;

@end
