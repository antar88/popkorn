//
//  Category.m
//  popKorn
//
//  Created by Àlex Vergara on 28/09/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import "Genre.h"


@implementation Genre
@dynamic name;
@dynamic localizedName;
@dynamic shows;

/* INITIALIZERS */
+ (Genre *) genreWithName:(NSString *)givenName inContext:(NSManagedObjectContext *)givenContext
{
    Genre *genre = nil;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Genre" inManagedObjectContext:givenContext];
    request.entity = entity;
    request.predicate = [NSPredicate predicateWithFormat:@"name = %@",givenName];
    NSError *error = nil;
    //NSLog(@"[Genre] Initializing Genre with context %@....",givenContext);
    genre = [[givenContext executeFetchRequest:request error:&error] lastObject];
    //NSLog(@"[Genre] Done");
    
    if (!error && !genre) {
        genre = [NSEntityDescription insertNewObjectForEntityForName:@"Genre" inManagedObjectContext:givenContext];
        genre.name = givenName;
        genre.localizedName = NSLocalizedString(givenName,nil);
    }
    
    return genre;
}

@end
