//
//  PopKornTabBarController.m
//  popKorn
//
//  Created by Àlex Vergara on 06/09/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import "PopKornTabBarController.h"

@implementation PopKornTabBarController

@synthesize brain;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithPopKornBrain:(PopKornBrain *)givenBrain
{
    self = [super init];
    if (self) {
        
        self.brain = givenBrain;
        
        UIViewController *featShows, *categories, *favShows, *showCalendar, *preferences;

        UINavigationController *featShowsNavCon = [[UINavigationController alloc] init];
        featShowsNavCon.navigationBar.tintColor = [UIColor orangeColor];
        featShows = [[FeaturedViewController alloc] initWithPopKornBrain:self.brain];
        self.brain.uiKache.fvc = (FeaturedViewController *) featShows;
        [featShowsNavCon pushViewController:featShows animated:NO];
        
        UINavigationController *categoriesNavCon = [[UINavigationController alloc] init];
        categoriesNavCon.navigationBar.tintColor = [UIColor orangeColor];
        categories = [[CategoriesViewController alloc] initWithPopKornBrain:self.brain];
        [categoriesNavCon pushViewController:categories animated:NO];
        
        /*
        UINavigationController *allShowsNavCon = [[UINavigationController alloc] init];
        allShowsNavCon.navigationBar.tintColor = [UIColor orangeColor];
        allShows = [[AllShowsViewController alloc] initInManagedObjectContext:self.brain.context];
        ((AllShowsViewController*) allShows).pkb = self.brain;
        [allShowsNavCon pushViewController:allShows animated:NO];
        */
        
        UINavigationController *favShowsNavCon = [[UINavigationController alloc] init];
        favShowsNavCon.navigationBar.tintColor = [UIColor orangeColor];
        favShows = [[AltFavShowsViewController alloc] initWithPopKornBrain:self.brain];
        self.brain.uiKache.afsvc = (AltFavShowsViewController *) favShows;
        [favShowsNavCon pushViewController:favShows animated:NO];
        
        UINavigationController *showCalNavCon = [[UINavigationController alloc] init];
        showCalNavCon.navigationBar.tintColor = [UIColor orangeColor];
        showCalendar = [[CalendarViewController alloc] init];
        ((CalendarViewController *) showCalendar).brain = self.brain;
        [showCalNavCon pushViewController:showCalendar animated:NO];
        
        UINavigationController *prefNavController = [[UINavigationController alloc] init];
        prefNavController.navigationBar.tintColor = [UIColor orangeColor];
        preferences = [[PreferencesViewController alloc] init];
        ((PreferencesViewController *) preferences).brain = self.brain;
        [prefNavController pushViewController:preferences animated:NO];
        
        self.viewControllers = [NSArray arrayWithObjects:
                                featShowsNavCon,
                                categoriesNavCon,
                             // allShowsNavCon,
                                favShowsNavCon,
                                showCalNavCon,
                                prefNavController,
                                nil];
        
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];

    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView
 {
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.

- (void)viewDidLoad
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications]; //notificacions
    [super viewDidLoad];
}

- (void)showReminder:(NSString *)text {
	
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Reminder" 
                                                        message:text 
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
	[alertView show];
}


- (void)changeBadge:(NSInteger)badgeNum {
    //NSLog(@"Entro a canviar els badges amb badges = %d", badgeNum);
    //NSLog(@"%@",self.tabBar.items);
    //[[self.tabBarController.tabBar.items objectAtIndex:2] setBadgeValue:[[NSString alloc] initWithFormat:@"%d",badgeNum]];
    
    [[[[[self tabBarController] tabBar] items] 
      objectAtIndex:2] setBadgeValue:@"3"];
    //NSLog(@"%@", [[self.tabBarController.tabBar.items objectAtIndex:2] badgeValue]);
    [self.tabBarController reloadInputViews];
    //[[self.tabBarController.tabBar.items objectAtIndex:2] setBadgeValue:@"3"];
    //[self.tabBarController.tabBar reloadInputViews];
    
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
