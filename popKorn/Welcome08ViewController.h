//
//  help01ViewController.h
//  popKorn
//
//  Created by Àlex Vergara Nebot on 27/10/11.
//  Copyright (c) 2011 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WelcomeViewController.h"
#import "TKProgressBarView.h"
#import "PopKornTabBarController.h"

@interface Welcome08ViewController : WelcomeViewController {
    IBOutlet TKProgressBarView *progressBar;
}

@property (strong, nonatomic) IBOutlet UIButton *letsGo;
@property (strong, nonatomic) IBOutlet UILabel *label;
@property (strong, nonatomic) IBOutlet UILabel *label2;
@property (nonatomic) double progress;

- (IBAction)letsGo:(id)sender;

@end
