//
//  CalendarViewController.m
//  popKorn
//
//  Created by Antar Moratona on 08/09/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import "CalendarViewController.h"

#define UN_DIA (60*60*24)

@implementation CalendarViewController
@synthesize brain;
@synthesize days;
@synthesize taula;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"list", nil);
        self.navigationController.navigationBar.topItem.title = NSLocalizedString(@"list", nil);
        
        //self.tabBarItem.image = [UIImage imageNamed:@"calendar"];
        //self.tabBarItem.title = NSLocalizedString(@"calendar", nil);
        
        CustomTabBarItem *tabItem = [[CustomTabBarItem alloc] initWithTitle:NSLocalizedString(@"calendar", nil) image:[UIImage imageNamed:@"calendar"] tag:0];
        tabItem.customHighlightedImage=[UIImage imageNamed:@"orangeCalendar"];
        self.tabBarItem = tabItem;
        
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"monthView", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(canviarCalendari:)];       
        self.navigationItem.leftBarButtonItem = barButtonItem;
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


- (IBAction)canviarCalendari:(id)sender {
    CalendarMonthView *aMonthView = [[CalendarMonthView alloc] initWithSunday:NO];
    ((CalendarMonthView *) aMonthView).brain = self.brain;
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration: 0.8];
	//Definir tipus d'animació i push del VC
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:YES];
	[self.navigationController pushViewController:aMonthView animated:NO];
	//Start Animation
	[UIView commitAnimations];
}

- (void)loadEpisodeCalendar
{
    /*
     NSFetchRequest *request = [[NSFetchRequest alloc] init];
     request.entity = [NSEntityDescription entityForName:@"Episode" inManagedObjectContext:brain.context];
     request.predicate = [NSPredicate predicateWithFormat:@"name = %@ AND showName = %@ AND seasonName = %@",givenName,givenSeason.show.name,givenSeason.name];
     NSError *error = nil;
     NSArray *episodes = [brain.context executeFetchRequest:request error:&error];
     
     //sfasff
     
     NSNumber *num  = [[NSNumber alloc]initWithInt:1];
     NSDate *startDate = [self initData:[[NSDate alloc]init] ambOffset:num];
     NSDate *endDate = [self initData:[[NSDate alloc]init] ambOffset:num];
     */
    
    //definicions
    NSDate *now = [[NSDate date]init];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [gregorian setTimeZone:[NSTimeZone localTimeZone]];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.month = 0;
    components.year = 0;
    components.hour = 0;
    components.minute = 0;
    components.second = 0;
    
    //data anterior
    NSNumber *diesBefore = [[NSNumber alloc] initWithInt:([[PKSettings daysBefore] intValue]*(-1))];
    [components setDay:diesBefore.integerValue];
    NSDate *startDate = [gregorian dateByAddingComponents:components toDate:now options:0];
    
    //data posterior
    NSNumber *diesAfter = [[NSNumber alloc] initWithInt:[[PKSettings daysAfter] intValue]];
    [components setDay:diesAfter.integerValue];
    NSDate *endDate = [gregorian dateByAddingComponents:components toDate:now options:0];
    
    //Consulta
    
    /*
    NSFetchRequest *request2 = [[NSFetchRequest alloc] init];
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"isFavorite = %@",[NSNumber numberWithBool:YES]];
    [request2 setEntity:[NSEntityDescription entityForName:@"TVshow" inManagedObjectContext:brain.context]];
    [request2 setPredicate:predicate2];
    
    NSError *error2 = nil;
    NSArray *results2 = [brain.context executeFetchRequest:request2 error:&error2];
    */
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"season.show.isFavorite = %@ AND (date >= %@) AND (date <= %@)",[NSNumber numberWithBool:YES], startDate, endDate];
    [request setEntity:[NSEntityDescription entityForName:@"Episode" inManagedObjectContext:brain.context]];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSLog(@"[CalendarViewController] Loading favorite episodes in context %@....",self.brain.context);
    NSArray *results = [self.brain.context executeFetchRequest:request error:&error];
    NSLog(@"[CalendarViewController] Done!");
    
    results = [results sortedArrayUsingSelector:@selector(dateCompare:)];
    
    NSDate *lastEpDate = nil;
    self.days = [[NSMutableArray alloc] init];
    for (Episode *ep in results) {
        if (!lastEpDate || [ep.date compare:lastEpDate] == NSOrderedDescending) {
            [self.days addObject:[[NSMutableArray alloc] init]];
        }
        [[self.days lastObject] addObject:ep];
        lastEpDate = ep.date;
    }
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    //[self setTaula:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)viewWillAppear:(BOOL)animated {
    [self loadEpisodeCalendar];
    [self.taula reloadData];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

//TAULA
/*
 - (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
 
 NSMutableArray *tempArray = [[NSMutableArray alloc] init];
 for (NSMutableArray *tmp in self.days) {
 Episode *ep = [tmp lastObject];
 [tempArray addObject:ep.date.description];
 }
 return tempArray;
 }
 
 -(NSString *) keyForSection:(NSInteger)section {
 NSString *k;
 if (section == 0)  k = @"#";
 else if (section == 1) k = @"a";
 else if (section == 2) k = @"b";
 else if (section == 3) k = @"c";
 else if (section == 4) k = @"d";
 else if (section == 5) k = @"e";
 else if (section == 6) k = @"f";
 else if (section == 7) k = @"g";
 else if (section == 8) k = @"h";
 else if (section == 9) k = @"i";
 else if (section == 10) k = @"j";
 else if (section == 11) k = @"k";
 else if (section == 12) k = @"l";
 else if (section == 13) k = @"m";
 else if (section == 14) k = @"n";
 else if (section == 15) k = @"o";
 else if (section == 16) k = @"p";
 else if (section == 17) k = @"q";
 else if (section == 18) k = @"r";
 else if (section == 19) k = @"s";
 else if (section == 20) k = @"t";
 else if (section == 21) k = @"u";
 else if (section == 22) k = @"v";
 else if (section == 23) k = @"w";
 else if (section == 24) k = @"x";
 else if (section == 25) k = @"y";
 else if (section == 26) k = @"z";
 else k = nil;
 return k;
 }*/


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning MARRANADA_EPICA
    self.taula = tableView;
    return [days count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self.days objectAtIndex:section] count]; 
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSDate *date = ((Episode * )[[days objectAtIndex:section] lastObject]).date;
    
    NSMutableString *title = [[NSMutableString alloc] initWithString: [UCMLParser stringFromDate:date]] ;
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	[gregorian setTimeZone:[NSTimeZone localTimeZone]];
	NSDateComponents *ara = [gregorian components:(NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit | NSYearCalendarUnit) 
										  fromDate:[NSDate date]];
    
    
    NSDateComponents *serie = [gregorian components:(NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit | NSYearCalendarUnit) 
                                           fromDate:date];

    
    NSTimeInterval interval = [[gregorian dateFromComponents:serie] timeIntervalSinceDate:[gregorian dateFromComponents:ara]];
    interval = interval/UN_DIA;

    
    if (interval == 1.0) return [title stringByAppendingString:NSLocalizedString(@" (tomorrow)",nil)];
    if (interval == -1.0) return [title stringByAppendingString:NSLocalizedString(@" (yesterday)",nil)];    
    
    int dies = interval;
    
    if (interval < 0) return [title stringByAppendingFormat:NSLocalizedString(@" (%d days ago)",nil),0-dies];
    if (interval > 0) return [title stringByAppendingFormat:NSLocalizedString(@" (in %d days)",nil),dies];
    return [title stringByAppendingString:NSLocalizedString(@" (today)",nil)];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"ShowCell";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
	}
    
    Episode *ep = ((Episode *)[[self.days objectAtIndex:indexPath.section] objectAtIndex:indexPath.row]);
    
    cell.textLabel.text = ep.displayName;
    cell.detailTextLabel.text = ep.season.show.name;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.imageView.image = [ep.seen boolValue] ? [UIImage imageNamed:@"seen"] : [UIImage imageNamed:@"unSeen"];
    
    //per canviar el color de la cell quan es selecciona
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIColor orangeColor]];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Episode *episode = [[self.days objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    EpisodeDetailView *edv = [[EpisodeDetailView alloc] init];
    edv.episode = episode;
    edv.pkb = self.brain;
    [self.navigationController pushViewController:edv animated:YES];
}



@end
