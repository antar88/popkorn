//
//  help01ViewController.h
//  popKorn
//
//  Created by Àlex Vergara Nebot on 27/10/11.
//  Copyright (c) 2011 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WelcomeViewController.h"
#import "PopKornNotificator.h"

@interface Welcome03ViewController : WelcomeViewController <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, UIPickerViewDelegate> {
    NSArray *nombreDies, *notisTitles, *nombreHores, *nombreMinuts;
    NSIndexPath *filaSelec;
}

@property (strong, nonatomic) IBOutlet UILabel *label;
@property (strong, nonatomic) IBOutlet UISwitch *switchNotis;
@property (strong, nonatomic) IBOutlet UITableView *taula;

- (IBAction)activateNotis:(id)sender;

@end
