//
//  PopKornNotificator.m
//  popKorn
//
//  Created by Antar Moratona on 04/10/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import "PopKornNotificator.h"

@implementation PopKornNotificator
@synthesize brain;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

-(NSString *) stringFromDate:(NSDate *)date {
    NSLocale *POSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]; 
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; 
    [dateFormatter setLocale:POSIXLocale];   
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];   
    return [dateFormatter stringFromDate:date]; 
    
    NSDateFormatter *df = [NSDateFormatter new]; 
    [df setDateFormat:@"dd/MM/yyyy hh:mm:ss 24"]; // here we cut time part
    return [df stringFromDate:date];
}

-(NSDate *) dateFromInitialDate:(NSDate *)date toDay:(NSInteger)day hour:(NSInteger)hour minutes:(NSInteger)minutes {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.month = 0;
    components.year = 0;
    components.hour = hour;
    components.minute = minutes;
    components.second = 0;
    components.day = day;
    NSDate *fireDate = [gregorian dateByAddingComponents:components toDate:date options:0];
    
    NSDate* sourceDate = [NSDate date];
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = sourceGMTOffset - destinationGMTOffset;
    NSDate* destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:fireDate] ;
    
    return destinationDate;
}

-(NSDate *) yesterday {
    /*
     //ahir
     NSDate* sourceDate = [NSDate date];
     NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
     NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
     NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
     NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
     NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
     NSDate* destinationDate = [[[NSDate alloc] initWithTimeInterval:interval sinceDate:[NSDate yesterday]] autorelease];
     //ahir a les 00:00
     */
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *now = [gregorian components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|
                             NSSecondCalendarUnit fromDate:[[NSDate alloc]init]];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.month = 0;
    components.year = 0;
    components.hour = now.hour*(-1);
    components.minute = now.minute*(-1);
    components.second = now.second*(-1);
    components.day = -1;
    NSDate *fireDate = [gregorian dateByAddingComponents:components toDate:[[NSDate alloc]init] options:0];
    return fireDate;
}

-(NSDate *) tomorrow {
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *now = [gregorian components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|
                             NSSecondCalendarUnit fromDate:[[NSDate alloc]init]];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.month = 0;
    components.year = 0;
    components.hour = now.hour*(-1);
    components.minute = now.minute*(-1);
    components.second = now.second*(-1);
    components.day = 1;
    NSDate *fireDate = [gregorian dateByAddingComponents:components toDate:[[NSDate alloc]init] options:0];
    return fireDate;
}

-(NSDate *) today {
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *now = [gregorian components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|
                             NSSecondCalendarUnit fromDate:[[NSDate alloc]init]];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.month = 0;
    components.year = 0;
    components.hour = now.hour*(-1);
    components.minute = now.minute*(-1);
    components.second = now.second*(-1);
    components.day = 0;
    NSDate *fireDate = [gregorian dateByAddingComponents:components toDate:[[NSDate alloc]init] options:0];
    return fireDate;
}

- (void)scheduleNotifications {
    dispatch_queue_t parseQueue = dispatch_queue_create("notiSchedule", NULL);
    dispatch_async(parseQueue, ^{
    
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        if([PKSettings notis]) {
            Class cls = NSClassFromString(@"UILocalNotification"); //notificacions
            if (cls != nil)  {
                NSInteger contNotis = 1;
                NSDate *startDate;
                if([[PKSettings notisDay]intValue] == 0) startDate = [self tomorrow]; // el dia anterior
                else if([[PKSettings notisDay]intValue] == 1) startDate = [self today]; //mateix dia
                else startDate = [self yesterday];//dia posterior

                NSFetchRequest *request = [[NSFetchRequest alloc] init];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"season.show.isFavorite = %@ AND date >= %@",[NSNumber numberWithBool:YES], startDate];
                [request setEntity:[NSEntityDescription entityForName:@"Episode" inManagedObjectContext:self.brain.context]];
                [request setPredicate:predicate];
                
                NSError *error = nil;
                //NSLog(@"[PopKornNotificator] Getting episodes to notify in context %@....",brain.context);
                NSArray *results = [brain.context executeFetchRequest:request error:&error];
                //NSLog(@"[PopKornNotificator] Done");
                
                results = [results sortedArrayUsingSelector:@selector(dateCompare:)];
                
                for (Episode *ep in results) {
                    UILocalNotification *notif = [[cls alloc] init];
                    // Data a notificar
                    notif.timeZone = [NSTimeZone systemTimeZone];
                    if([[PKSettings notisDay]intValue] == 0) notif.fireDate = [self dateFromInitialDate:ep.date 
                                                                                                  toDay:-1 
                                                                                                   hour:[[PKSettings notisHour]integerValue]
                                                                                                minutes:[[PKSettings notisMinutes]integerValue]]; //notificaciÃ³ el dia abans
                    else if ([[PKSettings notisDay]intValue] == 2) notif.fireDate = [self dateFromInitialDate:ep.date 
                                                                                                        toDay:1 
                                                                                                         hour:[[PKSettings notisHour]integerValue]
                                                                                                      minutes:[[PKSettings notisMinutes]integerValue]]; //notificaciÃ³ a l'endemÃ 
                    else notif.fireDate = [self dateFromInitialDate:ep.date 
                                                              toDay:0
                                                               hour:[[PKSettings notisHour]integerValue]
                                                            minutes:[[PKSettings notisMinutes]integerValue]]; //notificaciÃ³ el mateix dia d'emissiÃ³
                    
                    // Caracteristiques del missatge
                    if([[PKSettings notisDay]intValue] == 0)  notif.alertBody = [NSString stringWithFormat:NSLocalizedString(@"showsYesterday %@: %@", nil),ep.season.show.name,ep.name]; // el dia anterior
                    else if([[PKSettings notisDay]intValue] == 1) notif.alertBody = [NSString stringWithFormat:NSLocalizedString(@"showsToday %@: %@", nil),ep.season.show.name,ep.name];  //mateix dia
                    else notif.alertBody = [NSString stringWithFormat:NSLocalizedString(@"showsTomorrow %@: %@", nil),ep.season.show.name,ep.name];//dia posterior
                    
                    
                    
                    notif.alertAction = NSLocalizedString(@"notisShowMe", nil);
                    notif.soundName = UILocalNotificationDefaultSoundName;
                    notif.applicationIconBadgeNumber = contNotis;
                    contNotis++;
                    NSDictionary *userDict = [NSDictionary dictionaryWithObject:notif.alertBody
                                                                         forKey:@"kRemindMeNotificationDataKey"];
                    notif.userInfo = userDict;

                    NSDate *now = [[NSDate alloc]init];
                    if([now compare:notif.fireDate] == NSOrderedAscending) {
                        [[UIApplication sharedApplication] scheduleLocalNotification:notif];
                    } else {}
                }
            }
        }
    });
}

@end