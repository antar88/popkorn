//
//  TxDownloadFeatured.h
//  popKorn
//
//  Created by Àlex Vergara Nebot on 29/02/12.
//  Copyright (c) 2012 Undead Coders. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopKornBrain.h"

@interface TxDownloadFeatured : NSObject

+ (void) downloadFeaturedInContext:(NSManagedObjectContext *)context;

@end
