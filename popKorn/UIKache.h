//
//  UIKache.h
//  popKorn
//
//  Created by Àlex Vergara Nebot on 27/02/12.
//  Copyright (c) 2012 Undead Coders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PopKornBrain.h"

#pragma Mark - KachedFavorite
@interface KachedFavorite : NSObject {
    NSString *name;
    NSString *nextEp;
    UIImage *thumbnail;
}

@property (retain) NSString *name;
@property (retain) NSString *nextEp;
@property (retain) UIImage *thumbnail;
+(KachedFavorite *) favoriteWithName:(NSString *)name nextEp:(NSString *)nextEp thumbnail:(UIImage *)thumbnail;

@end

#pragma Mark - UIKacheTable Protocol
@protocol UIKacheTable <NSObject>
-(void) reloadTable;
@end

#pragma Mark - UIKache
@interface UIKache : NSObject {
    NSMutableArray *genres;
    NSMutableArray *favShows;
    NSMutableArray *unseenFavShows;
    NSMutableArray *top10;
    NSMutableArray *ourFavs;
    
    BOOL working;
    BOOL repeat;
    
    NSObject <UIKacheTable> *afsvc;
    NSObject <UIKacheTable> *fvc;
}

@property (retain, readonly) NSMutableArray *genres;
@property (retain, readonly) NSMutableArray *favShows;
@property (retain, readonly) NSMutableArray *unseenFavShows;
@property (retain, readonly) NSMutableArray *top10;
@property (retain, readonly) NSMutableArray *ourFavs;

@property (retain) NSObject <UIKacheTable> *afsvc;
@property (retain) NSObject <UIKacheTable> *fvc;

-(id) initInManagedObjectContext:(NSManagedObjectContext *)context;
-(void) loadGenresFromManagedObjectContext:(NSManagedObjectContext *)context;
-(void) loadFavoritesInBackgroundWithContext:(NSManagedObjectContext *)context;
-(void) loadFeaturedFromManagedObjectContext:(NSManagedObjectContext *)context;

@end
