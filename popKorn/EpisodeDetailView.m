//
//  EpisodeDetailView.m
//  popKorn
//
//  Created by Àlex Vergara on 02/09/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import "EpisodeDetailView.h"

@implementation EpisodeDetailView

@synthesize episode;
@synthesize pkb;
@synthesize titleLabel;
@synthesize shareTwitter;
@synthesize shareFacebook;
@synthesize shareMail;
@synthesize plotLabel;
@synthesize epImage;
@synthesize plotLoading;
@synthesize plotStatus;

-(void)imageRequestSuccesful:(UIImage *)img {
    //NSLog(@"Rebo al delegat la imatge correctament!");
    self.epImage.image = img;
    [UIView animateWithDuration:0.6
                     animations:^{ 
                         epImage.alpha = 1;
                     }];
}

-(void)imageRequestUnsuccesful:(NSError *)error {
    //NSLog(@"NO Rebo al delegat la imatge!");
}

- (void) checkAuth
{
    if ([pkb.sharer facebookIsAuthorized]) [self.shareFacebook setImage:[UIImage imageNamed:@"face"] forState:UIControlStateNormal];
    else [self.shareFacebook setImage:[UIImage imageNamed:@"faceDisabled"] forState:UIControlStateNormal];
    
    if ([pkb.sharer twitterIsAuthorized]) [self.shareTwitter setImage:[UIImage imageNamed:@"twitter"] forState:UIControlStateNormal];
    else [self.shareTwitter setImage:[UIImage imageNamed:@"twitterDisabled"] forState:UIControlStateNormal];
    
    [self.shareMail setImage:[UIImage imageNamed:@"mail"] forState:UIControlStateNormal];
}

- (void) sendAuthNotification
{
    [self checkAuth];
}

- (void) seenPressed {
    self.episode.seen = [NSNumber numberWithBool:![self.episode.seen boolValue]];
    NSError* mocError = nil;
    [self.pkb.context save:&mocError];
    if (mocError) {
        NSLog(@"ERROR al guardar context en - (void) seenPressed, codi: %@",[mocError localizedDescription]);
    }
    if ([self.episode.seen boolValue]) {
        if ([self.pkb areTherePastUnseenEpisodes:self.episode]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"pastEpisodes",nil) message:NSLocalizedString(@"pastEpisodesDescription",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"no",nil) otherButtonTitles:NSLocalizedString(@"yes",nil), nil];
            [alert show];
        }
        else {
            [[TKAlertCenter defaultCenter]postAlertWithMessage:NSLocalizedString(@"seen", nil)
                                                         image:[UIImage imageNamed:@"detailSeen"]];
            [self.pkb.uiKache loadFavoritesInBackgroundWithContext:self.pkb.context];
        }
    }
    else {
        [[TKAlertCenter defaultCenter]postAlertWithMessage:NSLocalizedString(@"notSeen", nil)
                                                     image:[UIImage imageNamed:@"detailUnseen"]];
        [self.pkb.uiKache loadFavoritesInBackgroundWithContext:self.pkb.context];
    }
    UIImage *favImage = [self.episode.seen boolValue] ? [UIImage imageNamed:@"detailSeen"] : [UIImage imageNamed:@"detailUnseen"];
    self.navigationItem.rightBarButtonItem.image = favImage;
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) [self.pkb setPastEpisodesAsSeen:self.episode];
    [[TKAlertCenter defaultCenter]postAlertWithMessage:NSLocalizedString(@"seen", nil) image:[UIImage imageNamed:@"detailSeen"]];
    [self.pkb.uiKache loadFavoritesInBackgroundWithContext:self.pkb.context];
}

- (IBAction)shareTwitterPressed:(id)sender {
    if ([pkb.sharer twitterIsAuthorized])
        [pkb.sharer postTwitter:[NSString stringWithFormat:NSLocalizedString(@"I am watching %@ - %@! #popKornApp",nil),self.episode.season.show.name,self.episode.displayName]];
    else [pkb.sharer authorizeTwitter];
    //[TestFlight passCheckpoint:@"Ha compartit a Twitter."];
}

- (IBAction)shareFacebookPressed:(id)sender {
    if ([pkb.sharer facebookIsAuthorized]) {
        NSString *plot = self.episode.plot;
        NSLog(@"[ShowInfo] publico al face amb titol: %@ desc: %@ i imatge: %@", [NSString stringWithFormat:NSLocalizedString(@"I am watching %@ - %@!",nil),self.episode.season.show.name,self.episode.displayName]
              , plot ? plot : self.episode.name
              , self.episode.photoURL);
        [pkb.sharer postFacebook:[NSString stringWithFormat:NSLocalizedString(@"I am watching %@ - %@!",nil),self.episode.season.show.name,self.episode.displayName] 
                  andDescription:plot ? plot : self.episode.name
                        andImage:self.episode.photoURL];
    }
    else [pkb.sharer authorizeFacebook];
    //[TestFlight passCheckpoint:@"Ha compartit a FaceBook."];
}

- (IBAction)shareMailPressed:(id)sender {
    [pkb.sharer postMail:[NSString stringWithFormat:NSLocalizedString(@"I am watching %@ - %@!",nil),self.episode.season.show.name,self.episode.displayName] withSubject:@"popKorn"];
    //[TestFlight passCheckpoint:@"Ha compartit amb Mail."];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        pkb.sharer.delegate = self;
        imgCnt = [[UCImageCache alloc] init];
        imgCnt.delegate = self;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)putPlot:(NSString *)plot
{
    txtPlot.text = plot;
    txtPlot.contentOffset = CGPointMake(0.0, 1.0);
    txtPlot.contentOffset = CGPointMake(0.0, 0.0);
}

#pragma mark - episodeDownloadDelegate methods

- (void) setImage:(NSURL *)imageUrl
{
    UIImage *img = [imgCnt requestImage:imageUrl];
    if (img) {
        //NSLog(@"Image already in cache: origin 1");
        self.epImage.image = img;
        self.epImage.alpha = 1.0;
    }
}

- (void) setPlot:(NSString *)plot
{
    txtPlot.alpha = 0.0;
    
    if ([PKSettings translate] && [plot length]) {
        NSString * locale = [[NSLocale preferredLanguages] objectAtIndex:0];
        UCTranslate *tr = [[UCTranslate alloc] init];
        tr.delegate = self;
        self.plotStatus.text = NSLocalizedString(@"translating",nil);
        [tr bingTranslate:plot toLanguage:locale];
        return;
    }
    
    if ([plot length] == 0) [self putPlot:NSLocalizedString(@"noPlot",nil)];
    else [self putPlot:plot];
    
    [UIView animateWithDuration:0.4
                     animations:^{
                         txtPlot.alpha = 1.0;
                         self.plotLoading.alpha = 0.0;
                         self.plotStatus.alpha = 0.0;
                     }
     ];
}

- (void) requestUnsuccessful:(NSError *)error
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error",nil) message:NSLocalizedString(@"error500",nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    NSLog(@"%@",NSLocalizedString(@"error500", nil));
}

-(void)translationOperationDidFailWithError:(NSError *)error {
    NSLog(@"Error al traduir");
}

-(void)translationOperationDidFinishTranslatingText:(NSString *)result {
    [self.pkb.context refreshObject:self.episode mergeChanges:NO];
    self.episode.plotLocalized = result;
    self.episode.plotLocale = [[NSLocale preferredLanguages] objectAtIndex:0];

    NSError* mocError = nil;
    [self.pkb.context save:&mocError];
    if (mocError) {
        NSLog(@"ERROR al guardar context en -(void)translationOperationDidFinishTranslatingText:(NSString *)result, codi: %@",[mocError localizedDescription]);
    }
    
    [self putPlot:result];
    
    [UIView animateWithDuration:0.4
                     animations:^{
                         txtPlot.alpha = 1.0;
                         self.plotLoading.alpha = 0.0;
                         self.plotStatus.alpha = 0.0;
                     }
     ];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.plotLabel.text = NSLocalizedString(@"plot", nil);
    self.title = self.episode.name;
    self.titleLabel.text = self.episode.name;
    epCode.text = self.episode.code;
    showInfo.text = self.episode.season.show.name;
    seasonInfo.text = self.episode.season.name;
    airDate.text = [UCMLParser stringFromDate:self.episode.date];
    
    //Demanant imatge
    NSURL *imageUrl = [NSURL URLWithString:self.episode.photoURL];
    if (!imageUrl) imageUrl = [NSURL URLWithString:self.episode.season.show.photoURL];
    UIImage *img = [imgCnt requestImage:imageUrl];
    if (img) {
        //NSLog(@"Image already in cache: origin 2");
        self.epImage.image = img;
        self.epImage.alpha = 1.0;
    }
    
    //social
    [self checkAuth];
    
    //plot
    if (self.episode.plot) {
        if ([self.episode.plot length] == 0) {
            [self putPlot:NSLocalizedString(@"noPlot", nil)];
            self.plotLoading.alpha = 0.0;
            self.plotStatus.alpha = 0.0;
        }
        else if ([PKSettings translate]) {
            NSString * locale = [[NSLocale preferredLanguages] objectAtIndex:0];
            if ([self.episode.plotLocale hasPrefix:locale]) {
                [self putPlot:self.episode.plotLocalized];
                self.plotLoading.alpha = 0.0;
                self.plotStatus.alpha = 0.0;
            }
            else {
                UCTranslate *tr = [[UCTranslate alloc] init];
                tr.delegate = self;
                self.plotStatus.text = NSLocalizedString(@"translating",nil);
                [tr bingTranslate:self.episode.plot toLanguage:locale];
            }
        }
        else {
            [self putPlot:self.episode.plot];
            self.plotLoading.alpha = 0.0;
            self.plotStatus.alpha = 0.0;
        }
    }
    else {
        self.plotStatus.text = NSLocalizedString(@"loading info",nil);
    }

    //boto vist
    UIImage *favImage = [self.episode.seen boolValue] ? [UIImage imageNamed:@"detailSeen"] : [UIImage imageNamed:@"detailUnseen"];
    UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithImage:favImage
                                                             style:UIBarButtonItemStylePlain
                                                            target:self
                                                            action:@selector(seenPressed)];
    self.navigationItem.rightBarButtonItem = item;
    
    [episode downloadInfoInManagedObjectContext:self.pkb.context withDelegate:self];
}

- (void)viewDidUnload
{
    [self setPlotStatus:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated {
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
