//
//  UCSharer.m
//  shareTest
//
//  Created by Àlex Vergara on 01/09/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import "UCSharer.h"
#import "popKornAppDelegate.h"
@implementation UCSharer


@synthesize delegate;
@synthesize ios5;

- (void) sendAuthNotification
{
    [delegate sendAuthNotification];
}

- (id)init
{
    self = [super init];
    if (self) {
        twitter = [[SHKTwitter alloc] init];
        twitter.UCdelegate = self;

        if (NSClassFromString(@"TWTweetComposeViewController")) ios5 = YES;
        else ios5 = NO;
    }
    
    return self;
}

- (BOOL) twitterIsAuthorized
{
    if (ios5) {
        Class twitterClass = NSClassFromString(@"TWTweetComposeViewController");
        return (BOOL) [twitterClass performSelector:@selector(canSendTweet)];
    }
    else {
        return [twitter isAuthorized];
    }
}

- (void) authorizeTwitter
{
    if (ios5) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error",nil) message:NSLocalizedString(@"twitterAuth",nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else {
        [twitter promptAuthorization:self];
    }
}

- (void) postTwitter:(NSString *)status
{
    if (ios5) {
        Class twitterClass = NSClassFromString(@"TWTweetComposeViewController");
        id tweetViewController = [[twitterClass alloc] init];
        [tweetViewController performSelector:@selector(setInitialText:) withObject:status];
        [delegate presentModalViewController:tweetViewController animated:YES];
        
    }
    else {
        SHKItem *item = [SHKItem text:status];
        [item setCustomValue:item.text forKey:@"status"];
        twitter.item = item;
        [twitter sendStatus];
        [twitter sendDidStart];
    }
}

- (BOOL) facebookIsAuthorized
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:@"FBAccessTokenKey"] && [defaults objectForKey:@"FBExpirationDateKey"];
}

- (void) authorizeFacebook
{
    popKornAppDelegate *appDelegate = (popKornAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.facebook authorize:nil];
    NSLog(@"[Facebook] autoritza!");
    //[facebook dialog:@"feed"andDelegate:self];
    
}


- (void) postFacebook:(NSString *)status andDescription:(NSString *)desc andImage:(NSString *)imageURL {
    // Dialog parameters
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:status forKey:@"name"];
    [params setValue:desc forKey:@"caption"];
    [params setValue:@"http://www.undeadcoders.com/popkorn/discover.php" forKey:@"link"];
    [params setValue:imageURL forKey:@"picture"];
    [params setValue:NSLocalizedString(@"shareWithPopKorn",nil) forKey:@"description"];
    popKornAppDelegate *appDelegate = (popKornAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.facebook dialog:@"feed" andParams:params andDelegate:self];
}

- (void) postFacebook:(NSString *)status andDescription:(NSString *)desc andImage:(NSString *)imageURL andLink:(NSString *)url {
    // Dialog parameters
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:status forKey:@"name"];
    if ([desc length] > 995)
        desc = [[desc substringToIndex:990] stringByAppendingString:@"..."];
    [params setValue:desc forKey:@"caption"];
    [params setValue:url forKey:@"link"];
    [params setValue:imageURL forKey:@"picture"];
    [params setValue:NSLocalizedString(@"shareWithPopKorn",nil) forKey:@"description"];
    popKornAppDelegate *appDelegate = (popKornAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.facebook dialog:@"feed" andParams:params andDelegate:self];
}






//Delegate methods

    //FACEBOOK METHODS


/**
 * Called when the user dismissed the dialog without logging in.
 */
- (void)fbDidNotLogin:(BOOL)cancelled {
    NSLog(@"Sessio Not Login Cancelled: %d!", (int) cancelled);
}

/**
 * Called after the access token was extended. If your application has any
 * references to the previous access token (for example, if your application
 * stores the previous access token in persistent storage), your application
 * should overwrite the old access token with the new one in this method.
 * See extendAccessToken for more details.
 */
- (void)fbDidExtendToken:(NSString*)accessToken
               expiresAt:(NSDate*)expiresAt {
    NSLog(@"Extensio AccessToken!");
}

/**
 * Called when the user logged out.
 */
- (void)fbDidLogout {
    NSLog(@"Logout!");
}

/**
 * Called when the current session has expired. This might happen when:
 *  - the access token expired
 *  - the app has been disabled
 *  - the user revoked the app's permissions
 *  - the user changed his or her password
 */
- (void)fbSessionInvalidated {
    NSLog(@"Sessio invalidada!");
}

- (void)request:(FBRequest *)request didFailWithError:(NSError *)error {
    NSLog(@"%@", [error localizedDescription]);
    NSLog(@"Err details: %@", [error description]);
};
    


- (void)requestLoading:(FBRequest *)request {
    NSLog(@"Request loading %@", request);
}

/**
 * Called when the server responds and begins to send back data.
 */
- (void)request:(FBRequest *)request didReceiveResponse:(NSURLResponse *)response {
    NSLog(@"Request didrecieveRespose %@", response);
}


/**
 * Called when a request returns and its response has been parsed into
 * an object.
 *
 * The resulting object may be a dictionary, an array, a string, or a number,
 * depending on thee format of the API response.
 */
- (void)request:(FBRequest *)request didLoad:(id)result{
    NSLog(@"Request Load %@", result);
}

/**
 * Called when a request returns a response.
 *
 * The result object is the raw response from the server of type NSData
 */
- (void)request:(FBRequest *)request didLoadRawResponse:(NSData *)data{
    NSLog(@"Request RAW %@", data.description);
}


    //END FACEBOOK METHODS

- (BOOL) mailIsAuthorized
{
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass)
    {
        if ([mailClass canSendMail]) return true;
    }
    return false;
}

- (void) authorizeMail
{
}

- (void) postMail:(NSString *)status withSubject:(NSString *)subject
{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
	
	[picker setSubject:subject];
	[picker setMessageBody:status isHTML:NO];
	
	[delegate presentModalViewController:picker animated:YES];
}

- (void) postMail:(NSString *)status withSubject:(NSString *)subject toMail:(NSString *)mail
{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
	
	[picker setSubject:subject];
	[picker setMessageBody:status isHTML:NO];
    [picker setToRecipients:[NSArray arrayWithObject:mail]];

	[delegate presentModalViewController:picker animated:YES];
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	[delegate dismissModalViewControllerAnimated:YES];
}

@end
