//
//  UCTranslate.m
//  popKorn
//
//  Created by Antar Moratona on 17/09/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "UCTranslate.h"

@implementation UCTranslate

@synthesize delegate;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
        queue = [[NSOperationQueue alloc] init];
    }
    
    return self;
}

//GTRANSLATOR

-(void)translationOperation:(MRTranslationOperation *)operation didFailWithError:(NSError *)error {
    NSLog(@"ERROR: %@",[error localizedDescription]);
    [delegate translationOperationDidFailWithError:error];
}

-(void)translationOperation:(MRTranslationOperation *)operation didFinishTranslatingText:(NSString *)result {
    NSLog(@"SUCCESS: %@",result);
    [delegate translationOperationDidFinishTranslatingText:result];
}


-(void) translate:(NSString *)text toLanguage:(NSString *)language {
    NSLog(@"TRANSLATING TO %@: %@",language,text);
    MRTranslationOperation *op = [[MRTranslationOperation alloc] initWithSourceString:text
                                                              destinationLanguageCode:language];
	op.delegate = self;
	[queue addOperation:op];
}

//BING TRANSLATOR

-(void)bingTranslate:(NSString *)text toLanguage:(NSString *)language {
    //NSLog(@"Entro a bingTranslator i vull traduir a  %@", language);
    BingTranslator *t = [[BingTranslator alloc] initWithText:text];
    t.delegate = self;
    [t translate:language];
}

-(NSString *)bingTranslateSync:(NSString *)text toLanguage:(NSString *)language {
    BingTranslator *t = [[BingTranslator alloc] initWithText:text];
    return [t translateSync:language];
}

-(void)bingTranslationSuccess:(NSString *)res {
    [delegate translationOperationDidFinishTranslatingText:res];
}

-(void)bingTranslationdidFailWithError:(NSError *)error {
    NSLog(@"Traducció amb ERROR");
    [delegate translationOperationDidFailWithError:error];
}

@end
