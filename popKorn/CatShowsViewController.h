//
//  CatShowsViewController.h
//  popKorn
//
//  Created by Àlex Vergara on 23/08/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataTableViewController.h"
#import "PopKornBrain.h"
#import "ShowDetailView.h"

@interface CatShowsViewController : CoreDataTableViewController {
@private
    PopKornBrain *pkb;
    Genre *genre;
}

@property (strong) PopKornBrain *pkb;
@property (strong) Genre *genre;

- initInManagedObjectContext:(NSManagedObjectContext *)context;

@end
