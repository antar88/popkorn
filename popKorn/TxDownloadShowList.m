//
//  TxDownloadShowList.m
//  popKorn
//
//  Created by Àlex Vergara Nebot on 27/02/12.
//  Copyright (c) 2012 Undead Coders. All rights reserved.
//

#import "TxDownloadShowList.h"

@implementation TxDownloadShowList

#pragma mark - Methods
+ (BOOL) showListDownloaded
{
    NSDate *lastUpdate = [PKSettings lastUpdate];
    if (lastUpdate) return YES;
    return NO;
}

+ (void) downloadShowListInContext:(NSManagedObjectContext *)context withDelegate:(id <popKornBrainDownloadDelegate>)delegate
{
    BOOL first_time = NO;
    if (![PKSettings lastUpdate]) first_time = YES;
    
    dispatch_queue_t parseQueue = dispatch_queue_create("parse queue", NULL);
    dispatch_async(parseQueue, ^{
        
        //Imprescindible crear el managedObjectContext dins el thread que el tracti
        NSManagedObjectContext *threadContext = [[NSManagedObjectContext alloc] init];
        [threadContext setPersistentStoreCoordinator:context.persistentStoreCoordinator];
        
        UCMLParser *ucp = [[UCMLParser alloc] initWithURL:@"http://undeadcoders.com/pkdb/showDbV1_0/showDb.txt"];
        if (!ucp){
            [delegate requestUnsuccessful:nil];
            return;
        }
        int i = 0;
        [ucp skipTag];
        [delegate setNumberOfShows:[[ucp getNonTag] integerValue]];
        [ucp readUntil:@"<shows>"];
        NSString *tag = [ucp getTag];
        while(![tag hasSuffix:@"</shows>"]) {
            [ucp readUntil:@"<name>"];
            NSString *name = [ucp getNonTag];
            [ucp readUntil:@"<url>"];
            NSString *url = [ucp getNonTag];
            TVshow *show;
            if (first_time) show = [TVshow createTvShowWithName:name url:url inContext:threadContext];
            else show = [TVshow tvShowWithName:name url:url inContext:threadContext];
            ++i;
            dispatch_async(dispatch_get_main_queue(), ^{
                [delegate setShowsDownloaded:i];
            });
            [ucp readUntil:@"<genres>"];
            tag = [ucp getTag];
            while (![tag hasSuffix:@"</genres>"]) {
                NSString *genre = [ucp getNonTag];
                Genre *gen = [Genre genreWithName:genre inContext:threadContext];
                [show addGenresObject:gen];
                [ucp skipTag];
                tag = [ucp getTag];
            }
            [ucp skipTag];
            tag = [ucp getTag];
        }
        
        NSLog(@"UPDATE SUCCESFUL: %d entries found",i);
        
        NSError* mocError = nil;
        [threadContext save:&mocError];
        if (mocError) {
            NSLog(@"ERROR al guardar context en - (void) seenPressed, codi: %@",[mocError localizedDescription]);
        }
        
        NSDate *now = [[NSDate alloc] init];
        [PKSettings setLastUpdate:now];
        [delegate requestSuccessful];
    });
}

@end
