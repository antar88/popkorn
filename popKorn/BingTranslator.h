//
//  BingTranslator.h
//  popKorn
//
//  Created by Antar Moratona Franco on 28/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//
#import <Foundation/Foundation.h>

@protocol BingTranslatorDelegate;

@interface BingTranslator : NSObject <NSURLConnectionDataDelegate>{
@private
	NSString *text;
    id < BingTranslatorDelegate > delegate;
}

@property (strong) id < BingTranslatorDelegate > delegate;


-(id) initWithText:(NSString *) textATraduir;

-(NSString *) translateSync:(NSString *) to;

-(void) translate:(NSString *) to;

@end


@protocol BingTranslatorDelegate < NSObject>

- (void)bingTranslationSuccess:(NSString *)res;
- (void)bingTranslationdidFailWithError:(NSError *)error;

@end


