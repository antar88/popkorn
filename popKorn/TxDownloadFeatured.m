//
//  TxDownloadFeatured.m
//  popKorn
//
//  Created by Àlex Vergara Nebot on 29/02/12.
//  Copyright (c) 2012 Undead Coders. All rights reserved.
//

#import "TxDownloadFeatured.h"

@implementation TxDownloadFeatured

+ (NSMutableArray *) getShowsFromURL:(NSString *)url
{
    UCMLParser *ucp = [[UCMLParser alloc] initWithURL:url];
    if (!ucp) return nil;
    NSMutableArray *results = [[NSMutableArray alloc] init];
    while (![ucp eof]) {
        [ucp goToNext:@"<show>"];
        if ([ucp eof]) return results;
        NSString *nom = [ucp getNonTag];
        [results addObject:[nom substringWithRange:NSMakeRange(5, nom.length-6)]];
        //NSLog(@"%@",[results lastObject]);
    }
    return results;
}

+ (void) downloadFeaturedInContext:(NSManagedObjectContext *)context
{
    FeaturedList *list = [FeaturedList featuredListInManagedObjectContext:context];

    NSNumber *position = [[NSNumber alloc] initWithInt:1];
    
    NSMutableArray *top10 = [TxDownloadFeatured getShowsFromURL:@"http://undeadcoders.com/pkdb/showDbV1_0/top10.txt"];
    if (top10) {
        [list removeTop10:list.top10];
        for (NSString *show in top10) {
            FeaturedShow *fShow = [FeaturedShow featuredShowInManagedObjectContext:context
                                                                          withName:show
                                                                       andPosition:position];
            [list addTop10Object:fShow];
            position = [[NSNumber alloc] initWithInt:[position intValue]+1];
        }
    }
    
    NSMutableArray *ourFavs = [TxDownloadFeatured getShowsFromURL:@"http://undeadcoders.com/pkdb/showDbV1_0/ourFavs.txt"];
    if (ourFavs) {
        [list removeOurFavs:list.ourFavs];
        for (NSString *show in ourFavs) {
            FeaturedShow *fShow = [FeaturedShow featuredShowInManagedObjectContext:context
                                                                          withName:show
                                                                       andPosition:position];
            [list addOurFavsObject:fShow];
        }
    }
    
    [context save:nil];
}
@end
