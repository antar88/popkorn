//
//  FeaturedShow.m
//  popKorn
//
//  Created by Àlex Vergara Nebot on 15/02/12.
//  Copyright (c) 2012 Undead Coders. All rights reserved.
//

#import "FeaturedShow.h"
#import "TVshow.h"


@implementation FeaturedShow

@dynamic position;
@dynamic show;

+ (FeaturedShow *) featuredShowInManagedObjectContext:(NSManagedObjectContext *)context withName:(NSString *)name andPosition:(NSNumber *)position
{
    FeaturedShow *show = [NSEntityDescription insertNewObjectForEntityForName:@"FeaturedShow" inManagedObjectContext:context];
    show.show = [TVshow tvShowWithName:name inContext:context];
    show.position = position;
    
    return show;
}

@end
