//
//  popKornAppDelegate.h
//  popKorn
//
//  Created by Àlex Vergara on 04/09/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "welcomeScreenViewController.h"
#import "PopKornTabBarController.h"

#import "PopKornBrain.h"
//#import "TestFlight.h"
#import "PopKornNotificator.h"
#import "FBConnect.h"

#warning TODO: remove
#import "TVshow.h"


@interface popKornAppDelegate : NSObject <UIApplicationDelegate, UITabBarControllerDelegate, FBSessionDelegate> {
    PopKornBrain *brain;
    IBOutlet UITabBarItem *cal;
    Facebook *facebook;
}

@property (nonatomic, strong) IBOutlet UIWindow *window;

@property (nonatomic, retain) Facebook *facebook;

@property (nonatomic, strong, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (strong) PopKornBrain *brain;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end
