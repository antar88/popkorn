//
//  EpisodesViewController.h
//  popKorn
//
//  Created by Àlex Vergara on 01/09/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataTableViewController.h"
#import "PopKornBrain.h"
#import "EpisodeDetailView.h"

@interface EpisodesViewController : CoreDataTableViewController {
@private
    PopKornBrain *pkb;
}

@property (strong) PopKornBrain *pkb;

- initInManagedObjectContext:(NSManagedObjectContext *)context withSeason:(Season *)season;

@end
