//
//  FeaturedList.m
//  popKorn
//
//  Created by Àlex Vergara Nebot on 15/02/12.
//  Copyright (c) 2012 Undead Coders. All rights reserved.
//

#import "FeaturedList.h"


@implementation FeaturedList

@dynamic top10;
@dynamic ourFavs;

+ (FeaturedList *) featuredListInManagedObjectContext:(NSManagedObjectContext *)context
{
    FeaturedList *list = nil;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"FeaturedList" inManagedObjectContext:context];
    request.entity = entity;
    NSError *error = nil;
    //NSLog(@"[FeaturedList] Fetching list in context %@....",context);
    NSArray *results = [context executeFetchRequest:request error:&error];
    //NSLog(@"[FeaturedList] Done");
    list = [results lastObject];
    
    if (!error && !list) {
        list = [NSEntityDescription insertNewObjectForEntityForName:@"FeaturedList" inManagedObjectContext:context];
    }
    
    return list;
}

@end
