//
//  TxDownloadShowInfo.m
//  popKorn
//
//  Created by Àlex Vergara Nebot on 27/02/12.
//  Copyright (c) 2012 Undead Coders. All rights reserved.
//

#import "TxDownloadShowInfo.h"

@implementation TxDownloadShowInfo

+ (NSMutableSet *) downloadSeasonsFromUndeadCodersDb:(UCMLParser *)ucp withShow:(TVshow *)show inMoc:(NSManagedObjectContext *)givenContext
{
    NSMutableSet *seasons = [[NSMutableSet alloc] init];
    while ([ucp getTag]) {
        [ucp readUntil:@"<name>"];
        NSString *seasonTitle = [ucp readUntil:@"</name>"];
        Season *season;
        if ([Season seasonExists:seasonTitle show:show.name inContext:givenContext]) {
            season = [Season seasonWithName:seasonTitle show:show.name inContext:givenContext];
        }
        else {
            season = [Season createSeasonWithName:seasonTitle withShow:show.name inContext:givenContext];
            [seasons addObject:season];
        }
        while (![[ucp getTag] hasSuffix:@"</episodes>"]) {
            NSString *epCode = [ucp readBetween:@"<code>" and:@"</code>"];
            NSString *epName = [ucp readBetween:@"<name>" and:@"</name>"];
            NSString *epDate = [ucp readBetween:@"<date>" and:@"</date>"];
            NSString *epUrl = [ucp readBetween:@"<url>" and:@"</url>"];
            Episode *ep = [Episode episodeWithCode:epCode season:season inContext:givenContext];
            ep.name = epName;
            ep.date = [UCMLParser dateFromString:epDate];
            ep.url = epUrl;
            
            NSError* mocError = nil;
            [givenContext save:&mocError];
            if (mocError) {
                NSLog(@"ERROR al guardar context en + (NSMutableSet *) downloadSeasonsFromUndeadCodersDb:(UCMLParser *)ucp withShow:(TVshow *)show inMoc:(NSManagedObjectContext *)givenContext, codi: %@",[mocError localizedDescription]);
            }
            
            [ucp readUntil:@"</episode>"];
        }
        [ucp goToNext:@"</season>"];
    }
    return seasons;
}

+ (NSMutableSet *) downloadSeasonsFromTVRageDb:(UCMLParser *)ucp withShow:(TVshow *)show inMoc:(NSManagedObjectContext *)givenContext
{
    NSMutableSet *seasons = [[NSMutableSet alloc] init];
    
    [ucp goToNext:@" align='center'><font size='4'>"];
    NSString *seasonTitle = [ucp getNonTag];
    while(seasonTitle) {
        int airdate = -1;
        int title = 0;
        Season *season;
        if ([Season seasonExists:seasonTitle show:show.name inContext:givenContext]) {
            season = [Season seasonWithName:seasonTitle show:show.name inContext:givenContext];
        }
        else {
            season = [Season createSeasonWithName:seasonTitle withShow:show.name inContext:givenContext];
            [seasons addObject:season];
        }

        [ucp goToNext:@"<table width='100%' cellspacing='0' align='center' class='b' >"];
        NSString *trTag = [ucp getTag];
        while ([trTag hasPrefix:@"<tr "]) {
            NSMutableArray *epInfo = [[NSMutableArray alloc] init];
            NSString *tdTag = [ucp getTag];
            while ([tdTag hasPrefix:@"<td "]) {
                [epInfo addObject:[ucp readUntil:@"</td>"]];
                tdTag = [ucp getTag];
            }

            if (airdate == -1) {
                for (int i = 0; i < epInfo.count; ++i) {
                    if ([UCMLParser string:[epInfo objectAtIndex:i] contains:@"Airdate"]) {
                        airdate = i;
                        if ([seasonTitle hasPrefix:@"Season"] || [seasonTitle hasPrefix:@"Series"]) ++airdate;
                    }
                    if ([UCMLParser string:[epInfo objectAtIndex:i] contains:@"Title"]) {
                        title = i;
                        if ([seasonTitle hasPrefix:@"Season"] || [seasonTitle hasPrefix:@"Series"]) ++title;
                    }
                }
            }
            else {
                /* TREAT EPINFO */
                UCMLParser *epCodeParser = [[UCMLParser alloc] initWithString:[epInfo objectAtIndex:1]];
                NSString *epCode = [epCodeParser getNonTag];
                if (epCode) {
                    [epCodeParser skipTag];
                    epCode = [epCodeParser getNonTag];
                }
                else {
                    epCode = [epInfo objectAtIndex:epInfo.count-4];
                }
                
                NSString *epDate = [epInfo objectAtIndex:airdate];
                NSDate *date = nil;
                if (![epDate hasPrefix:@"Un"]) date = [UCMLParser dateFromString:epDate];
                
                UCMLParser *epTitleParser = [[UCMLParser alloc] initWithString:[epInfo objectAtIndex:title]];
                [epTitleParser goToNext:@"href='"];
                NSString *epURL = [@"http://tvrage.com" stringByAppendingString:[epTitleParser readUntil:@"'"]];
                [epTitleParser skipTag];
                NSString *epTitle = [epTitleParser getNonTag];
                if (epTitle.length == 0) {
                    [epTitleParser goToNext:@"href='"];
                    epURL = [@"http://tvrage.com" stringByAppendingString:[epTitleParser readUntil:@"'"]];
                    [epTitleParser skipTag];
                    epTitle = [epTitleParser getNonTag];
                }
                
                Episode *ep = [Episode episodeWithCode:epCode season:season inContext:givenContext];
                ep.name = epTitle;
                ep.date = date;
                ep.url = epURL;
                
                NSError* mocError = nil;
                [givenContext save:&mocError];
                if (mocError) {
                    NSLog(@"ERROR al guardar context en + (NSMutableSet *) downloadSeasonsFromTVRageDb:(UCMLParser *)ucp withShow:(TVshow *)show inMoc:(NSManagedObjectContext *)givenContext, codi: %@",[mocError localizedDescription]);
                }
            }
            trTag = [ucp getTag];
        }
        [ucp goToNext:@" align='center'><font size='4'>"];
        seasonTitle = [ucp getNonTag];
    }
    
    return seasons;
}

+ (void)                   download:(NSString *)showName
               episodeListWithBrain:(PopKornBrain *)brain
             inManagedObjectContext:(NSManagedObjectContext *)givenContext
                       withDelegate:(id <tvShowDownloadDelegate>)delegate
{
    TVshow *show = [TVshow tvShowWithName:showName inContext:givenContext];
    
    NSTimeInterval seasonsInterval = [show.lastSeasonsUpdate timeIntervalSinceNow] / (60*60*24);
    if (seasonsInterval > -1 && seasonsInterval) return;
    
    UCMLParser *ucp = [[UCMLParser alloc] initWithURL:[[@"http://undeadcoders.com/pkdb/showDbV1_0" stringByAppendingString:show.url] stringByAppendingString:@"_episodes"]];
    NSMutableSet *seasons;
    if (ucp) {
        NSLog(@"Downloading %@ seasons off undeadcoders.com",showName);
        seasons = [self downloadSeasonsFromUndeadCodersDb:ucp withShow:show inMoc:givenContext];
    }
    else {
        ucp = [[UCMLParser alloc] initWithURL:[[@"http://tvrage.com" stringByAppendingString:show.url] stringByAppendingString:@"/episode_list/all"]];
        if (!ucp) {
            [delegate episodeListRequestUnsuccessful];
            NSLog(@"Downloading %@ seasons resulted in network error",showName);
            return;
        }
        NSLog(@"Downloading %@ seasons off TVRage.com",showName);
        seasons = [self downloadSeasonsFromTVRageDb:ucp withShow:show inMoc:givenContext];
    }
    [givenContext refreshObject:show mergeChanges:NO];
    [show addSeasons:seasons];
    show.lastSeasonsUpdate = [[NSDate alloc] init];
    
    NSError* mocError = nil;
    [givenContext save:&mocError];
    if (mocError) {
        NSLog(@"ERROR al guardar context en + (void) download:(NSString *)showName episodeListInManagedObjectContext:(NSManagedObjectContext *)givenContext withDelegate:(id <tvShowDownloadDelegate>)delegate, codi: %@",[mocError localizedDescription]);
    }

    NSString *yearTag = NSLocalizedString(@"year",nil);
    [givenContext refreshObject:show mergeChanges:NO];
    int year = show.year;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (year == 0)
            [delegate setYear:[yearTag stringByAppendingString:@"-"]];
        else
            [delegate setYear:[yearTag stringByAppendingString:[NSString stringWithFormat:@"%d",[TVshow tvShowWithName:showName inContext:givenContext].year]]];
        [delegate episodeListRequestSuccessful];
    });
    
    [brain.uiKache loadFavoritesInBackgroundWithContext:brain.context];
}

+ (void)download:(NSString*)showName infoInManagedObjectContext:(NSManagedObjectContext *)givenContext withDelegate:(id<tvShowDownloadDelegate>)delegate
{
    NSString *photoURL, *plot, *classification, *status;
    TVshow *show = [TVshow tvShowWithName:showName inContext:givenContext];
    
    NSTimeInterval interval = [show.lastInfoUpdate timeIntervalSinceNow] / (60*60*24);
    if (interval > -1 && interval) return;
    
    //NSLog(@"About to initialize ucp");
    UCMLParser *ucp = [[UCMLParser alloc] initWithURL:[@"http://undeadcoders.com/pkdb/showDbV1_0" stringByAppendingString:show.url]];
    if (ucp) {
        NSLog(@"Downloading %@ info off undeadcoders.com",showName);
        photoURL =       [ucp readBetween:@"<photoURL>"       and:@"</photoURL>"];
        plot =           [ucp readBetween:@"<plot>"           and:@"</plot>"];
        classification = [ucp readBetween:@"<classification>" and:@"</classification>"];
        status =         [ucp readBetween:@"<status>"         and:@"</status>"];
    } else {
        ucp = [[UCMLParser alloc] initWithURL:[@"http://tvrage.com" stringByAppendingString: show.url]];
        if (!ucp) {
#warning SET_ERROR
            [delegate requestUnsuccessful:nil];
            NSLog(@"Downloading %@ info resulted in network error",showName);
            return;
        }
        NSLog(@"Downloading %@ info off TVRage.com",showName);
        [ucp goToNext:@"<img style='max-width:270px;' "];
        [ucp goToNext:@"src='"];
        photoURL = [ucp readUntil:@"'"];
        [ucp goToNext:@"show_synopsis\">"];
        plot = [[[UCMLParser alloc] initWithString:[ucp readUntil:@"</div>"]] plainText];
        [ucp readUntil:@"<b>Classification</b>: "];
        classification = [ucp getNonTag];
        [ucp readUntil:@"<b>Status</b>: "];
        status = [ucp getNonTag];
    }
    
    [givenContext refreshObject:show mergeChanges:NO];
    show.photoURL = photoURL;
    show.plot = plot;
    show.classification = classification;
    show.status = status;
    show.lastInfoUpdate = [[NSDate alloc] init];
    
    NSError* mocError = nil;
    [givenContext save:&mocError];
    if (mocError) {
        NSLog(@"ERROR al guardar context en + (void)download:(NSString*)showName infoInManagedObjectContext:(NSManagedObjectContext *)givenContext withDelegate:(id<tvShowDownloadDelegate>)delegate, codi: %@",[mocError localizedDescription]);
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *classifTag = NSLocalizedString(@"classification",nil);
        [delegate setClassif:[classifTag stringByAppendingString:NSLocalizedString(classification,nil)]];
        
        NSString *statusTag = NSLocalizedString(@"status",nil);
        [delegate setStatus:[statusTag stringByAppendingString:NSLocalizedString(status,nil)]];
        
        [delegate setPlot:plot];
        
        [delegate setImage:[NSURL URLWithString:photoURL]];
    });
}

+ (void) downloadShowInfoInBackground:(NSString *)showName withDelegate:(id<tvShowDownloadDelegate>)givenDelegate withBrain:(PopKornBrain *)brain
{
    //NSLog(@"Downloading %@ info in background...",showName);
    dispatch_queue_t showInfoQueue = dispatch_queue_create("download show info", NULL);
    dispatch_async(showInfoQueue, ^{
        NSManagedObjectContext *threadContext = [[NSManagedObjectContext alloc] init];
        [threadContext setPersistentStoreCoordinator:brain.context.persistentStoreCoordinator];
        
        [self                   download:showName
              infoInManagedObjectContext:threadContext
                            withDelegate:givenDelegate];
        
        [self           download:showName
            episodeListWithBrain:brain
          inManagedObjectContext:threadContext
                    withDelegate:givenDelegate];
    });
}

@end
