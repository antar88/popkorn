//
//  CustomTabBarItem.h
//  popKorn
//
//  Created by Àlex Vergara Nebot on 02/11/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTabBarItem : UITabBarItem
{
    UIImage  *customHighlightedImage;
}

@property (nonatomic, retain) UIImage *customHighlightedImage;

@end
