//
//  CalendarMonthView.h
//  popKorn
//
//  Created by Antar Moratona on 20/09/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import "TapkuLibrary.h"
#import <UIKit/UIKit.h>
#import "PopKornBrain.h"
#import "EpisodeDetailView.h"


@interface CalendarMonthView : TKCalendarMonthTableViewController{
    PopKornBrain *brain;
	NSMutableArray *dataArray; 
	NSMutableDictionary *dataDictionary;
}

@property (strong) PopKornBrain *brain;
@property (strong,nonatomic) NSMutableArray *dataArray;
@property (strong,nonatomic) NSMutableDictionary *dataDictionary;


-(IBAction)tornar:(id)sender;
-(IBAction)avui:(id)sender;

@end
