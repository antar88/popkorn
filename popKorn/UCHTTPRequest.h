//
//  UCHTTPRequest.h
//  UCInternetUtils
//
//  Created by Àlex Vergara on 11/08/11.
//  Copyright 2011 Àlex Vergara. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UCHTTPRequest;

@protocol httpRequestDelegate <NSObject>
@required
- (void) httpRequest:(UCHTTPRequest *)request successful: (NSMutableData *)requestedData;
- (void) httpRequest:(UCHTTPRequest *)request unsuccessful: (NSError *)error;
@end

@interface UCHTTPRequest : NSObject //<NSURLConnectionDataDelegate>
{
@private
    id <httpRequestDelegate> delegate;
    NSURL *urlRequested;
    NSMutableData *receivedData;
    NSError *receivedError;
    NSString *notes;
}

/* PROPERTIES */
@property (strong) id delegate;
@property (readonly) NSURL *urlRequested;
@property (strong, readonly) NSMutableData *receivedData;
@property (readonly) NSError *receivedError;
@property (strong) NSString *notes;

/* INITIALIZERS */
- (id) initWithURL:(NSURL *)url;

/* METHODS */
- (void) startSynchronous;
- (void) startAsynchronousWithPOST:(NSString *)post withDelegate:(id)assignedDelegate;
- (void) startAsynchronousWithDelegate:(id)assignedDelegate;
- (void) startAsynchronousWithDelegate:(id)assignedDelegate withNotes:(NSString *)givenNotes;


@end
