//
//  AllShowsViewController.m
//  popKorn
//
//  Created by Àlex Vergara on 23/08/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import "AllShowsViewController.h"

@implementation AllShowsViewController

@synthesize pkb;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- initInManagedObjectContext:(NSManagedObjectContext *)context
{
    if (self = [super initWithStyle:UITableViewStylePlain]) {
        //self.tabBarItem.image = [UIImage imageNamed:@"allSeries"];
        
        CustomTabBarItem *tabItem = [[CustomTabBarItem alloc] initWithTitle:NSLocalizedString(@"allTvShows", nil) image:[UIImage imageNamed:@"allSeries"] tag:0];
        tabItem.customHighlightedImage=[UIImage imageNamed:@"orangeAllSeries"];
        self.tabBarItem = tabItem;
        self.title = NSLocalizedString(@"allTvShows", nil);

        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        request.entity = [NSEntityDescription entityForName:@"TVshow" inManagedObjectContext:context];
        request.sortDescriptors = [NSArray arrayWithObjects:
                                   [NSSortDescriptor sortDescriptorWithKey:@"sectionName" ascending:YES],
                                   [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES],nil];
        request.predicate = nil;

        NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                              managedObjectContext:context
                                                                                sectionNameKeyPath:@"firstLetterOfName"
                                                                                         cacheName:nil];        
        self.fetchedResultsController = frc;
        
        self.titleKey = @"name";
        
        self.searchKey = @"name";
    }
    return self;
}

- (void)managedObjectSelected:(NSManagedObject *)managedObject
{
    TVshow *show = (TVshow *) managedObject;
    //[pkb downloadShowInfo:show];
    ShowDetailView *dv = [[ShowDetailView alloc] init];
    dv.show = show;
    dv.pkb = self.pkb;
    [self.navigationController pushViewController:dv animated:YES];
}

- (UIImage *)thumbnailImageForManagedObject:(NSManagedObject *)managedObject
{
    TVshow *show = (TVshow *) managedObject;
    if ([show.isFavorite boolValue]) {
        int seen = show.seen;
        if (seen == seasonUnseen) return [UIImage imageNamed:@"favUnseen"];
        if (seen == seasonHalfSeen) return [UIImage imageNamed:@"favHalfseen"];
        return [UIImage imageNamed:@"favSeen"];
    }
    return nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

@end
