//
//  FeaturedViewControllerViewController.m
//  popKorn
//
//  Created by Àlex Vergara Nebot on 29/02/12.
//  Copyright (c) 2012 Undead Coders. All rights reserved.
//

#import "FeaturedViewController.h"

@interface FeaturedViewController ()

@end

@implementation FeaturedViewController
@synthesize pkb;
@synthesize table;

-(id) initWithPopKornBrain:(PopKornBrain *)brain
{
    self = [super init];
    if (self) {
        CustomTabBarItem *tabItem = [[CustomTabBarItem alloc] initWithTitle:NSLocalizedString(@"featured", nil) image:[UIImage imageNamed:@"featured"] tag:0];
        tabItem.customHighlightedImage=[UIImage imageNamed:@"orangeFeatured"];
        self.tabBarItem = tabItem;
        //self.title = NSLocalizedString(@"favorites", nil);
        
        sc = [[UISegmentedControl alloc] initWithItems:
              [NSArray arrayWithObjects:
               NSLocalizedString(@"top10", nil),
               NSLocalizedString(@"ourFavorites", nil),
               nil]];
        [sc         addTarget:self
                       action:@selector(segmentAction)
             forControlEvents:UIControlEventValueChanged];
        sc.selectedSegmentIndex = 0;
        sc.frame = CGRectMake(0,0,310,30);
        sc.segmentedControlStyle = UISegmentedControlStyleBar;
        UIBarButtonItem *sbi = [[UIBarButtonItem alloc] initWithCustomView:sc];
        self.navigationItem.rightBarButtonItem = sbi;
        
        self.pkb = brain;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - Table ops

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (sc.selectedSegmentIndex == 0)
        return [self.pkb.uiKache.top10 count];
    else
        return [self.pkb.uiKache.ourFavs count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"FeatShowCell";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
	}

    if (sc.selectedSegmentIndex == 0)
        cell.textLabel.text = [self.pkb.uiKache.top10 objectAtIndex:indexPath.row];
    else
        cell.textLabel.text = [self.pkb.uiKache.ourFavs objectAtIndex:indexPath.row];

    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    //per canviar el color de la cell quan es selecciona
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIColor orangeColor]];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *name;
    if (sc.selectedSegmentIndex == 0) {
        name = [self.pkb.uiKache.top10 objectAtIndex:indexPath.row];
        if (indexPath.row < 9) name = [name substringWithRange:NSMakeRange(3, name.length-3)];
        else name = [name substringWithRange:NSMakeRange(4, name.length-4)];
    }
    else
        name = [self.pkb.uiKache.ourFavs objectAtIndex:indexPath.row];

    TVshow *show = [TVshow tvShowWithName:name inContext:self.pkb.context];
    ShowDetailView *dv = [[ShowDetailView alloc] init];
    dv.show = show;
    dv.pkb = self.pkb;
    [self.navigationController pushViewController:dv animated:YES];
}

#pragma mark - Auxiliary ops

- (void) segmentAction {
    [self.table reloadData];
}

- (void) reloadTable {
    [self.table reloadData];
}

#pragma mark - View lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [self.table reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [self setTable:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
