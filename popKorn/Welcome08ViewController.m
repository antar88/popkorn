//
//  help01ViewController.m
//  popKorn
//
//  Created by Àlex Vergara Nebot on 27/10/11.
//  Copyright (c) 2011 Apple. All rights reserved.
//

#import "Welcome08ViewController.h"

@implementation Welcome08ViewController
@synthesize letsGo;
@synthesize label;
@synthesize label2;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (IBAction)letsGo:(id)sender {
    PopKornTabBarController *pktbc = [[PopKornTabBarController alloc] initWithPopKornBrain:brain];
    self.view.window.rootViewController = pktbc;
}

#pragma mark - ProgressBar

- (double)progress
{
    return progressBar.progress;
}

- (void)setProgress:(double)progress
{
    progressBar.progress = progress;
    if (progress == 1.0) {
        [UIView animateWithDuration:1.0
                         animations:^{
                             progressBar.alpha = 0;
                             self.label.alpha = 0;
                             self.label2.alpha = 1;
                             self.letsGo.alpha = 1;
                         }];
    }
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([TxDownloadShowList showListDownloaded]) {
        progressBar.alpha = 0;
        self.label.alpha = 0;
        self.label2.alpha = 1;
        self.letsGo.alpha = 1;
    }
    self.label.text = NSLocalizedString(@"welcomeWait", nil);
    self.label2.text = NSLocalizedString(@"welcomeLetsGo", nil);
    self.letsGo.titleLabel.text = NSLocalizedString(@"letsGo", nil);
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
