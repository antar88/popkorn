//
//  UIKache.m
//  popKorn
//
//  Created by Àlex Vergara Nebot on 27/02/12.
//  Copyright (c) 2012 Undead Coders. All rights reserved.
//

#import "UIKache.h"

#pragma Mark - KachedFavorite
@implementation KachedFavorite
@synthesize name, nextEp, thumbnail;
+(KachedFavorite *) favoriteWithName:(NSString *)name nextEp:(NSString *)nextEp thumbnail:(UIImage *)thumbnail {
    KachedFavorite *kf = [[KachedFavorite alloc] init];
    kf.name = name;
    kf.nextEp = nextEp;
    kf.thumbnail = thumbnail;
    return kf;
}
@end

#pragma Mark - UIKache
@implementation UIKache
@synthesize genres, favShows, unseenFavShows, top10, ourFavs;
@synthesize afsvc, fvc;

-(id) initInManagedObjectContext:(NSManagedObjectContext *)context {
    self = [super init];
    if (self) {
        genres = [[NSMutableArray alloc] init];
        favShows = [[NSMutableArray alloc] init];
        unseenFavShows = [[NSMutableArray alloc] init];
        top10 = [[NSMutableArray alloc] init];
        ourFavs = [[NSMutableArray alloc] init];
        working = NO;
        repeat = NO;
        [self loadGenresFromManagedObjectContext:context];
        [self loadFavoritesFromManagedObjectContext:context];
        [self loadFeaturedFromManagedObjectContext:context];
    }
    return self;
}

-(void) loadGenresFromManagedObjectContext:(NSManagedObjectContext *)context {
    //NSLog(@"[UIKache] Reloading genre list...");
    [genres removeAllObjects];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"Genre" inManagedObjectContext:context];
    request.sortDescriptors = [NSArray arrayWithObjects:
                               [NSSortDescriptor sortDescriptorWithKey:@"localizedName" ascending:YES],
                               nil];
    request.predicate = nil;
    NSArray *result = [context executeFetchRequest:request error:nil];
    for (Genre *g in result)
        [genres addObject:g.localizedName];
    //NSLog(@"[UIKache] Done");
}

- (UIImage *)thumbnailImageForShow:(int)seen
{
    if (seen == seasonUnseen) return [UIImage imageNamed:@"favUnseen"];
    if (seen == seasonHalfSeen) return [UIImage imageNamed:@"favHalfseen"];
    return [UIImage imageNamed:@"favSeen"];
}

-(void) loadFavoritesFromManagedObjectContext:(NSManagedObjectContext *)context {
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"TVshow" inManagedObjectContext:context];
    request.sortDescriptors = [NSArray arrayWithObjects:
                               [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES],
                               nil];
    request.predicate = [NSPredicate predicateWithFormat:@"isFavorite = %@",[NSNumber numberWithBool:YES]];
    NSArray *result = [context executeFetchRequest:request error:nil];
    
    
    NSMutableArray *favShowsTemp = [[NSMutableArray alloc] init];
    NSMutableArray *unseenFavShowsTemp = [[NSMutableArray alloc] init];
    
    for (TVshow *show in result) {
        int seen = show.seen;
        UIImage *img = [self thumbnailImageForShow:seen];
        [favShowsTemp addObject:[KachedFavorite favoriteWithName:show.name
                                                          nextEp:show.nextEpisode
                                                       thumbnail:img]];
        if (seen != seasonSeen)
            [unseenFavShowsTemp addObject:[KachedFavorite favoriteWithName:show.name
                                                                    nextEp:show.nextEpisode
                                                                 thumbnail:img]];
    }
    
    favShows = favShowsTemp;
    unseenFavShows = unseenFavShowsTemp;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"%@",afsvc);
        [afsvc reloadTable];
    });
}

-(void) loadFavoritesInBackgroundWithContext:(NSManagedObjectContext *)context {
    dispatch_queue_t favQueue = dispatch_queue_create("reload favs", NULL);
    dispatch_async(favQueue, ^{
        if (working) {
            repeat = YES;
        }
        else {
            working = YES;
            NSManagedObjectContext *moc = [[NSManagedObjectContext alloc] init];
            [moc setPersistentStoreCoordinator:context.persistentStoreCoordinator];
            [self loadFavoritesFromManagedObjectContext:moc];
            
            if (repeat) {
                repeat = NO;
                [self loadFavoritesInBackgroundWithContext:context];
            }
            else {
                working = NO;
            }
        }
    });
}

-(void) loadFeaturedFromManagedObjectContext:(NSManagedObjectContext *)context {
    ////NSLog(@"[UIKache] Reloading featured list...");
    FeaturedList *list = [FeaturedList featuredListInManagedObjectContext:context];
    
    NSArray *sortDescriptors = [NSArray arrayWithObjects: [NSSortDescriptor sortDescriptorWithKey:@"position" ascending:YES],nil];
    NSArray *top10list = [list.top10 sortedArrayUsingDescriptors:sortDescriptors];
    [top10 removeAllObjects];
    for (FeaturedShow *show in top10list)
        [top10 addObject:[NSString stringWithFormat:@"%d. %@",[show.position intValue],show.show.name]];
    
    [ourFavs removeAllObjects];
    for (FeaturedShow *show in list.ourFavs)
        [ourFavs addObject:show.show.name];
    ////NSLog(@"[UIKache] Done");
    dispatch_async(dispatch_get_main_queue(), ^{
        [fvc reloadTable];
    });
}

@end
