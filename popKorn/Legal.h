//
//  About.h
//  popKorn
//
//  Created by Antar Moratona on 12/09/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Legal : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *copyrightTitle;
@property (strong, nonatomic) IBOutlet UILabel *copyright;

@end
