//
//  PopKornBrain.m
//  popKorn
//
//  Created by Àlex Vergara on 23/08/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import "PopKornBrain.h"

@implementation PopKornBrain

#pragma mark - Properties
@synthesize context;
@synthesize sharer;
@synthesize uiKache;

#pragma mark - Initializers
- (id)init
{
    self = [super init];
    if (self) {
        sharer = [[UCSharer alloc] init];
    }
    return self;
}

#pragma mark - TVshow methods
- (void) setShow:(TVshow *)show asFavorite:(BOOL)isFavorite
{
    dispatch_queue_t parseQueue = dispatch_queue_create("favSet", NULL);
    dispatch_async(parseQueue, ^{
        NSManagedObjectContext *moc = [[NSManagedObjectContext alloc] init];
        [moc setPersistentStoreCoordinator:self.context.persistentStoreCoordinator];
        [TVshow tvShowWithName:show.name inContext:moc].isFavorite = [NSNumber numberWithBool:isFavorite];
        
        NSError* mocError = nil;
        [moc save:&mocError];
        if (mocError) {
            NSLog(@"ERROR al guardar context en - (void) setShow:(TVshow *)show asFavorite:(BOOL)isFavorite, codi: %@",[mocError localizedDescription]);
        }
        
        [self.uiKache loadFavoritesInBackgroundWithContext:moc];
        
        NSString *showName = [show.name stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:[@"http://www.undeadcoders.com/addfavorite.php?showname=" stringByAppendingString:showName]];
        [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    });
}

#pragma mark - Episode methods
- (NSMutableArray *) getAllPastUnseenEpisodes:(Episode *)episode
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"season.show = %@ AND date <= %@ AND seen = %@", episode.season.show,episode.date,[NSNumber numberWithBool:NO]];
    [request setEntity:[NSEntityDescription entityForName:@"Episode" inManagedObjectContext:self.context]];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSLog(@"[PopKornBrain] Getting all past unseen episodes in context %@....",context);
    NSMutableArray *episodes = [[self.context executeFetchRequest:request error:&error] mutableCopy];
    NSLog(@"[PopKornBrain] Done");
    
    return episodes;
}

- (BOOL) areTherePastUnseenEpisodes:(Episode *)episode
{
    if (!episode.date) return NO;
    return [[self getAllPastUnseenEpisodes:episode] count] > 0;
}

- (void) setPastEpisodesAsSeen:(Episode *)episode
{
    NSMutableArray *episodes = [self getAllPastUnseenEpisodes:episode];
    for (Episode *episode in episodes) episode.seen = [NSNumber numberWithBool:YES];
    
    NSError* mocError = nil;
    [self.context save:&mocError];
    if (mocError) {
        NSLog(@"ERROR al guardar context en - (void) setPastEpisodesAsSeen:(Episode *)episode, codi: %@",[mocError localizedDescription]);
    }
}

@end
