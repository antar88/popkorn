//
//  DetailView.h
//  popKorn
//
//  Created by Àlex Vergara on 26/08/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import "PopKornBrain.h"

#import "ShowInfoViewController.h"
#import "SeasonsView.h"
#import "TapkuLibrary.h"
#import "PopKornNotificator.h"

@interface ShowDetailView : UIViewController<tvShowDownloadDelegate, UCSharerDelegate, UCTranslateDelegate> {
    TVshow *show;
    PopKornBrain *pkb;
    IBOutlet UIView *showInfo;
    IBOutlet UITextView *showPlot;
    UITableView *showSeasons;
    UIImageView *tabImage;

    ShowInfoViewController *sivc;
    SeasonsView *sv;

    UIButton *butPlot;
    UIButton *butSeasons;
}

@property (nonatomic, strong) IBOutlet UIButton *butPlot;
@property (nonatomic, strong) IBOutlet UIButton *butSeasons;
@property (strong) TVshow *show;
@property (strong) PopKornBrain *pkb;
@property (nonatomic, strong) IBOutlet UIView *showInfo;
@property (nonatomic, strong) IBOutlet UITextView *showPlot;
@property (nonatomic, strong) IBOutlet UITableView *showSeasons;
@property (nonatomic, strong) IBOutlet UIImageView *tabImage;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *plotLoading;
@property (strong, nonatomic) IBOutlet UILabel *plotStatus;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *seasonsLoading;

@property (nonatomic, strong) ShowInfoViewController *sivc;
@property (nonatomic, strong) SeasonsView *sv;

- (IBAction)buttonPlot:(id)sender;
- (IBAction)buttonSeasons:(id)sender;

- (void) sendAuthNotification;

@end
