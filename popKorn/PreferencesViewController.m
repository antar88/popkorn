//
//  PreferencesViewController.m
//  popKorn
//
//  Created by Antar Moratona on 11/09/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import "PreferencesViewController.h"
#import "popKornAppDelegate.h"

#define datePicker 0
#define notifPicker 1
#define hourPicker 2
#define minutePicker 3

@implementation PreferencesViewController
@synthesize pref, filaSelec, brain;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        CustomTabBarItem *tabItem = [[CustomTabBarItem alloc] initWithTitle:NSLocalizedString(@"pref", nil) image:[UIImage imageNamed:@"gear"] tag:0];
        tabItem.customHighlightedImage=[UIImage imageNamed:@"orangeGear"];
        self.tabBarItem = tabItem;
        self.title = NSLocalizedString(@"pref", nil);
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)loadPrefsFirstTime { // carregar preferencies!!!!
    NSMutableArray *cal = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"numDaysBefore",nil), NSLocalizedString(@"numDaysAfter",nil),nil];
    
    NSMutableArray *notis = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"activate", nil),NSLocalizedString(@"when", nil),nil];
    
    NSMutableArray *traduccio = [[NSMutableArray alloc]initWithObjects: NSLocalizedString(@"translate", nil),nil];
    
    NSMutableArray *cache = [[NSMutableArray alloc]initWithObjects: NSLocalizedString(@"delCache", nil),nil];
    
    NSMutableArray *info = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"About", nil), @"Legal", @"FeedBack",nil];
    
    NSMutableArray *social = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"FaceLogout", nil),nil];
    
    self.pref = [[NSMutableArray alloc] initWithObjects:cal, notis, traduccio, cache, social, info,nil];  
    
    nombreDies = [NSArray arrayWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12",@"13",@"14",
                  @"15",@"16",@"17",@"18",@"19",@"20",@"21",@"22",@"23",@"24",@"25",@"26",@"27",@"28",@"29",@"30",
                  nil]; 
    
    nombreHores = [NSArray arrayWithObjects:@"00",@"01",@"02",@"03",@"04",@"05",@"06",@"07",@"08",@"09",@"10",@"11",@"12",@"13",@"14",
                   @"15",@"16",@"17",@"18",@"19",@"20",@"21",@"22",@"23",nil];
    
    nombreMinuts = [NSArray arrayWithObjects:@"00",@"15",@"30",@"45",nil];
    
    notisTitles = [[NSArray alloc]initWithObjects:NSLocalizedString(@"dayBefore", nil), NSLocalizedString(@"sameDay", nil), NSLocalizedString(@"dayAfter", nil), nil];
    
    pickHourNotis = [[UIDatePicker alloc] initWithFrame: CGRectMake(0, 73, 325, 250)];       
    
    pickHourNotis.datePickerMode  = UIDatePickerModeTime;
    
    /*
     NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
     [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss Z"];
     NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"NL"];
     NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
     [formatter setTimeZone:timeZone];
     [formatter setLocale:locale];
     
     NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
     NSDate *date = [gregorian dateFromComponents:[PKSettings notisDate]];
     NSString *dateString = [formatter stringFromDate:date];
     
     
     NSDate *date2 = [formatter dateFromString:dateString];
     pickHourNotis.locale =locale;
     */
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *date = [gregorian dateFromComponents:[PKSettings notisDate]];
    
    
    pickHourNotis.hidden = NO;
    pickHourNotis.minuteInterval = 15;
    
    
    pickHourNotis.date = date;
    
    [pickHourNotis addTarget:self
                      action:@selector(canviHoresNotis:)
            forControlEvents:UIControlEventValueChanged];
    
    notif.on = [PKSettings notis];
    traductor.on = [PKSettings translate];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadPrefsFirstTime];
    //pickDies.hidden = YES;
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)viewDidAppear:(BOOL)animated {
    [taula reloadData];
}

//PickerView

-(void)pickerViewLoaded: (UIPickerView *)myPickerView{
	NSUInteger max = 16384;
    if(myPickerView.tag == hourPicker) {
        NSUInteger base24 = (max/2)-(max/2)%24;
        [myPickerView selectRow:[myPickerView selectedRowInComponent:0]%24+base24 inComponent:0 animated:false];
    }
    else {
        NSUInteger base4 = (max/2)-(max/2)%4;
        [myPickerView selectRow:[myPickerView selectedRowInComponent:0]%4+base4 inComponent:0 animated:false];
    }
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(pickerView.tag == datePicker) return [NSString stringWithFormat:@"                      %@",[nombreDies objectAtIndex:row]];
    else if (pickerView.tag == notifPicker) return [NSString stringWithFormat:@"%@",[notisTitles objectAtIndex:row]];
    else if (pickerView.tag == hourPicker) return [NSString stringWithFormat:@" %@",[nombreHores objectAtIndex:row%24]];
    else if (pickerView.tag == minutePicker) return [NSString stringWithFormat:@" %@",[nombreMinuts objectAtIndex:row%4]];
    return @"";
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(pickerView.tag == datePicker) return nombreDies.count;
    else if(pickerView.tag == notifPicker) return notisTitles.count;
    else if(pickerView.tag == hourPicker) return 16384;//return nombreHores.count;
    else if(pickerView.tag == minutePicker) return 16384;//return nombreMinuts.count;
    return 0;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if(pickerView.tag == datePicker) {
        if (self.filaSelec.row == 0) [PKSettings setDaysBefore:[nombreDies objectAtIndex:row]];
        else if (self.filaSelec.row == 1) [PKSettings setDaysAfter:[nombreDies objectAtIndex:row]];
    }
    else if(pickerView.tag == notifPicker) {
        if(row == 0) [PKSettings setNotisDay:@"0"];
        else if (row == 1) [PKSettings setNotisDay:@"1"];
        else [PKSettings setNotisDay:@"2"];
        PopKornNotificator *pkn = [[PopKornNotificator alloc]init];
        pkn.brain = self.brain;
        [pkn scheduleNotifications];
    }
    else if(pickerView.tag == hourPicker) {
        [self pickerViewLoaded:pickerView];
        [PKSettings setNotisHour:[nombreHores objectAtIndex:row%24]];
        PopKornNotificator *pkn = [[PopKornNotificator alloc]init];
        pkn.brain = self.brain;
        [pkn scheduleNotifications];
    }
    else if(pickerView.tag == minutePicker) {
        [self pickerViewLoaded:pickerView];
        [PKSettings setNotisMinutes:[nombreMinuts objectAtIndex:row%4]];
        PopKornNotificator *pkn = [[PopKornNotificator alloc]init];
        pkn.brain = self.brain;
        [pkn scheduleNotifications];
    }
    [taula reloadData];
}


//custom showsPickers

- (void) showPicker{
    //Definim menu
    UIActionSheet *menu = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"selectDays", nil) 
                                                      delegate:self
                                             cancelButtonTitle:NSLocalizedString(@"done", nil)
                                        destructiveButtonTitle:nil 
                                             otherButtonTitles:nil];
    //Definim Picker
    UIPickerView *pickerView = [[UIPickerView alloc] init];
    pickerView.frame = CGRectMake(0, 94, 320, 162);                           
    pickerView.delegate = self;
    pickerView.tag = datePicker;
    pickerView.showsSelectionIndicator = YES; 
    if(self.filaSelec.row == 0) [pickerView selectRow:([[PKSettings daysBefore]integerValue]-1) inComponent:0 animated:YES];
    else if(self.filaSelec.row == 1) [pickerView selectRow:([[PKSettings daysAfter]integerValue]-1) inComponent:0 animated:YES];
    //Afegim Picker al menu
    menu.alpha = 0.8;
    [menu addSubview:pickerView];
    [menu showFromTabBar:self.tabBarController.tabBar];
    [menu setBounds:CGRectMake(0,0,320, 400)]; 
    //Relases
}


-(void) canviHoresNotis:(id) sender{
    NSDateComponents *hourAndMinutes = [[NSCalendar currentCalendar] components:NSHourCalendarUnit | NSMinuteCalendarUnit fromDate:pickHourNotis.date];
    [PKSettings setNotisHour:[[NSString alloc] initWithFormat:@"%d",hourAndMinutes.hour]];
    [PKSettings setNotisMinutes:[[NSString alloc] initWithFormat:@"%d",hourAndMinutes.minute]];
    [taula reloadData];
    PopKornNotificator *pkn = [[PopKornNotificator alloc]init];
    pkn.brain = self.brain;
    [pkn scheduleNotifications];
}


- (void) showDatePicker{
    //Definim menu
    UIActionSheet *menu = [[UIActionSheet alloc] initWithTitle:nil
                                                      delegate:self
                                             cancelButtonTitle:NSLocalizedString(@"done", nil)
                                        destructiveButtonTitle:nil
                                             otherButtonTitles:nil];
    //Definim DatePiker
    UIPickerView *hores = [[UIPickerView alloc]init];
    hores.hidden = NO;
    hores.tag = hourPicker;
    hores.frame = CGRectMake(180,74,75, 216);
    hores.delegate = self;
    hores.showsSelectionIndicator = YES;
    
    UIPickerView *minuts = [[UIPickerView alloc]init];
    minuts.hidden = NO;
    minuts.tag = minutePicker;
    minuts.frame = CGRectMake(245,74,75, 216);
    minuts.delegate = self;
    minuts.showsSelectionIndicator = YES;
    
    //Definim Picker
    UIPickerView *picDiesNotis = [[UIPickerView alloc]init];
    picDiesNotis.hidden = NO;
    picDiesNotis.tag = notifPicker;
    picDiesNotis.frame = CGRectMake(0,74,190, 216);
    picDiesNotis.delegate = self;
    picDiesNotis.showsSelectionIndicator = YES;
    
    //transparencies
    picDiesNotis.alpha = 1.0;
    //pickHourNotis.alpha = 1.0;
    
    //Afegim Pickers al menu
    menu.alpha = 0.7;
    //[menu addSubview:pickHourNotis];
    [menu addSubview:hores];
    [menu addSubview:minuts];
    [menu addSubview:picDiesNotis];
    [menu showFromTabBar:self.tabBarController.tabBar];
    [menu setBounds:CGRectMake(0,0,320, 500)]; 
    
    //seleccionem dia i hora anterior
    if([[PKSettings notisDay]intValue] == 0) [picDiesNotis selectRow:0 inComponent:0 animated:YES];
    else if ([[PKSettings notisDay]intValue] == 1) [picDiesNotis selectRow:1 inComponent:0 animated:YES];
    else [picDiesNotis selectRow:2 inComponent:0 animated:YES];
    [hores selectRow:[[PKSettings notisHour]intValue]+24*3 inComponent:0 animated:NO];
    int val = [[PKSettings notisMinutes]intValue];
    if (val == 15) val = 1;
    else if (val == 30) val = 2;
    else if (val == 45) val = 3;
    [minuts selectRow:val+400 inComponent:0 animated:NO];
    //Relases
}

// TAULA

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  self.pref.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self.pref objectAtIndex:section] count]; 
}



- (unsigned long long int)folderSize:(NSString *)folderPath {
    NSArray *filesArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:folderPath error:nil];
    NSEnumerator *filesEnumerator = [filesArray objectEnumerator];
    NSString *fileName;
    unsigned long long int fileSize = 0;
    
    while (fileName = [filesEnumerator nextObject]) {
        NSDictionary *fileDictionary = [[NSFileManager defaultManager] fileAttributesAtPath:[folderPath stringByAppendingPathComponent:fileName] traverseLink:YES];
        fileSize += [fileDictionary fileSize];
    }
    
    return fileSize;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) return NSLocalizedString(@"calendar",nil);
    else if(section == 1) return NSLocalizedString(@"noti", nil);
    else if(section == 2) return NSLocalizedString(@"translator", nil);
    else if(section == 3) {
        NSMutableString *titolCache = [[NSMutableString alloc] initWithString:@"Cache: "];
        unsigned long long int tam = [self folderSize:[UCImageCache pathCache]];
        //NSLog (@"%@",[titolCache stringByAppendingFormat:@"%d kB", tam/1000]);
        NSString *tamany = [NSString stringWithFormat:@"%d",tam/10000];
        while ([tamany length] < 3) tamany = [@"0" stringByAppendingString:tamany];
        tamany = [[[tamany substringToIndex:[tamany length]-2] stringByAppendingString:@","] stringByAppendingString:[tamany substringFromIndex:[tamany length]-2]];
        return [titolCache stringByAppendingFormat:@"%@ MB", tamany];
    }
    else if (section == 5)return NSLocalizedString(@"info",nil);
    else return NSLocalizedString(@"Social",nil);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSMutableString *CellIdentifier = [[NSMutableString alloc] initWithString:@"ShowCell"];
    if((indexPath.section == 1 || indexPath.section == 2) && indexPath.row == 0) CellIdentifier = (NSMutableString *)@"SwitchCell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
        if (indexPath.section == 2) { //Configuració - Traducció
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.accessoryView = traductor;
        }
        else if (indexPath.section == 1 && indexPath.row == 0) {  // notificacions - Enable
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.accessoryView = notif;
        }
		else cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
	}
    
    cell.textLabel.text = (NSString *)[[pref objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    cell.detailTextLabel.text = @"";
    
    if (indexPath.section == 0){ // Dies
        cell.accessoryType = UITableViewCellAccessoryNone;
        if (indexPath.row == 0) cell.detailTextLabel.text = [PKSettings daysBefore];
        else if (indexPath.row == 1) cell.detailTextLabel.text = [PKSettings daysAfter];
    }
    else if (indexPath.section == 1){  //Notificacions
        if (indexPath.row == 1) { 
            NSString *notisConf;
            if ([[PKSettings notisDay]intValue] == 0) notisConf = NSLocalizedString(@"dayBefore", nil);
            else if ([[PKSettings notisDay]intValue] == 1) notisConf = NSLocalizedString(@"sameDay", nil);
            else notisConf = NSLocalizedString(@"dayAfter", nil);
            if([[PKSettings notisHour]intValue] == 1) cell.detailTextLabel.text = [notisConf stringByAppendingFormat:@" %@ %@",NSLocalizedString(@"at", nil) ,[PKSettings notisHourMinutes]];
            else cell.detailTextLabel.text = [notisConf stringByAppendingFormat:@" %@ %@",NSLocalizedString(@"ats", nil) ,[PKSettings notisHourMinutes]];
        }
        if(indexPath.row != 0) {
            if(notif.on) {
                cell.textLabel.alpha = 1.0;
                cell.detailTextLabel.alpha = 1.0;
                cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                cell.userInteractionEnabled = YES;
            }
            else { // no tenim actives les notificacions
                cell.textLabel.alpha = 0.439216f;
                cell.detailTextLabel.alpha = 0.439216f;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.userInteractionEnabled = NO;
            } 
        }
    }
    else if(indexPath.section == 2) { // traduccio
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.detailTextLabel.text = NSLocalizedString(@"bTranslate", nil);
        
    }
    else if(indexPath.section == 3 || indexPath.section == 4) { // cache & social
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    else {
        if(indexPath.row != 2) cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        else cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.filaSelec = indexPath;
    if (indexPath.section == 0)  { //configuració 
        if(indexPath.row == 0 || indexPath.row == 1) { //nombre de dies 
            [self showPicker];
        } 
    }
    else if(indexPath.section == 1) { //notificacions 
        if(indexPath.row == 1) [self showDatePicker]; //datePicker
    }
    else if(indexPath.section == 3 && indexPath.row  == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"delCache",nil) 
                                                        message:NSLocalizedString(@"delCacheConfirm",nil) 
                                                       delegate:self 
                                              cancelButtonTitle:NSLocalizedString(@"no",nil) 
                                              otherButtonTitles:NSLocalizedString(@"yes",nil), nil];
        [alert show];
    }
    else if(indexPath.section == 5) { //Info
        if(indexPath.row == 0) {
            About *about = [[About alloc]init];
            [self.navigationController pushViewController:about animated:YES];
        }
        else if (indexPath.row == 1) {
            Legal *legal = [[Legal alloc]init];
            [self.navigationController pushViewController:legal animated:YES];
        }
        else if (indexPath.row == 2) {
            brain.sharer.delegate = self;
            [brain.sharer postMail:NSLocalizedString(@"feedback",nil) withSubject:@"FeedBack PopKorn" toMail:@"support@undeadcoders.com" ];
        }
    }
    else if (indexPath.section == 4) { //Social
        popKornAppDelegate *appDelegate = (popKornAppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate.facebook logout];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if ([defaults objectForKey:@"FBAccessTokenKey"]) {
            [defaults removeObjectForKey:@"FBAccessTokenKey"];
            [defaults removeObjectForKey:@"FBExpirationDateKey"];
            [defaults synchronize];
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

//Alert View

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [UCImageCache deleteCache];
        [taula reloadData];
    }   
}



//IBACTIONS

- (IBAction)notificationsChangeStat:(id)sender {
    [PKSettings setNotis:notif.on];
    [taula reloadData];
    PopKornNotificator *pkn = [[PopKornNotificator alloc]init];
    pkn.brain = self.brain;
    [pkn scheduleNotifications];
}

- (IBAction)translatorChangeStat:(id)sender {
    [PKSettings setTranslate:traductor.on];
    [taula reloadData];
}


@end
