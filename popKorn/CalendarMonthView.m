//
//  CalendarMonthView.m
//  popKorn
//
//  Created by Antar Moratona on 20/09/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import "CalendarMonthView.h"

@implementation CalendarMonthView
@synthesize dataArray, dataDictionary, brain;


- (void) viewDidLoad{
	[super viewDidLoad];
    self.navigationController.navigationBar.topItem.title = NSLocalizedString(@"calendar", nil);
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"listView", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(tornar:)]; 
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"today", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(avui:)]; 
    self.navigationItem.hidesBackButton = YES;
	[self.monthView selectDate:[[NSDate alloc]init] ];
}

-(IBAction)tornar:(id)sender {
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration: 0.8];
	//Definir tipus d'animaciÃ³ i push del VC
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:YES];
	[self.navigationController popToRootViewControllerAnimated:NO];
	//Start Animation
	[UIView commitAnimations];
}

-(IBAction)avui:(id)sender {
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	[gregorian setTimeZone:[NSTimeZone localTimeZone]];
	NSDateComponents *comp = [gregorian components:(NSMonthCalendarUnit | NSMinuteCalendarUnit | NSYearCalendarUnit | 
													NSDayCalendarUnit | NSWeekdayCalendarUnit | NSHourCalendarUnit | NSSecondCalendarUnit) 
										  fromDate:[NSDate date]];
    

    
    //NSLog(@"1 - %d/%d/%d %d/%d/%d", comp.day, comp.month, comp.year, comp.hour, comp.minute, comp.second);
    
    NSDate *date = [gregorian dateFromComponents:comp];
/*
    NSDate *date = [gregorian dateFromComponents:comp];

    NSLog(@"2 - %@", date);

    NSDateFormatter *formatterNew = [[NSDateFormatter alloc] init];
    [formatterNew setDateFormat:@"dd-MM-yyyy HH:mm:ss "]; 
    NSLog(@"%@", [formatterNew stringFromDate:date]);
    
    NSDate *dataN = [formatterNew dateFromString:[formatterNew stringFromDate:date] ];
     NSLog(@"3 - %@", dataN);
    
    //[self.monthView selectDate: date];*/
    [self.monthView selectDate: date];
    [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
    NSDate *seleccionat = self.monthView.dateSelected;
    [self.monthView reload];
    [self.monthView selectDate:seleccionat];
    
    [self.tableView reloadData];
}

- (void) viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
}


- (NSArray*) calendarMonthView:(TKCalendarMonthView*)monthView marksFromDate:(NSDate*)startDate toDate:(NSDate*)lastDate{
    //Creem les arrays
    self.dataArray = [NSMutableArray array];
	self.dataDictionary = [NSMutableDictionary dictionary];
    
    //Consulta
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"season.show.isFavorite = %@ AND (date >= %@) AND (date <= %@)", [NSNumber numberWithBool:YES], startDate, lastDate];
    [request setEntity:[NSEntityDescription entityForName:@"Episode" inManagedObjectContext:brain.context]];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSLog(@"[CalendarMonthView] Requesting favorite episodes in context %@....",brain.context);
    NSArray *results = [brain.context executeFetchRequest:request error:&error];
    NSLog(@"[CalendarMonthView] Done!");
    
    results = [results sortedArrayUsingSelector:@selector(dateCompare:)];
	NSDate *d = startDate;
    int i = 0;
	while(YES){
        NSComparisonResult comparacio = NSOrderedAscending;
        if(i < results.count) comparacio = [[[results objectAtIndex:i]date] compare:d];
		if(comparacio == NSOrderedSame) {
            NSMutableArray *epis = [[NSMutableArray alloc] init];
            while (i < results.count && [[[results objectAtIndex:i]date] compare:d] == NSOrderedSame) {
                /*
                 NSString *nom = [[[NSString alloc] initWithString:[[results objectAtIndex:i]name]]autorelease];
                 NSString *serie = [[[[[results objectAtIndex:i]season]show]name]autorelease];
                 NSNumber *vist = [NSNumber alloc];
                 Episode *ep = [results objectAtIndex:i];
                 vist = [[results objectAtIndex:i]seen];
                 NSArray *trio =  [[NSArray alloc] initWithObjects:nom, serie, vist, ep, nil];
                 [epis addObject: trio];*/
                [epis addObject:[results objectAtIndex:i]];
                ++i;
            }
            [self.dataDictionary setObject:epis forKey:d];
			[self.dataArray addObject:[NSNumber numberWithBool:YES]];
		}
        else [self.dataArray addObject:[NSNumber numberWithBool:NO]];
		
		TKDateInformation info = [d dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
		info.day++;
		d = [NSDate dateFromDateInformation:info timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
		if([d compare:lastDate]==NSOrderedDescending) break;
	}
	return dataArray;
}
- (void) calendarMonthView:(TKCalendarMonthView*)monthView didSelectDate:(NSDate*)date{
	// CHANGE THE DATE TO YOUR TIMEZONE
	//TKDateInformation info = [date dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	//NSDate *myTimeZoneDay = [NSDate dateFromDateInformation:info timeZone:[NSTimeZone systemTimeZone]];
	//NSLog(@"Date Selected: %@",myTimeZoneDay);
	[self.tableView reloadData];
}
- (void) calendarMonthView:(TKCalendarMonthView*)mv monthDidChange:(NSDate*)d animated:(BOOL)animated{
	[super calendarMonthView:mv monthDidChange:d animated:animated];
	[self.tableView reloadData];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
	
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {	
	NSArray *ar = [dataDictionary objectForKey:[self.monthView dateSelected]];
	if(ar == nil) return 0;
    return [ar count]+1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row != [tableView numberOfRowsInSection:indexPath.section]-1) {
        NSArray *ar = [dataDictionary objectForKey:[self.monthView dateSelected]];
        Episode *episode = [ar objectAtIndex:indexPath.row];
        EpisodeDetailView *edv = [[EpisodeDetailView alloc] init];
        edv.episode = episode;
        edv.pkb = self.brain;
        [self.navigationController pushViewController:edv animated:YES];
    }

    
}



- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }

    
    if (indexPath.row == [tv numberOfRowsInSection:indexPath.section]-1) {
        cell.textLabel.text = @"";
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.imageView.image = nil;
        cell.detailTextLabel.text = @"";
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } 
    NSArray *ar = [dataDictionary objectForKey:[self.monthView dateSelected]];
    Episode *ep =  [ar objectAtIndex:indexPath.row];
	cell.textLabel.text = ep.displayName;
	cell.detailTextLabel.text = ep.season.show.name;
    cell.imageView.image = [ep.seen boolValue] ? [UIImage imageNamed:@"seen"] : [UIImage imageNamed:@"unSeen"];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
	
    //per canviar el color de la cell quan es selecciona
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIColor orangeColor]];
    [cell setSelectedBackgroundView:bgColorView];

    
    return cell;
    
}



@end