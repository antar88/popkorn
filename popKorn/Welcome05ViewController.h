//
//  help01ViewController.h
//  popKorn
//
//  Created by Àlex Vergara Nebot on 27/10/11.
//  Copyright (c) 2011 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WelcomeViewController.h"

@interface Welcome05ViewController : WelcomeViewController

@property (strong, nonatomic) IBOutlet UILabel *label01;
@property (strong, nonatomic) IBOutlet UILabel *label02;

@end
