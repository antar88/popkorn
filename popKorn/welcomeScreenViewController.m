
//
//  firstLaunchViewController.m
//  popKorn
//
//  Created by Àlex Vergara on 05/09/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import "welcomeScreenViewController.h"

@implementation welcomeScreenViewController

@synthesize brain;
@synthesize contentController;
@synthesize showListFinished,userIsWaiting;
@synthesize numberOfShows, showsDownloaded;
@synthesize viewControllers;

@synthesize splashImg;
@synthesize splashBox;
@synthesize pageControl;
@synthesize scrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.showListFinished = NO;
        self.userIsWaiting = NO;
    }
    return self;
}

- (void)checkAuth
{
    //UIImage *twitterImage = brain.sharer.twitterIsAuthorized ? [UIImage imageNamed:@"twitterChecked"] : [UIImage imageNamed:@"twitterUnchecked"];
    //[twitterButton setImage:twitterImage forState:UIControlStateNormal];
    //UIImage *facebookImage = brain.sharer.facebookIsAuthorized ? [UIImage imageNamed:@"facebookChecked"] : [UIImage imageNamed:@"facebookUnchecked"];
    //[facebookButton setImage:facebookImage forState:UIControlStateNormal];
}

- (void)sendAuthNotification
{
    [self checkAuth];
}

- (IBAction)valueChanged:(id)sender {
    
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (CGRect) moveFrame:(CGRect)frame x:(int)x
{
    frame.origin.x += x;
    return frame;
}

- (CGRect) moveFrame:(CGRect)frame y:(int)y
{
    frame.origin.y += y;
    return frame;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.splashBox.frame = [self moveFrame:splashBox.frame y:300];
    self.scrollView.frame = [self moveFrame:scrollView.frame y:300];
    self.pageControl.frame = [self moveFrame:pageControl.frame y:300];
    
    [self checkAuth];
    
    [UIView animateWithDuration:1.5
                     animations:^{
                         splashImg.frame = [self moveFrame:splashImg.frame y:-150];
                         self.splashBox.frame = [self moveFrame:splashBox.frame y:-300];
                         self.scrollView.frame = [self moveFrame:scrollView.frame y:-300];
                         self.pageControl.frame = [self moveFrame:pageControl.frame y:-300];
                     }];
    
    self.contentController.brain = self.brain;
    self.contentController.superview = self;
}
- (void) setShowsDownloaded:(int)number {
    showsDownloaded = number;
    contentController.progress = (float)number/(float)numberOfShows;
}

- (void)requestSuccessful {
    NSLog(@"All downloads over!");
    
    [TxDownloadFeatured downloadFeaturedInContext:self.brain.context];
    [self.brain.uiKache loadGenresFromManagedObjectContext:self.brain.context];
    [self.brain.uiKache loadFeaturedFromManagedObjectContext:self.brain.context];
    
    if (self.userIsWaiting) {
        PopKornTabBarController *pktbc = [[PopKornTabBarController alloc] initWithPopKornBrain:brain];
        self.view.window.rootViewController = pktbc;
    }
    else self.showListFinished = YES;
}

- (void)requestUnsuccessful:(NSError *)error {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error",nil) message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"User has acknowledged no Internet Connection Error");
}

- (void)viewDidUnload
{
    [self setPageControl:nil];
    [self setScrollView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
