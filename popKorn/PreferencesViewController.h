//
//  PreferencesViewController.h
//  popKorn
//
//  Created by Antar Moratona on 11/09/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import "PopKornBrain.h"
#import "PopKornNotificator.h"
#import "About.h"
#import "Legal.h"
#import "CustomTabBarItem.h"


//#warning TRY_TO_REMOVE
@class PopKornNotificator;
@class PopKornBrain;

@interface PreferencesViewController : UIViewController <UITableViewDelegate,UITableViewDataSource, UISearchDisplayDelegate,UIPickerViewDelegate, UIPickerViewDataSource, UIActionSheetDelegate,UCSharerDelegate, UIAlertViewDelegate> {
    IBOutlet UITableView *taula;
    NSMutableArray *pref;
    NSArray *nombreDies, *notisTitles, *nombreHores, *nombreMinuts;
    NSIndexPath *filaSelec;
    IBOutlet UISwitch *notif, *traductor;
    IBOutlet UIDatePicker *pickHourNotis;
    PopKornBrain *brain;
}

@property(strong) NSMutableArray *pref;
@property(strong) NSIndexPath *filaSelec;
@property(strong) PopKornBrain *brain;

- (IBAction)notificationsChangeStat:(id)sender;
- (IBAction)translatorChangeStat:(id)sender;

@end


