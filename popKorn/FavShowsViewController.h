//
//  FavShowsViewController.h
//  popKorn
//
//  Created by Àlex Vergara on 27/08/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataTableViewController.h"
#import "PopKornBrain.h"
#import "ShowDetailView.h"

@interface FavShowsViewController : CoreDataTableViewController {
@private
    PopKornBrain *pkb;
}

@property (strong) PopKornBrain *pkb;

- initInManagedObjectContext:(NSManagedObjectContext *)context;

@end
