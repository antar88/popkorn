//
//  CalendarViewController.h
//  popKorn
//
//  Created by Antar Moratona on 08/09/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopKornBrain.h"
#import "EpisodeDetailView.h"
#import "CalendarMonthView.h"

@interface CalendarViewController : UIViewController <UITableViewDelegate,UITableViewDataSource, UISearchDisplayDelegate>{
    PopKornBrain *brain;
    NSMutableArray *days;
    UITableView *taula;
}

@property (strong) PopKornBrain *brain;
@property (strong) NSMutableArray *days;
@property (nonatomic, strong) IBOutlet UITableView *taula;

- (IBAction)canviarCalendari:(id)sender;

- (void)loadEpisodeCalendar;

@end
