//
//  Episode.h
//  PopKorn
//
//  Created by Àlex Vergara on 26/08/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "UCInternetUtils.h"
#import "Season.h"

@class Season;

#pragma Mark - episodeDownloadDelegate protocol
@protocol episodeDownloadDelegate <NSObject>
@required
- (void) setImage:(NSURL *)imageUrl;
- (void) setPlot:(NSString *)plot;
- (void) requestUnsuccessful:(NSError *)error;
@end

@interface Episode : NSManagedObject

@property (nonatomic, strong) NSString * code;
@property (nonatomic, strong) NSDate * date;
@property (nonatomic, strong) NSDate * lastUpdate;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * plot;
@property (nonatomic, strong) NSString * plotLocale;
@property (nonatomic, strong) NSString * plotLocalized;
@property (nonatomic, strong) NSString * photoURL;
@property (nonatomic, strong) NSNumber * seen;
@property (nonatomic, strong) Season *season;
@property (nonatomic, strong) NSString * url;
@property (unsafe_unretained, readonly) NSString * localizedDate;

@property (unsafe_unretained, readonly) NSString * displayName;
@property (readonly) int year;

/* INITIALIZERS */
+ (Episode *) episodeWithCode:(NSString *)givenCode season:(Season *)givenSeason inContext:(NSManagedObjectContext *)context;

/* METHODS */
- (void) downloadInfoInManagedObjectContext:(NSManagedObjectContext *)moc withDelegate:(id<episodeDownloadDelegate>)delegate;

@end
