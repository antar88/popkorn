//
//  Episode.m
//  PopKorn
//
//  Created by Àlex Vergara on 26/08/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import "Episode.h"

@implementation Episode

@dynamic code;
@dynamic date;
@dynamic lastUpdate;
@dynamic name;
@dynamic plot;
@dynamic plotLocalized;
@dynamic plotLocale;
@dynamic photoURL;
@dynamic seen;
@dynamic season;
@dynamic url;

/* INITIALIZERS */
+ (Episode *) episodeWithCode:(NSString *)givenCode season:(Season *)givenSeason inContext:(NSManagedObjectContext *)context
{
    Episode *episode = nil;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"Episode" inManagedObjectContext:context];
    request.predicate = [NSPredicate predicateWithFormat:@"code = %@ AND season.show = %@ AND season = %@",givenCode,givenSeason.show,givenSeason];
    NSError *error = nil;
    //NSLog(@"[Episode] Loading episode with context %@....",context);
    episode = [[context executeFetchRequest:request error:&error] lastObject];
    //NSLog(@"[Episode] Done!");
    
    if (!error && !episode) {
        episode = [NSEntityDescription insertNewObjectForEntityForName:@"Episode" inManagedObjectContext:context];
        episode.code = givenCode;
        episode.season = givenSeason;
        
        NSError* mocError = nil;
        [context save:&mocError];
        if (mocError) {
            NSLog(@"ERROR al guardar context en + (Episode *) episodeWithCode:(NSString *)givenCode season:(Season *)givenSeason inContext:(NSManagedObjectContext *)context, codi: %@",[mocError localizedDescription]);
        }
    }
    return episode;
}

/* METHODS */
- (void) downloadInfoInManagedObjectContext:(NSManagedObjectContext *)moc withDelegate:(id<episodeDownloadDelegate>)delegate
{
    [moc refreshObject:self mergeChanges:NO];
    
    if (self.lastUpdate) {
        NSTimeInterval interval = [self.lastUpdate timeIntervalSinceNow] / (60*60*24);
        if (interval > -1) {
            return;
        }
    }
    dispatch_queue_t parseQueue = dispatch_queue_create("downloadEpisodeInfo", NULL);
    dispatch_async(parseQueue, ^{
        NSURL *url = [NSURL URLWithString:self.url];
        NSError* error = nil;
        NSString *data = [NSString stringWithContentsOfURL:url
                                                  encoding:NSUTF8StringEncoding
                                                     error:&error];
        
        if (error) {
            error = nil;
            data = [NSString stringWithContentsOfURL:url
                                            encoding:NSASCIIStringEncoding
                                               error:&error];
            if (error) {
                [delegate requestUnsuccessful:error];
            }
        }
        
        UCMLParser *ucp = [[UCMLParser alloc] initWithString:data];
        [ucp goToNext:@"<a class=\"addthis_button_facebook\">"];
        [ucp goToLast:@"<div>"];
        NSString *plot = [[[UCMLParser alloc] initWithString:[ucp readUntil:@"<span"]] plainText];
        if ([plot hasPrefix:@"Click here"]) plot = @"";
        
        NSString *photoURL = nil;
        UCMLParser *ucp2 = [[UCMLParser alloc] initWithString:data];
        [ucp2 goToNext:@"'max-width:280px'"];
        if (![ucp2 eof]) {
            [ucp2 goToLast:@"<img src='"];
            photoURL = [ucp2 readUntil:@"'"];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.plot = plot;
            self.photoURL = photoURL;
            self.lastUpdate = [[NSDate alloc] init];
            NSError* mocError = nil;
            [moc save:&mocError];
            if (mocError) {
                NSLog(@"ERROR al guardar context en - (void) downloadInfoInManagedObjectContext:(NSManagedObjectContext *)moc withDelegate:(id<episodeDownloadDelegate>)delegate, codi: %@",[mocError localizedDescription]);
                [delegate requestUnsuccessful:mocError];
            }
            
            [delegate setPlot:self.plot];
            if (self.photoURL) [delegate setImage:[NSURL URLWithString:self.photoURL]];
        });
    });
}

/* CONSULTORS */
- (int) year
{
    if (!self.date) return -1;
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *yearComponents = [gregorian components:NSYearCalendarUnit fromDate:self.date];
    return [yearComponents year];
}

- (NSComparisonResult) dateCompare:(Episode *)episode {
    return [self.date compare:episode.date];
}

- (NSString * ) displayName {
    return [self.code stringByAppendingFormat:@" %@",self.name];
}

- (NSString *) localizedDate {
    return [UCMLParser stringFromDate:self.date];
}

@end
