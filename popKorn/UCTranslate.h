//
//  UCTranslate.h
//  popKorn
//
//  Created by Antar Moratona on 17/09/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MRTranslationOperation.h"
#import "BingTranslator.h"

@protocol UCTranslateDelegate
@required
- (void)translationOperationDidFinishTranslatingText:(NSString *)result;
- (void)translationOperationDidFailWithError:(NSError *)error;
@end

@interface UCTranslate : NSObject <MRTranslationOperationDelegate, BingTranslatorDelegate> {
    NSOperationQueue *queue;
    id <UCTranslateDelegate> delegate;
}

@property (nonatomic, retain) id <UCTranslateDelegate> delegate;

-(void) translate:(NSString *)text toLanguage:(NSString *)language;

-(void) bingTranslate:(NSString *)text toLanguage:(NSString *)language;

-(NSString *) bingTranslateSync:(NSString *)text toLanguage:(NSString *)language;


@end




