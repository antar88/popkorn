//
//  AllShowsViewController.h
//  popKorn
//
//  Created by Àlex Vergara on 23/08/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataTableViewController.h"
#import "PopKornBrain.h"
#import "ShowDetailView.h"

@interface AllShowsViewController : CoreDataTableViewController {
@private
    PopKornBrain *pkb;
}

@property (strong) PopKornBrain *pkb;

- initInManagedObjectContext:(NSManagedObjectContext *)context;

@end
