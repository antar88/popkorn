//
//  UCHTTPRequest.m
//  UCInternetUtils
//
//  Created by Àlex Vergara on 11/08/11.
//  Copyright 2011 Àlex Vergara. All rights reserved.
//

#import "UCHTTPRequest.h"

@implementation UCHTTPRequest

/* PROPERTIES */
@synthesize delegate;
@synthesize urlRequested;
@synthesize receivedData;
@synthesize receivedError;
@synthesize notes;

/* INITIALIZERS */
- (id)init
{
    self = [super init];
    if (self) {
        urlRequested = [[NSURL alloc] init];
        receivedData = [[NSMutableData alloc] init];
        receivedError = [[NSError alloc] init];
    }
    
    return self;
}

- (id) initWithURL:(NSURL *)url
{
    self = [self init];
    urlRequested = url;
    return self;
}

/* METHODS */
- (void) startSynchronous {
    NSError *error;
    NSString *webPageContent = [NSString stringWithContentsOfURL:urlRequested
                                                        encoding:NSUTF8StringEncoding 
                                                           error:&error];
    if(webPageContent) {
        NSMutableData *test = [NSMutableData dataWithBytes:[webPageContent cStringUsingEncoding:NSUTF8StringEncoding]
                                                    length:[webPageContent length]];
        receivedData = test;
    }
    receivedError = error;
}

-(void) startAsynchronousWithDelegate:(id)assignedDelegate
{
    //NSLog(@"Begining connection...");
    // Handling delegate stuff
    delegate = assignedDelegate;
    // Create the request.
    NSMutableURLRequest *theRequest=[NSMutableURLRequest requestWithURL:urlRequested
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                        timeoutInterval:60.0];
    // create the connection with the request
    // and start loading the data
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    if (!theConnection) {
        NSLog(@"FAIL!");
    }
}

- (void) startAsynchronousWithDelegate:(id)assignedDelegate withNotes:(NSString *)givenNotes
{
    //NSLog(@"Begining connection with notes...");
    self.notes = givenNotes;
    [self startAsynchronousWithDelegate:assignedDelegate];
}

- (void) startAsynchronousWithPOST:(NSString *)post withDelegate:(id)assignedDelegate
{
    //NSLog(@"Begining connection with POST...!");
    // Handling delegate stuff
    delegate = assignedDelegate;
    // Create the request.
    NSMutableURLRequest *theRequest=[NSMutableURLRequest requestWithURL:urlRequested
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                        timeoutInterval:60.0];
    //POST
    NSData *myRequestData = [NSData dataWithBytes: [post UTF8String] length: [post length]];
    [theRequest setHTTPBody:myRequestData];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    // create the connection with the request
    // and start loading the data
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    if (!theConnection) {
        [assignedDelegate httpRequest:self unsuccessful:nil];
        NSLog(@"FAIL!");
    }
}

/* METODES DELEGATES FROM NSURLCONNECTION */
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // This method is called when the server has determined that it
    // has enough information to create the NSURLResponse.
    
    // It can be called multiple times, for example in the case of a
    // redirect, so each time we reset the data.
    
    // receivedData is an instance variable declared elsewhere.
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the new data to receivedData.
    // receivedData is an instance variable declared elsewhere.
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    // release the connection, and the data object
     //tret per ARC
    // receivedData is declared as a method instance elsewhere
     //tret per ARC
    
    // inform the user
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
    
    [delegate httpRequest:self unsuccessful:error];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    //NSLog(@"Succeeded! Received %d bytes of data",[receivedData length]);

    //call delegate method
    if ([self.delegate respondsToSelector:@selector(httpRequest: successful:)]) {
        [self.delegate httpRequest:self successful:self.receivedData];
    }
    else NSLog(@"WTF");
}

@end
