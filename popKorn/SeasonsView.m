//
//  SeasonsView.m
//  popKorn
//
//  Created by Àlex Vergara on 16/09/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import "SeasonsView.h"

@implementation SeasonsView

@synthesize brain;
@synthesize show;
@synthesize seasons;
@synthesize navCon;

- (void) loadSeasons
{
    [self.brain.context refreshObject:self.show mergeChanges:NO];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"show = %@", self.show];
    [request setEntity:[NSEntityDescription entityForName:@"Season" inManagedObjectContext:self.brain.context]];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSLog(@"[SeasonsView] Requesting seasons list in context %@",self.brain.context);
    seasons = [self.brain.context executeFetchRequest:request error:&error];
    NSLog(@"[SeasonsView] Done!");
    //seasons = [results sortedArrayUsingSelector:@selector(compare:)];
}

- (id)initWithShow:(TVshow *)givenShow brain:(PopKornBrain *)givenBrain
{
    self = [super init];
    if (self) {
        self.brain = givenBrain;
        self.show = givenShow;
        [self loadSeasons];
    }
    
    return self;
}

/* UITABLEVIEW DATASOURCE METHODS */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [seasons count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"SeasonCell";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
	}
    
    cell.textLabel.text = NSLocalizedString(((Season *)[seasons objectAtIndex:indexPath.row]).name,nil);
    if ([cell.textLabel.text hasPrefix:@"Season"] || [cell.textLabel.text hasPrefix:@"Series"]) {
        cell.textLabel.text = [NSLocalizedString([cell.textLabel.text substringToIndex:[@"Season" length]], nil) stringByAppendingString:[cell.textLabel.text substringFromIndex:[@"Season" length]]];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    int epNumber = ((Season *)[seasons objectAtIndex:indexPath.row]).episodes.count;
    cell.detailTextLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%d episodes",nil),epNumber];
    
    //Imatge lateral
    Season *season = [self.seasons objectAtIndex:indexPath.row];
    int seen = season.seen;
    if      (seen == seasonUnseen)   cell.imageView.image = [UIImage imageNamed:@"unSeen"];
    else if (seen == seasonHalfSeen) cell.imageView.image = [UIImage imageNamed:@"halfSeen"];
    else                             cell.imageView.image = [UIImage imageNamed:@"seen"];
    
    //per canviar el color de la cell quan es selecciona
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIColor orangeColor]];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Season *season = [seasons objectAtIndex:indexPath.row];
    EpisodesViewController *evc = [[EpisodesViewController alloc] initInManagedObjectContext:brain.context withSeason:season];
    evc.pkb = self.brain;
    //self.view
    [self.navCon pushViewController:evc animated:YES];
}

@end
