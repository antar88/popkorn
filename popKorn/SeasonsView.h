//
//  SeasonsView.h
//  popKorn
//
//  Created by Àlex Vergara on 16/09/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PopKornBrain.h"
#import "EpisodesViewController.h"

@interface SeasonsView : NSObject <UITableViewDelegate, UITableViewDataSource> {
@private
    PopKornBrain *brain;
    TVshow *show;
    NSArray *seasons;
    UINavigationController *navCon;
}

@property (strong) PopKornBrain *brain;
@property (strong) TVshow *show;
@property (strong) NSArray *seasons;
@property (strong) UINavigationController *navCon;

- (id)initWithShow:(TVshow *)givenShow brain:(PopKornBrain *)givenBrain;
- (void) loadSeasons;

@end
