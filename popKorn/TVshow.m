//
//  TVshow.m
//  popKorn
//
//  Created by Àlex Vergara on 23/08/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import "TVshow.h"

@implementation TVshow

/* PROPERTIES */
@dynamic genres;
@dynamic classification;
@dynamic name;
@dynamic photoURL;
@dynamic isFavorite;
@dynamic lastInfoUpdate;
@dynamic lastSeasonsUpdate;
@dynamic plot;
@dynamic plotLocale;
@dynamic plotLocalized;
@dynamic url;
@dynamic seasons;
@dynamic sectionName;
@dynamic status;

/* INITIALIZERS */
+ (TVshow *) tvShowWithName:(NSString *)givenName inContext:(NSManagedObjectContext *)givenContext
{
    TVshow *show = nil;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"TVshow" inManagedObjectContext:givenContext];
    request.entity = entity;
    request.predicate = [NSPredicate predicateWithFormat:@"name = %@",givenName];
    NSError *error = nil;
    //NSLog(@"[TVshow] Initializing show in context %@....",givenContext);
    show = [[givenContext executeFetchRequest:request error:&error] lastObject];
    //NSLog(@"[TVshow] Done");
    
    if (!error && !show) {
        show = [NSEntityDescription insertNewObjectForEntityForName:@"TVshow" inManagedObjectContext:givenContext];
        show.name = givenName;
        show.sectionName = [[givenName substringToIndex:1] capitalizedString];
        if ([show.sectionName characterAtIndex:0] < 'A' || [show.sectionName characterAtIndex:0] > 'Z')
            show.sectionName = @"#";
    }
    
    return show;
}

+ (TVshow *) tvShowWithName:(NSString *)givenName url:(NSString *)givenURL inContext:(NSManagedObjectContext *)givenContext
{
    //NSLog(@"Called tvShowWithName:%@ url:%@ inContext:%@",givenName,givenURL,givenContext);
    TVshow *show = nil;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"TVshow" inManagedObjectContext:givenContext];
    request.entity = entity;
    request.predicate = [NSPredicate predicateWithFormat:@"name = %@",givenName];
    NSError *error = nil;
    //NSLog(@"[TVshow] Initializing show in context %@....",givenContext);
    show = [[givenContext executeFetchRequest:request error:&error] lastObject];
    //NSLog(@"[TVshow] Done");
    
    if (!error && !show) {
        show = [NSEntityDescription insertNewObjectForEntityForName:@"TVshow" inManagedObjectContext:givenContext];
        show.name = givenName;
        show.sectionName = [[givenName substringToIndex:1] capitalizedString];
        if ([show.sectionName characterAtIndex:0] < 'A' || [show.sectionName characterAtIndex:0] > 'Z')
            show.sectionName = @"#";
        show.url = givenURL;
    }
    
    return show;
}

+ (TVshow *) createTvShowWithName:(NSString *)givenName url:(NSString *)givenURL inContext:(NSManagedObjectContext *)givenContext
{
    TVshow *show = [NSEntityDescription insertNewObjectForEntityForName:@"TVshow" inManagedObjectContext:givenContext];
    show.name = givenName;
    show.sectionName = [[givenName substringToIndex:1] capitalizedString];
    if ([show.sectionName characterAtIndex:0] < 'A' || [show.sectionName characterAtIndex:0] > 'Z')
        show.sectionName = @"#";
    show.url = givenURL;

    return show;
}

/* CONSULTORS */
- (int) year
{
    int year = 0;
    for (Season *s in self.seasons) {
        int y = s.year;
        //NSLog(@"%d",y);
        if (y != 0 && (y < year || year == 0)) year = y;
    }
    //NSLog(@"Result: %d",year);
    return year;
}

- (NSString *)firstLetterOfName
{
    return self.sectionName;
}

- (int) seen
{
    int seen = 0;
    int unseen = 0;
    for (Season *s in self.seasons) {
        int sSeen = s.seenAndEmitted;
        if (sSeen == seasonSeen) ++seen;
        else if (sSeen == seasonHalfSeen) return seasonHalfSeen;
        else ++unseen;
    }
    if (unseen == 0) return seasonSeen;
    if (seen == 0) return seasonUnseen;
    return seasonHalfSeen;
}

- (int) unseenEpisodes {
    int unseen = 0;
    for (Season *s in self.seasons)
        unseen += s.unseenEpisodes;
    return unseen;
}

- (NSString *) nextEpisode
{
    int unseen = [self unseenEpisodes];
    if (unseen)
        return [NSString stringWithFormat:NSLocalizedString(@"%d episodes pending",nil),unseen];
    
    int nDies = 1000;
    for (Season *s in self.seasons) {
        for (Episode *ep in s.episodes) {
            NSTimeInterval interval = [ep.date timeIntervalSinceNow];
            interval = interval / (60*60*24);
            int dies = interval;
            if (interval > dies) dies++;
            if (ep.date && dies >= 0 && dies < nDies) nDies = dies;
        }
    }
    if (nDies == 1000) return NSLocalizedString(self.status, nil);
    NSString *next = NSLocalizedString(@"nextEpisode", nil);
    if (nDies > 1) return [next stringByAppendingFormat:NSLocalizedString(@"in %d days",nil),nDies];
    if (nDies == 1) return [next stringByAppendingString:NSLocalizedString(@"tomorrow", nil)];
    return [next stringByAppendingString:NSLocalizedString(@"today", nil)];
}

@end
