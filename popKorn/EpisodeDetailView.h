//
//  EpisodeDetailView.h
//  popKorn
//
//  Created by Àlex Vergara on 02/09/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopKornBrain.h"
#import "TapkuLibrary.h"

@interface EpisodeDetailView : UIViewController<UCSharerDelegate, imagesControllerDelegate, episodeDownloadDelegate, UCTranslateDelegate> {
    Episode *episode;
    PopKornBrain *pkb;
    UIButton *shareTwitter;
    UIButton *shareFacebook;
    UIButton *shareMail;
    IBOutlet UILabel *epCode;
    IBOutlet UILabel *showInfo;
    IBOutlet UILabel *seasonInfo;
    IBOutlet UILabel *airDate;
    IBOutlet UITextView *txtPlot;
    UCImageCache *imgCnt;
}

@property (strong, nonatomic) Episode *episode;
@property (strong) PopKornBrain *pkb;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UIButton *shareTwitter;
@property (nonatomic, strong) IBOutlet UIButton *shareFacebook;
@property (nonatomic, strong) IBOutlet UIButton *shareMail;
@property (strong, nonatomic) IBOutlet UILabel *plotLabel;
@property (strong, nonatomic) IBOutlet UIImageView *epImage;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *plotLoading;
@property (strong, nonatomic) IBOutlet UILabel *plotStatus;

- (IBAction)shareTwitterPressed:(id)sender;
- (IBAction)shareFacebookPressed:(id)sender;
- (IBAction)shareMailPressed:(id)sender;

@end
