//
//  CustomTabBarItem.m
//  popKorn
//
//  Created by Àlex Vergara Nebot on 02/11/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import "CustomTabBarItem.h"

@implementation CustomTabBarItem

@synthesize customHighlightedImage;

-(UIImage *)selectedImage {
    return self.customHighlightedImage;
}

@end
