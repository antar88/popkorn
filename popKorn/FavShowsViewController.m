//
//  FavShowsViewController.m
//  popKorn
//
//  Created by Àlex Vergara on 27/08/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import "FavShowsViewController.h"

@implementation FavShowsViewController

@synthesize pkb;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- initInManagedObjectContext:(NSManagedObjectContext *)context
{
    if (self = [super initWithStyle:UITableViewStylePlain]) {
        //self.tabBarItem.image = [UIImage imageNamed:@"favSeries"];
        
        CustomTabBarItem *tabItem = [[CustomTabBarItem alloc] initWithTitle:NSLocalizedString(@"favorites", nil) image:[UIImage imageNamed:@"favSeries"] tag:0];
        tabItem.customHighlightedImage=[UIImage imageNamed:@"orangeFavSeries"];
        self.tabBarItem = tabItem;
        self.title = NSLocalizedString(@"favorites", nil);

        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        request.entity = [NSEntityDescription entityForName:@"TVshow" inManagedObjectContext:context];
        request.sortDescriptors = [NSArray arrayWithObjects:
                                   [NSSortDescriptor sortDescriptorWithKey:@"sectionName" ascending:YES],
                                   [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES],nil];
        request.predicate = [NSPredicate predicateWithFormat:@"isFavorite = %@",[NSNumber numberWithBool:YES]];
        
        NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                              managedObjectContext:context
                                                                                sectionNameKeyPath:nil
                                                                                         cacheName:nil];        
        self.fetchedResultsController = frc;
        
        self.titleKey = @"name";
        
        self.subtitleKey = @"nextEpisode";
        
        self.searchKey = @"name";
    }
    return self;
}

- (void)managedObjectSelected:(NSManagedObject *)managedObject
{
    TVshow *show = (TVshow *) managedObject;
    ShowDetailView *dv = [[ShowDetailView alloc] init];
    dv.show = show;
    dv.pkb = self.pkb;
    [self.navigationController pushViewController:dv animated:YES];
}

- (UITableViewCellAccessoryType)accessoryTypeForManagedObject:(NSManagedObject *)managedObject
{
	return UITableViewCellAccessoryDisclosureIndicator;
}

- (UIImage *)thumbnailImageForManagedObject:(NSManagedObject *)managedObject
{
    TVshow *show = (TVshow *) managedObject;
    int seen = show.seen;
    if (seen == seasonUnseen) return [UIImage imageNamed:@"favUnseen"];
    if (seen == seasonHalfSeen) return [UIImage imageNamed:@"favHalfseen"];
    return [UIImage imageNamed:@"favSeen"];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

@end

