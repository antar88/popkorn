//
//  imagesController.m
//  popKorn
//
//  Created by Antar Moratona Franco on 21/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "UCImageCache.h"

@implementation UCImageCache

#pragma mark initializer
- (id)init
{
    self = [super init];
    if (self) {
        self.cache = [self obtenirCache];
    }
    
    return self;
}

# pragma mark static methods


+(NSString *) filePathCache {
    NSString *filePath = [UCImageCache pathCache];
    if(!filePath) return nil;
    return [filePath stringByAppendingPathComponent:@"CacheDict.plist"];
}

+(NSString *) pathCache {
    NSError *err = nil;
    NSString *filePath = NSTemporaryDirectory();
    filePath = [filePath stringByAppendingPathComponent:@"popKornCache"];
    BOOL directory = YES;
    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:&directory]) {
        if([[NSFileManager defaultManager] createDirectoryAtPath:filePath
                                     withIntermediateDirectories:YES
                                                      attributes:nil
                                                           error:&err]) NSLog(@"[ImageCache] Create permanent folder succes!");
        else NSLog(@"[ImageCache] Create folder failed!");
    }
    if (err) return nil;
    return filePath;
}

+ (void) deleteCache {
    NSLog(@"[ImageCache] Esborrem la cache amb path %@", [UCImageCache filePathCache]);
    NSString *filePath = [UCImageCache filePathCache];
    if(filePath) {
        //esborrem totes les imatges que hi ha al diccionari
        NSError *error = nil;
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
        for (NSString *key in dic) {
            NSString *path = [dic valueForKey:key];
            if (![[NSFileManager defaultManager] removeItemAtPath:path error:&error])
                NSLog(@"[ImageCache] Error al esborrar l'arxiu: %@ de la cache! %@", path, error);
        }
        //esborrem el propi diccionari
        error = nil;
        if(![[NSFileManager defaultManager] removeItemAtPath:filePath error:&error])
            NSLog(@"[ImageCache] Error al esborrar el diccionari. Error: %@", error);
    }
}

#pragma mark - class method

- (UIImage *) requestImage:(NSURL *) photoUrl{
    
    if (!photoUrl) {
        [self.delegate imageRequestUnsuccesful:[[NSError alloc] initWithDomain:@"Nil photoUrl" code:1 userInfo:nil]];
        return nil;
    }
    
    NSString *urlString = [photoUrl absoluteString];
    NSString *path = (NSString *)[self.cache valueForKey:urlString];
    
    if (path) { //existeix una entrada en la cache amb aquesta url.
        UIImage *photo = [UIImage imageWithContentsOfFile:path];
        if(photo) return photo;
        //la foto estava corrupte. Esborrem entrada a la cache.
        [self.cache removeObjectForKey:urlString];
    }
    //la imatge no està a la cache. L'hem de sol·licitar asicronament i esperar.
    
    NSString *nouPath = [self nouPathPermanent:urlString]; //crear entrada a la permanent
    if(nouPath) {
        UCHTTPRequest *request = [[UCHTTPRequest alloc] initWithURL:photoUrl];
        [request startAsynchronousWithDelegate:self withNotes:nouPath];
        [self guardarCache];
    }
    else [self.delegate imageRequestUnsuccesful:[[NSError alloc] initWithDomain:@"Error to create new path for the image" code:2 userInfo:nil]];
    
    return nil;
}

#pragma mark - methods for internal use

-(NSMutableDictionary *) obtenirCache {
    NSString *filePath = [UCImageCache filePathCache];
    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        //NSLog(@"Cache file doesn't exist, creating...");
        NSMutableDictionary *cacheNormal = [[NSMutableDictionary alloc] init];
        [cacheNormal writeToFile:filePath atomically:YES];
    }
    return [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
}


-(void) guardarCache {
    [self.cache writeToFile:[UCImageCache filePathCache] atomically:YES];
}

-(NSString *) canviarNomImatge: (NSString *) nom {
    
    UCMLParser *par = [[UCMLParser alloc] initWithString:nom]; // XXXXX.KKK
    
    NSMutableString *nomAmbPunt = [NSMutableString stringWithString:[par readUntil:@"."]]; // XXXXXX.
    
    NSString *laResta = [par readRest]; // KKK
    
    [nomAmbPunt setString: [nomAmbPunt stringByAppendingString:@"0"]];
    
    NSString *nouNom = [[nomAmbPunt stringByAppendingString:@"."] stringByAppendingString:laResta];
    
    return nouNom;
}

-(NSString *) nouPathPermanent:(NSString *) url {
    //crear entrada a la permanent i busca un nom adecuat a la imatge, retornant el path
    NSString *nom = [url lastPathComponent];
    NSString *path = [UCImageCache pathCache];
    if(!path) return nil;
    NSString *pathImage = [path stringByAppendingPathComponent:nom];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:pathImage]) {
        NSLog(@"[ImageCache] Ja hi ha una imatge amb aquest nom!");
        NSString *nouNom = [self canviarNomImatge:nom];
        NSString *nouPath =[path stringByAppendingPathComponent:nouNom];
        while ([[NSFileManager defaultManager] fileExistsAtPath:nouPath]) { //anem canviant el nom fins que obtinguem un sense imatge ja creada
            nouNom = [self canviarNomImatge:nouNom];
            nouPath = [path stringByAppendingPathComponent:nouNom];
        }
        //ja tenim un nom adecuat, per tant guardem la entrada
        [self.cache setObject:nouPath forKey:url];
        return nouPath;
    }
    
    [self.cache setObject:pathImage forKey:url]; //no hi ha cap imatge amb aquest nom. La guardem i llestos.
    return pathImage;
}

#pragma mark - delegate methods

- (void) httpRequest:(UCHTTPRequest *)request successful:(NSMutableData *)requestedData {
    
    if ([[NSFileManager defaultManager] createFileAtPath:request.notes contents:requestedData attributes:nil]) {
        //retornem la imatge
        UIImage *ui = [UIImage imageWithData:requestedData];
        [self.delegate imageRequestSuccesful:ui];
    }
    else [self.delegate imageRequestUnsuccesful:nil];
}

- (void) httpRequest:(UCHTTPRequest *)request unsuccessful:(NSError *)error {
    [self.delegate imageRequestUnsuccesful:error];
}




@end
