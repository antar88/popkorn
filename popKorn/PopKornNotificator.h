//
//  PopKornNotificator.h
//  popKorn
//
//  Created by Antar Moratona on 04/10/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import "PopKornBrain.h"

//#warning TRY_TO_REMOVE
@class PopKornBrain;

@interface PopKornNotificator : NSObject {
    PopKornBrain *brain;
}

@property (strong) PopKornBrain *brain;

- (void) scheduleNotifications;

@end
