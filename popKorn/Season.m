//
//  Season.m
//  popKorn
//
//  Created by Àlex Vergara on 23/08/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import "Season.h"

@implementation Season

@dynamic name;
@dynamic showName;
@dynamic show;
@dynamic episodes;

#pragma Mark - Initializers
+ (Season *) seasonWithName:(NSString *)givenName show:(NSString *)givenShow inContext:(NSManagedObjectContext *)context
{
    Season *season = nil;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"Season" inManagedObjectContext:context];
    request.predicate = [NSPredicate predicateWithFormat:@"name = %@ AND showName = %@",givenName,givenShow];
    NSError *error = nil;
    //NSLog(@"[Season] Initializing season in context %@...",context);
    season = [[context executeFetchRequest:request error:&error] lastObject];
    //NSLog(@"[Season] Done");
    
    if (!error && !season) {
        season = [NSEntityDescription insertNewObjectForEntityForName:@"Season" inManagedObjectContext:context];
        season.name = givenName;
        season.showName = givenShow;
        
        NSError* mocError = nil;
        [context save:&mocError];
        if (mocError) {
            NSLog(@"ERROR al guardar context en + (Season *) seasonWithName:(NSString *)givenName show:(NSString *)givenShow inContext:(NSManagedObjectContext *)context, codi: %@",[mocError localizedDescription]);
        }
    }
    
    return season;
}

+ (BOOL) seasonExists:(NSString *)givenName show:(NSString *)givenShow inContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"Season" inManagedObjectContext:context];
    request.predicate = [NSPredicate predicateWithFormat:@"name = %@ AND showName = %@",givenName,givenShow];
    NSError *error = nil;
    //NSLog(@"[Season] Checking whether season exists in context %@...",context);
    if ([[context executeFetchRequest:request error:&error] lastObject]) return YES;
    //NSLog(@"[Season] Done");
    return NO;
}

+ (Season *) createSeasonWithName:(NSString *)givenName withShow:(NSString*)showName inContext:(NSManagedObjectContext *)context
{
    Season *season = [NSEntityDescription insertNewObjectForEntityForName:@"Season" inManagedObjectContext:context];
    season.name = givenName;
    season.showName = showName;
    return season;
}

#pragma Mark - Consultors
- (int) seen
{
    int seen = 0;
    int unseen = 0;
    for (Episode *ep in self.episodes) {
        if ([ep.seen boolValue]) ++seen;
        else ++unseen;
    }
    if (unseen == 0) return seasonSeen;
    if (seen == 0) return seasonUnseen;
    return seasonHalfSeen;
}

- (NSDate *) yesterday {
    NSDate *now = [[NSDate date]init];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [gregorian setTimeZone:[NSTimeZone localTimeZone]];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.month = 0;
    components.year = 0;
    components.hour = 0;
    components.minute = 0;
    components.second = 0;
    components.day = -1;
    return [gregorian dateByAddingComponents:components toDate:now options:0];
}

- (int) seenAndEmitted
{
    NSDate *yesterday = [self yesterday];
    int seen = 0;
    int unseen = 0;
    for (Episode *ep in self.episodes) {
        NSDate *date = ep.date;
        if (date) {
            if ([date compare:yesterday] == NSOrderedAscending) {
                if ([ep.seen boolValue]) ++seen;
                else ++unseen;
            }
        }
    }
    if (unseen == 0) return seasonSeen;
    if (seen == 0) return seasonUnseen;
    return seasonHalfSeen;
}

- (int) unseenEpisodes
{
    NSDate *yesterday = [self yesterday];
    int unseen = 0;
    for (Episode *ep in self.episodes) {
        NSDate *date = ep.date;
        if (date) {
            if ([date compare:yesterday] == NSOrderedAscending) {
                if (![ep.seen boolValue]) ++unseen;
            }
        }
    }
    return unseen;
}

- (int) year
{
    int year = 0;
    for (Episode *ep in self.episodes)
        if ((ep.year < year || year == 0) && ep.year != -1) year = ep.year;
    return year;
}

@end