//
//  FeaturedList.h
//  popKorn
//
//  Created by Àlex Vergara Nebot on 15/02/12.
//  Copyright (c) 2012 Undead Coders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "FeaturedShow.h"

@interface FeaturedList : NSManagedObject

@property (nonatomic, retain) NSSet *top10;
@property (nonatomic, retain) NSSet *ourFavs;

+ (FeaturedList *) featuredListInManagedObjectContext:(NSManagedObjectContext *)context;

@end


@interface FeaturedList (CoreDataGeneratedAccessors)

- (void)addTop10Object:(NSManagedObject *)value;
- (void)removeTop10Object:(NSManagedObject *)value;
- (void)addTop10:(NSSet *)values;
- (void)removeTop10:(NSSet *)values;
- (void)addOurFavsObject:(NSManagedObject *)value;
- (void)removeOurFavsObject:(NSManagedObject *)value;
- (void)addOurFavs:(NSSet *)values;
- (void)removeOurFavs:(NSSet *)values;
@end
