//
//  TxDownloadShowList.h
//  popKorn
//
//  Created by Àlex Vergara Nebot on 27/02/12.
//  Copyright (c) 2012 Undead Coders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PopKornBrain.h"

#pragma Mark - popKornBrainDownloadDelegate protocol
@protocol popKornBrainDownloadDelegate <NSObject>
@required
- (void) setNumberOfShows:(int)number;
- (void) setShowsDownloaded:(int)number;
- (void) requestSuccessful;
- (void) requestUnsuccessful:(NSError *)error;
@end

@interface TxDownloadShowList : NSObject

#pragma Mark - Methods


#pragma mark - Methods
+ (BOOL) showListDownloaded;
+ (void) downloadShowListInContext:(NSManagedObjectContext *)context withDelegate:(id <popKornBrainDownloadDelegate>)delegate;

@end
