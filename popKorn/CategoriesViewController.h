//
//  CatShowsViewController.h
//  popKorn
//
//  Created by Àlex Vergara on 23/08/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopKornBrain.h"
#import "CatShowsViewController.h"

@interface CategoriesViewController : UIViewController <UITableViewDelegate, UITableViewDelegate> {
@private
    PopKornBrain *pkb;
}

@property (strong) PopKornBrain *pkb;
@property (strong, nonatomic) IBOutlet UITableView *table;

- initWithPopKornBrain:(PopKornBrain *)brain;

@end
