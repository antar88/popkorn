//
//  help01ViewController.m
//  popKorn
//
//  Created by Àlex Vergara Nebot on 27/10/11.
//  Copyright (c) 2011 Apple. All rights reserved.
//

#import "Welcome03ViewController.h"

#define datePicker 0
#define notifPicker 1
#define hourPicker 2
#define minutePicker 3

@implementation Welcome03ViewController
@synthesize label;
@synthesize switchNotis;
@synthesize taula;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        nombreDies = [NSArray arrayWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12",@"13",@"14",
                      @"15",@"16",@"17",@"18",@"19",@"20",@"21",@"22",@"23",@"24",@"25",@"26",@"27",@"28",@"29",@"30",
                      nil]; 
        nombreHores = [NSArray arrayWithObjects:@"00",@"01",@"02",@"03",@"04",@"05",@"06",@"07",@"08",@"09",@"10",@"11",@"12",@"13",@"14",
                       @"15",@"16",@"17",@"18",@"19",@"20",@"21",@"22",@"23",nil];
        nombreMinuts = [NSArray arrayWithObjects:@"00",@"15",@"30",@"45",nil];
        notisTitles = [[NSArray alloc]initWithObjects:NSLocalizedString(@"dayBefore", nil), NSLocalizedString(@"sameDay", nil), NSLocalizedString(@"dayAfter", nil), nil];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (IBAction)activateNotis:(id)sender {
    [PKSettings setNotis:switchNotis.on];
    [taula reloadData];
}

#pragma mark - Picker methods

-(void)pickerViewLoaded: (UIPickerView *)myPickerView{
	NSUInteger max = 16384;
    if(myPickerView.tag == hourPicker) {
        NSUInteger base24 = (max/2)-(max/2)%24;
        [myPickerView selectRow:[myPickerView selectedRowInComponent:0]%24+base24 inComponent:0 animated:false];
    }
    else {
        NSUInteger base4 = (max/2)-(max/2)%4;
        [myPickerView selectRow:[myPickerView selectedRowInComponent:0]%4+base4 inComponent:0 animated:false];
    }
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(pickerView.tag == datePicker) return [NSString stringWithFormat:@"                      %@",[nombreDies objectAtIndex:row]];
    else if (pickerView.tag == notifPicker) return [NSString stringWithFormat:@"%@",[notisTitles objectAtIndex:row]];
    else if (pickerView.tag == hourPicker) return [NSString stringWithFormat:@" %@",[nombreHores objectAtIndex:row%24]];
    else if (pickerView.tag == minutePicker) return [NSString stringWithFormat:@" %@",[nombreMinuts objectAtIndex:row%4]];
    return @"";
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(pickerView.tag == datePicker) return nombreDies.count;
    else if(pickerView.tag == notifPicker) return notisTitles.count;
    else if(pickerView.tag == hourPicker) return 16384;//return nombreHores.count;
    else if(pickerView.tag == minutePicker) return 16384;//return nombreMinuts.count;
    return 0;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if(pickerView.tag == datePicker) {
        if (filaSelec.row == 0) [PKSettings setDaysBefore:[nombreDies objectAtIndex:row]];
        else if (filaSelec.row == 1) [PKSettings setDaysAfter:[nombreDies objectAtIndex:row]];
    }
    else if(pickerView.tag == notifPicker) {
        if(row == 0) [PKSettings setNotisDay:@"0"];
        else if (row == 1) [PKSettings setNotisDay:@"1"];
        else [PKSettings setNotisDay:@"2"];
        PopKornNotificator *pkn = [[PopKornNotificator alloc] init];
        pkn.brain = self.brain;
        [pkn scheduleNotifications];
    }
    else if(pickerView.tag == hourPicker) {
        [self pickerViewLoaded:pickerView];
        [PKSettings setNotisHour:[nombreHores objectAtIndex:row%24]];
        PopKornNotificator *pkn = [[PopKornNotificator alloc]init];
        pkn.brain = self.brain;
        [pkn scheduleNotifications];
    }
    else if(pickerView.tag == minutePicker) {
        [self pickerViewLoaded:pickerView];
        [PKSettings setNotisMinutes:[nombreMinuts objectAtIndex:row%4]];
        PopKornNotificator *pkn = [[PopKornNotificator alloc]init];
        pkn.brain = self.brain;
        [pkn scheduleNotifications];
    }
    [taula reloadData];
}


//custom showsPickers

- (void) showPicker
{
    //Definim menu
    UIActionSheet *menu = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"selectDays", nil) 
                                                      delegate:self
                                             cancelButtonTitle:NSLocalizedString(@"done", nil)
                                        destructiveButtonTitle:nil 
                                             otherButtonTitles:nil];
    //Definim Picker
    UIPickerView *pickerView = [[UIPickerView alloc] init];
    pickerView.frame = CGRectMake(0, 94, 320, 162);                           
    pickerView.delegate = self;
    pickerView.tag = datePicker;
    pickerView.showsSelectionIndicator = YES; 
    if(filaSelec.row == 0) [pickerView selectRow:([[PKSettings daysBefore]integerValue]-1) inComponent:0 animated:YES];
    else if(filaSelec.row == 1) [pickerView selectRow:([[PKSettings daysAfter]integerValue]-1) inComponent:0 animated:YES];
    //Afegim Picker al menu
    menu.alpha = 0.8;
    [menu addSubview:pickerView];
    [menu showFromTabBar:self.tabBarController.tabBar];
    [menu setBounds:CGRectMake(0,0,320, 400)]; 
    //Relases
}


-(void) canviHoresNotis:(id) sender
{
    NSDateComponents *hourAndMinutes = [NSDateComponents alloc];
    //hourAndMinutes = [[NSCalendar currentCalendar] components:NSHourCalendarUnit | NSMinuteCalendarUnit fromDate:pickHourNotis.date];
    [PKSettings setNotisHour:[[NSString alloc] initWithFormat:@"%d",hourAndMinutes.hour]];
    [PKSettings setNotisMinutes:[[NSString alloc] initWithFormat:@"%d",hourAndMinutes.minute]];
    [taula reloadData];
    PopKornNotificator *pkn = [[PopKornNotificator alloc]init];
    pkn.brain = self.brain;
    [pkn scheduleNotifications];
}

- (void) showDatePicker
{
    //Definim menu
    UIActionSheet *menu = [[UIActionSheet alloc] initWithTitle:nil
                                                      delegate:self
                                             cancelButtonTitle:NSLocalizedString(@"done", nil)
                                        destructiveButtonTitle:nil
                                             otherButtonTitles:nil];    
    //Definim DatePiker
    UIPickerView *hores = [[UIPickerView alloc]init];
    hores.hidden = NO;
    hores.tag = hourPicker;
    hores.frame = CGRectMake(180,74,75, 216);
    hores.delegate = self;
    hores.showsSelectionIndicator = YES;
    
    UIPickerView *minuts = [[UIPickerView alloc]init];
    minuts.hidden = NO;
    minuts.tag = minutePicker;
    minuts.frame = CGRectMake(245,74,75, 216);
    minuts.delegate = self;
    minuts.showsSelectionIndicator = YES;
    
    //Definim Picker
    UIPickerView *picDiesNotis = [[UIPickerView alloc]init];
    picDiesNotis.hidden = NO;
    picDiesNotis.tag = notifPicker;
    picDiesNotis.frame = CGRectMake(0,74,190, 216);
    picDiesNotis.delegate = self;
    picDiesNotis.showsSelectionIndicator = YES;
    
    //transparencies
    picDiesNotis.alpha = 1.0;
    //pickHourNotis.alpha = 1.0;
    
    //Afegim Pickers al menu
    menu.alpha = 0.7;
    //[menu addSubview:pickHourNotis];
    [menu addSubview:hores];
    [menu addSubview:minuts];
    [menu addSubview:picDiesNotis];
    [menu showInView:superview.view];
    //[menu showFromTabBar:self.tabBarController.tabBar];
    [menu setBounds:CGRectMake(0,0,320, 500)]; 

    
    //seleccionem dia i hora anterior
    if([[PKSettings notisDay]intValue] == 0) [picDiesNotis selectRow:0 inComponent:0 animated:YES];
    else if ([[PKSettings notisDay]intValue] == 1) [picDiesNotis selectRow:1 inComponent:0 animated:YES];
    else [picDiesNotis selectRow:2 inComponent:0 animated:YES];
    [hores selectRow:[[PKSettings notisHour]intValue]+24*3 inComponent:0 animated:NO];
    int val = [[PKSettings notisMinutes]intValue];
    if (val == 15) val = 1;
    else if (val == 30) val = 2;
    else if (val == 45) val = 3;
    [minuts selectRow:val+400 inComponent:0 animated:NO];
    //Relases
}

#pragma mark - Table dataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    if (indexPath.row == 0){
        cell.textLabel.text = NSLocalizedString(@"noti", nil);
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    else {
        cell.textLabel.text = NSLocalizedString(@"when", nil);

        if (switchNotis.on) {
            cell.textLabel.alpha = 1.0;
            cell.detailTextLabel.alpha = 1.0;
            cell.userInteractionEnabled = YES;

            UIView *bgColorView = [[UIView alloc] init];
            [bgColorView setBackgroundColor:[UIColor orangeColor]];
            [cell setSelectedBackgroundView:bgColorView];        //changes selection color to orange

            NSString *notisConf;
            if ([[PKSettings notisDay]intValue] == 0) notisConf = NSLocalizedString(@"dayBefore", nil);
            else if ([[PKSettings notisDay]intValue] == 1) notisConf = NSLocalizedString(@"sameDay", nil);
            else notisConf = NSLocalizedString(@"dayAfter", nil);
            if([[PKSettings notisHour]intValue] == 1) cell.detailTextLabel.text = [notisConf stringByAppendingFormat:@" %@ %@",NSLocalizedString(@"at", nil) ,[PKSettings notisHourMinutes]];
            else cell.detailTextLabel.text = [notisConf stringByAppendingFormat:@" %@ %@",NSLocalizedString(@"ats", nil) ,[PKSettings notisHourMinutes]];
        }
        else {
            cell.textLabel.alpha = 0.439216f;
            cell.detailTextLabel.alpha = 0.439216f;
            cell.userInteractionEnabled = NO;
            cell.detailTextLabel.text = NSLocalizedString(@"Notifications disabled", nil);
        }
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

#pragma mark - Table delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 1) [self showDatePicker]; //datePicker
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.label.text = NSLocalizedString(@"welcomeNotis", nil);
}

- (void)viewDidUnload
{
    [self setTaula:nil];
    [self setSwitchNotis:nil];
    [self setLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
