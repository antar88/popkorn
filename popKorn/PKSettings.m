//
//  PKSettings.m
//  popKorn
//
//  Created by Antar Moratona on 06/09/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import "PKSettings.h"

@implementation PKSettings

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

+(NSString *) prefPath {
    NSString *filePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Library"];
    filePath = [filePath stringByAppendingPathComponent:@"Preferences"];
    filePath = [filePath stringByAppendingPathComponent:@"Config.plist"];
    return filePath;
}

+(NSMutableDictionary *) loadSettings {
    NSString *filePath = [self prefPath];
    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        NSLog(@"File doesn't exist, creating...");
        NSMutableDictionary *plistDict = [[NSMutableDictionary alloc] init];
        
        // ... variables de settings ...
        
        [plistDict setValue:@"2" forKey:@"daysBefore"];
        [plistDict setValue:@"7" forKey:@"daysAfter"];
        [plistDict setValue:nil forKey:@"lastUpdate"];
        [plistDict setValue:[NSNumber numberWithBool:NO] forKey:@"translate"];
        [plistDict setValue:[NSNumber numberWithBool:NO] forKey:@"popKornPlus"];
        
        [plistDict setValue:[NSNumber numberWithBool:YES] forKey:@"notifications"];
        [plistDict setValue:@"12" forKey:@"notisHour"];
        [plistDict setValue:@"00" forKey:@"notisMinutes"];
        [plistDict setValue:@"1" forKey:@"notisDay"]; //sameDay
        
        [plistDict writeToFile:filePath atomically:YES];
    }
    return [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
}

+(void) saveSettings:(NSMutableDictionary *)aPlist {
    NSString *path = [self prefPath];
    [aPlist writeToFile:path atomically: YES];
}

+(NSDate *) lastUpdate {
    NSMutableDictionary *pref = [self loadSettings];
    if([pref objectForKey:@"lastUpdate"] == nil) {
        [pref setValue:nil forKey:@"lastUpdate"];
        [self saveSettings:pref];
    }
    return [pref objectForKey:@"lastUpdate"];
}

+(void) setLastUpdate:(NSDate *)date {
    NSMutableDictionary *pref = [self loadSettings];
    [pref setValue:date forKey:@"lastUpdate"];
    [self saveSettings:pref];
}

+(void) setDaysBefore:(NSString *) daysBefore {
    NSMutableDictionary *pref = [self loadSettings];
    [pref setValue:daysBefore forKey:@"daysBefore"];
    [self saveSettings:pref];
}

+(NSString *) daysBefore {
    NSMutableDictionary *pref = [self loadSettings];
    if([pref objectForKey:@"daysBefore"] == nil) {
        [pref setValue:@"2" forKey:@"daysBefore"];
        [self saveSettings:pref];
    }
    return [pref objectForKey:@"daysBefore"];
}

+(void) setDaysAfter:(NSString *) daysAfter {
    NSMutableDictionary *pref = [self loadSettings];
    [pref setValue:daysAfter forKey:@"daysAfter"];
    [self saveSettings:pref];
}

+(NSString *) daysAfter {
    NSMutableDictionary *pref = [self loadSettings];
    if([pref objectForKey:@"daysAfter"] == nil) {
        [pref setValue:@"7" forKey:@"daysAfter"];
        [self saveSettings:pref];
    }
    return [pref objectForKey:@"daysAfter"];
}

+(void) setTranslate:(BOOL) translate {
    NSMutableDictionary *pref = [self loadSettings];
    [pref setValue:[NSNumber numberWithBool:translate] forKey:@"translate"];
    [self saveSettings:pref];
}

+(BOOL) translate {
    NSMutableDictionary *pref = [self loadSettings];
    if([pref objectForKey:@"translate"] == nil) {
        [pref setValue:[NSNumber numberWithBool:NO] forKey:@"translate"];
        [self saveSettings:pref];
    }
    return [[pref objectForKey:@"translate"] boolValue];
}

+(void) setNotis:(BOOL)notis {
    NSMutableDictionary *pref = [self loadSettings];
    [pref setValue:[NSNumber numberWithBool:notis] forKey:@"notifications"];
    [self saveSettings:pref];
}

+(BOOL) notis {
    NSMutableDictionary *pref = [self loadSettings];
    if([pref objectForKey:@"notifications"] == nil) {
        [pref setValue:[NSNumber numberWithBool:YES] forKey:@"notifications"];
        [self saveSettings:pref];
    }
    return [[pref objectForKey:@"notifications"] boolValue];
}

+(void) setNotisDay:(NSString *) day {
    NSMutableDictionary *pref = [self loadSettings];
    [pref setValue:day forKey:@"notisDay"];
    [self saveSettings:pref];
}

+(NSString *) notisDay {
    NSMutableDictionary *pref = [self loadSettings];
    if([pref objectForKey:@"notisDay"] == nil) {
        [pref setValue:@"1" forKey:@"notisDay"];
        [self saveSettings:pref];
    }
    return [pref objectForKey:@"notisDay"];
}

+(void)setNotisHour:(NSString *)hour {
    NSMutableDictionary *pref = [self loadSettings];
    [pref setValue:hour forKey:@"notisHour"];
    [self saveSettings:pref];
}

+(NSString *)notisHour {
    NSMutableDictionary *pref = [self loadSettings];
    if([pref objectForKey:@"notisHour"] == nil) {
        [pref setValue:@"12" forKey:@"notisHour"];
        [self saveSettings:pref];
    }
    return [pref objectForKey:@"notisHour"];
}

+(void)setNotisMinutes:(NSString *)minutes {
    NSMutableDictionary *pref = [self loadSettings];
    [pref setValue:minutes forKey:@"notisMinutes"];
    [self saveSettings:pref];
}

+(NSString *)notisMinutes {
    NSMutableDictionary *pref = [self loadSettings];
    if([pref objectForKey:@"notisMinutes"] == nil) {
        [pref setValue:@"00" forKey:@"notisMinutes"];
        [self saveSettings:pref];
    }
    return [pref objectForKey:@"notisMinutes"];
}

+ (NSDateComponents *)notisDate {
    NSMutableDictionary *pref = [self loadSettings];
    NSDateComponents *comp = [[NSDateComponents alloc]init];
    comp.day = 0;
    comp.year = 0;
    comp.month = 0;
    comp.second = 0;
    if( [pref objectForKey:@"notisHour"] == nil) comp.hour = 12;
    else comp.hour = [[pref objectForKey:@"notisHour"] intValue];
    if ([pref objectForKey:@"notisMinutes"] == nil ) comp.minute = 0;
    else comp.minute = [[pref objectForKey:@"notisMinutes"] intValue];
    return comp;
}

+(NSString *)notisHourMinutes{
    NSDateComponents *comp = [self notisDate];
    if(comp.minute == 0) {
        if(comp.hour < 10) return [NSString stringWithFormat:@"0%d:00",comp.hour];
        return [NSString stringWithFormat:@"%d:00",comp.hour];
    }
    if(comp.hour < 10) return [NSString stringWithFormat:@"0%d:%d",comp.hour,comp.minute];
    return [NSString stringWithFormat:@"%d:%d",comp.hour,comp.minute];
}

+(BOOL) popKornPlus {
    NSMutableDictionary *pref = [self loadSettings];
    if([pref objectForKey:@"popKornPlus"] == nil) {
        [pref setValue:[NSNumber numberWithBool:NO] forKey:@"popKornPlus"];
        [self saveSettings:pref];
    }
    return [[pref objectForKey:@"popKornPlus"] boolValue];
}

+(void) setPopKornPlus:(BOOL)plus {
    NSMutableDictionary *pref = [self loadSettings];
    [pref setValue:[NSNumber numberWithBool:plus] forKey:@"popKornPlus"];
    [self saveSettings:pref];
}

@end
