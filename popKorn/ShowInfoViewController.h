//
//  ShowInfoViewController.h
//  popKorn
//
//  Created by Àlex Vergara on 10/09/11.
//  Copyright 2011 Undead Coders. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopKornBrain.h"

@interface ShowInfoViewController : UIViewController <UCSharerDelegate,imagesControllerDelegate> {
    UILabel *labelYear;
    UILabel *labelClassification;
    UILabel *labelGenre;
    UILabel *labelStatus;
    UILabel *labelTitle;
    UIButton *shareTwitter;
    UIButton *shareFacebook;
    UIButton *shareMail;
    UIImageView *showImage;
    UCImageCache *imgCnt;
    PopKornBrain *pkb;
    NSString *imageURL;
    NSString *showPlot;
    NSString *showUrl;
}

@property (nonatomic, strong) IBOutlet UILabel *labelYear;
@property (nonatomic, strong) IBOutlet UILabel *labelClassification;
@property (nonatomic, strong) IBOutlet UILabel *labelGenre;
@property (nonatomic, strong) IBOutlet UILabel *labelStatus;
@property (nonatomic, strong) IBOutlet UILabel *labelTitle;
@property (nonatomic, strong) IBOutlet UIButton *shareTwitter;
@property (nonatomic, strong) IBOutlet UIButton *shareFacebook;
@property (nonatomic, strong) IBOutlet UIButton *shareMail;
@property (nonatomic, strong) IBOutlet UIImageView *showImage;
@property (nonatomic, strong) NSString *showPlot;
@property (nonatomic, strong) NSString *showUrl;
@property (strong) PopKornBrain *pkb;

- (void) initSharer:(id <UCSharerDelegate>)givenDelegate;

- (IBAction)shareTwitterPressed:(id)sender;
- (IBAction)shareFacebookPressed:(id)sender;
- (IBAction)shareMailPressed:(id)sender;

- (void) setYear:(NSString *)year;
- (void) setClassif:(NSString *)classif;
- (void) setGenre:(NSString *)genre;
- (void) setStatus:(NSString *)status;
- (void) setTitle:(NSString *)title;
- (void) setImage:(NSURL *)imageUrl;

@end
